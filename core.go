package activitystreams

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"gitlab.com/activitygolang/go-activitystreams/object"
	"strings"
)

const (
	_ = iota
	CSListAppend
	CSListRemove
	CSAddField
	CSUpdateField
	CSRemoveField
	CSAddObject
	CSUpdateObject
	CSRemoveObject
	CSColAdd
	CSColRM
	CSColSet
	CSColRMAll
)

const RECURSE_MAX = 1

type Change struct {
	Action    int // The action to perform in reversing
	ObjectID  object.LocalID
	FieldName string
	Value     interface{}
}

type PropRetryTarget struct {
	Url      string
	Cluster  bool // Is this a cluster-local propagation?
	TryCount uint
	LastTry  string // RFC3339 timestamp
}

func NewPropRetryGlobal(url string) (p PropRetryTarget) {
	return PropRetryTarget{
		Url:      url,
		Cluster:  false,
		TryCount: 0,
		LastTry:  "",
	}
}

func NewPropRetryCluster(url object.LocalID) (p PropRetryTarget) {
	return PropRetryTarget{
		Url:      string(url),
		Cluster:  true,
		TryCount: 0,
		LastTry:  "",
	}
}

// server core
type Core struct {
	db             Storage
	outboxHandlers map[string]func(Transaction, object.LocalID, object.LocalID, map[string]interface{}) (object.LocalID, []interface{}, []Change, error)
	inboxHandlers  map[string]func(Transaction, object.LocalID, map[string]interface{}) error
	Hostname       string
	SubserverName  string
	Port           uint16
	Prefix         string
}

func NewCore() (c *Core) {
	c = &Core{}
	c.outboxHandlers = map[string]func(Transaction, object.LocalID, object.LocalID, map[string]interface{}) (object.LocalID, []interface{}, []Change, error){
		"Accept":          c.OutboxAccept,
		"Add":             c.OutboxAdd,
		"Announce":        c.OutboxNonSpecialActivity,
		"Arrive":          c.OutboxNonSpecialActivity,
		"Block":           c.OutboxBlock,
		"Create":          c.OutboxCreate,
		"Delete":          c.OutboxDelete,
		"Dislike":         c.OutboxNonSpecialActivity,
		"Flag":            c.OutboxNonSpecialActivity, // TODO: real flagging
		"Follow":          c.OutboxFollow,
		"Ignore":          nil,
		"Invite":          nil,
		"Join":            nil,
		"Leave":           nil,
		"Like":            c.OutboxLike,
		"Listen":          c.OutboxNonSpecialActivity,
		"Move":            c.OutboxMove,
		"Offer":           c.OutboxNonSpecialActivity,
		"Question":        c.OutboxQuestion,
		"Reject":          c.OutboxNonSpecialActivity,
		"Read":            c.OutboxNonSpecialActivity,
		"Remove":          c.OutboxRemove,
		"TentativeReject": c.OutboxNonSpecialActivity,
		"TentativeAccept": c.OutboxAccept,
		"Travel":          c.OutboxNonSpecialActivity,
		"Undo":            c.OutboxUndo,
		"Update":          c.OutboxUpdate,
		"View":            c.OutboxNonSpecialActivity,
	}
	c.inboxHandlers = map[string]func(Transaction, object.LocalID, map[string]interface{}) error{
		"Accept":   c.InboxAccept,
		"Add":      nil,
		"Announce": nil,
		"Arrive":   nil,
		"Block":    nil,
		"Create":   nil,
		"Delete":   nil,
		"Dislike":  nil,
		"Flag":     nil,
		"Follow":   nil,
		// TODO: flag ignore invite join leave
		"Like":            c.InboxLike,
		"Listen":          nil,
		"Move":            nil,
		"Offer":           nil,
		"Question":        nil,
		"Reject":          nil,
		"Read":            nil,
		"Remove":          nil,
		"TentativeReject": nil,
		"TentativeAccept": c.InboxAccept,
		"Travel":          nil,
		// TODO: undo, update
		"View": nil,
	}
	return c
}

func (c *Core) SetDatabase(s Storage) {
	c.db = s
}

func (c *Core) IsSubserver() (issub bool) {
	return len(c.SubserverName) > 0
}

// PostOutbox handles all of the generally applicable tasks for activities
// a client posts to an actor's outbox. Tasks specific to an activity are
// performed by the outbox handlers.
// PostOutbox handles:
// * Object sanity checking TODO
// * Actor / Outbox matching
// * Create wrapping of non-activity objects
// * Storing the incoming objects and adding them to the actor's outbox
// * Distributing replies and propagation
func (c *Core) PostOutbox(outboxID object.LocalID, activity map[string]interface{}) (activityIDStr string, err error) {
	// We can assume the poster is already authenticated by the outer layers
	tx, err := c.db.StartTransaction()
	if err != nil {
		return "", err
	}
	defer tx.EndTransaction(&err)
	// We need the actorID for the handlers, as well as to confirm the outbox
	actorID, err := c.NativeObjectOwner(tx, outboxID)
	if err == object.NoObject {
		return "", InvalidOutbox
	} else if err != nil {
		return "", err
	}
	// Actor and Outbox *must* match.
	obID, err := tx.LoadObjectField(actorID, "outbox")
	if obID != outboxID || err != nil {
		return "", InvalidOutbox
	}
	// Send the activity to one of the specific handlers
	activityType, err := object.GetStr(activity, "@type")
	if err == object.NoField { // Object may use grey-spec "type"
		activityType, _ = object.GetStr(activity, "type")
	}
	handler, ok := c.outboxHandlers[activityType]
	if !ok { // Not an activity, wrap it
		activity = CreateWrap(activity)
		activityType = "Create"
		handler = c.outboxHandlers["Create"]
	}
	var propagation []interface{}
	var activityID object.LocalID
	var changes []Change
	if handler != nil {
		activityID, propagation, changes, err = handler(tx, actorID, outboxID,
			activity)
		if err != nil {
			return "", err
		}
	}
	// Store activity
	if err = c.storeAndAppendActivity(tx, outboxID, activity, changes); err != nil {
		return "", err
	}
	if propagation == nil {
		propagation = []interface{}{}
	}
	addedPropagation := ExtractCorePropagation(activity, actorID, c.Hostname,
		c.Port)
	propagation = append(propagation, addedPropagation...)
	// Do propagation
	local, remote := c.splitPropagation(tx, propagation)
	// store remote
	err = tx.StoreProp(activity, remote)
	if err != nil {
		return "", err
	}
	// Don't want PostInbox errors to step on the activity: end transaction.
	tx.EndTransaction(nil)
	for _, localTarget := range local {
		localInbox := localTarget + "/inbox"
		err = c.PostInboxLocal(localInbox, activity)
		if err != nil {
		}
	}
	activityIDStr = GlobalizeLocal(activityID, c.Hostname, c.Port)
	return activityIDStr, nil
}

func (c *Core) PostInboxLocal(inboxID object.LocalID, activity map[string]interface{}) (err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	return c.postInboxCore(tx, inboxID, activity)
}

// postInboxCore handles the generally applicable tasks for activities that
// are propagated to an actor's inbox. Due to the different sources the
// activites can originate this is called indirectly through the PostInboxLocal
// and PostInboxRemote methods.
// postInboxCore handles:
// * Target sanity checking
// * Blocklist check
// * Adding to actor inboxes
// * Adding to object "replies" lists
func (c *Core) postInboxCore(tx Transaction, inboxID object.LocalID, activity map[string]interface{}) (err error) {
	// === Sanity check inbox
	// check that target is an inbox
	if !strings.HasSuffix(string(inboxID), "/inbox") {
		// bad target
		return InvalidTargetType
	}
	// check exists and type OrderedCollection
	targetInbox, err := tx.LoadObject(inboxID)
	if err != nil {
		return err
	}
	targetType, _ := object.GetStr(targetInbox, "@type")
	if !IsCollection(targetType) { // splitPropagation screwed up
		return InvalidTargetType
	}
	// === get actor and blocklist
	// owning actor is where?
	// Check to see if the sending actor is blocked by the target
	targetActorID, _ := object.GetLocalID(targetInbox, "attributedTo")
	// load target actor
	targetActor, err := tx.LoadObject(targetActorID)
	if err != nil {
		return err
	}
	blocklistID, err := object.GetLocalID(targetActor, "blocked")
	if err != nil {
		return object.BrokenObject
	}
	// load target block list
	blockList, err := tx.LoadObjectField(blocklistID, "items")
	if err != nil && err != object.NoField {
		return err
	}
	sourceActor, _ := object.GetStr(activity, "attributedTo")
	if blockList != nil { // skip if blocklist is empty
		switch blockList.(type) {
		case []interface{}:
			if IsBlocked(blockList.([]interface{}), sourceActor) {
				return BlockedActor
			}
		case string: // Scalar string
			if sourceActor == blockList.(string) {
				return BlockedActor
			}
		}
	}
	// Add to inbox
	// @id could be string or local
	// try string, else try local
	var activityID interface{}
	activityID, err = object.GetStr(activity, "@id")
	if err != nil {
		activityID, _ = object.GetLocalID(activity, "@id")
	}
	// check that this is not already in the inbox
	alreadySeen, err := tx.ValueInObject(inboxID, "items", activityID)
	if err != nil && err != object.NoField {
		return err
	}
	if alreadySeen {
		return nil
	}
	// add to inbox
	err = tx.ListAppend(inboxID, "items", activityID)
	if err != nil {
		return err
	}
	// Distribute replies
	if replyTos, ok := activity["inReplyTo"]; ok {
		fmt.Println("replyTo:", replyTos)
		err = c.distributeReplies(tx, activityID, targetActorID, replyTos)
		if err != nil {
			return err
		}
	}
	// Send to handler
	activityType, _ := object.GetStr(activity, "@type")
	handler, ok := c.inboxHandlers[activityType]
	if ok && handler != nil {
		actorID, _ := object.GetLocalID(targetActor, "@id")
		err = handler(tx, actorID, activity)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Core) PostInboxRemote(inboxID object.LocalID, activity map[string]interface{}) (err error) {
	// Handle: storing, ID extraction, call to get targets, send to core
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	// Did this object originally come from this server?
	activityID, err := object.GetStr(activity, "@id")
	if err != nil {
		return err
	}
	localActivityID := object.ExtractNativeFromGlobal(activityID, c.Hostname,
		c.Port)
	isLocal := localActivityID == ""
	if isLocal {
		// check main storage
		_, err = tx.LoadObject(localActivityID)
	} else {
		// check cache
		_, err = tx.LoadCache(activityID)
	}
	// Ghost reply handling *only* fires the first time an activity is seen.
	// Already seen objects do not need to be stored a second time.
	seen := true
	if err == object.NoObject {
		seen = false
	}
	// Need to check that we haven't seen this activity with this target.
	// if seen, look at target's inbox? TODO
	err = c.postInboxCore(tx, inboxID, activity)
	if err != nil {
		return err
	}
	if !seen {
		ghostCollections, err := c.ghostReplyTargets(tx, activity)
		localTargets, remoteTargets := c.splitPropagation(tx,
			ghostCollections)
		// store remote propagation
		err = tx.StoreProp(activity, remoteTargets)
		if err != nil {
			// TODO how do we handle this error?
		}
		if localTargets != nil {
			// send to PostInboxLocal
			for _, local := range localTargets {
				localInbox := local + "/inbox"
				c.postInboxCore(tx, localInbox, activity)
			}
		}
		if !isLocal {
			err = tx.StoreCache(activity)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (c *Core) Get(objectID object.LocalID, authActor object.LocalID) (gotObj map[string]interface{}, err error) {
	gotObj, err = c.GetLocal(objectID, authActor)
	if err != nil {
		return nil, err
	}
	GlobalizeObject(gotObj, c.Hostname, c.Port)
	return gotObj, nil
}

func (c *Core) GetLocal(objectID object.LocalID, authActor object.LocalID) (gotObj map[string]interface{}, err error) {
	objectID = object.EnforceLocalIDSanity(objectID)
	authActor = object.EnforceLocalIDSanity(authActor)
	// TODO: check authorization for this object against the authentication
	// TODO: filter object fields based on authentication
	// load the object, and any relevant subobjects
	tx, err := c.db.StartTransaction()
	if err != nil {
		return nil, err
	}
	defer tx.EndTransaction(&err)
	gotObj, err = tx.LoadObject(objectID)
	if err == nil { // Normal object
		// check to see if this object has any obj-cols
		fields, err := tx.ObjcolGetFields(objectID)
		if err == nil {
			for _, field := range fields {
				// assemble id
				temp := string(objectID) + "/" + field
				objcolID := object.EnforceLocalIDSanity(object.LocalID(temp))
				// inject id
				gotObj[field] = objcolID
			}
		} else if err != object.NoField {
			return nil, err
		}
	} else if err == object.NoObject { // Special object
		// index of last slash
		index := strings.LastIndex(string(objectID), "/")
		if index <= 0 { // Nothing to slice off as field name
			return nil, object.NoObject
		}
		// slice off last segment of ID
		choppedID := objectID[:index]
		field := string(objectID[index+1:]) // no trailing slash pre-enforced
		// Check that root object exists and get the owner
		_, owner, err := tx.BasicObjectData(choppedID)
		if err != nil {
			return nil, err
		}
		gotObj, err = c.get_Objcol(tx, objectID, choppedID, field, owner)
		if err != nil {
			return nil, err
		}
	} else if err != nil {
		return nil, err
	}
	return gotObj, nil
}

func (c *Core) get_Objcol(tx Transaction, colID object.LocalID, objectID object.LocalID, field string, ownerID object.LocalID) (obj map[string]interface{}, err error) {
	// load object
	data, err := tx.ObjcolGetAllField(objectID, field)
	if err != nil {
		fmt.Println("OBJCOL ALL FIELD", err)
		return nil, err
	}
	// assemble collection
	obj = map[string]interface{}{
		"@id": colID, "@type": "Collection", "attributedTo": ownerID,
		"items": data,
	}
	return obj, nil
}

// Generates object IDs for newly created objects and activities that are not
// special objects.
func (c *Core) GenerateObjectID(actorID object.LocalID) (objectID object.LocalID) {
	// No attempt in being made at thread safety for the moment
	// Use both actor ID and current time for collision avoidance
	base := string(actorID) + MakeNowTimestamp()
	hashBytes := md5.Sum([]byte(base))
	// TODO: better hash needed?
	// The point of the hash isn't to be secure in any way, just to scramble
	hashed := hex.EncodeToString(hashBytes[:])
	var temp string
	if c.Prefix == "" {
		temp = fmt.Sprintf("/%s/%s", actorID, hashed)
	} else {
		temp = fmt.Sprintf("/%s/%s/%s", c.Prefix, actorID, hashed)
	}
	objectID = object.LocalID(temp)
	objectID = object.EnforceLocalIDSanity(objectID)
	return objectID
}

// Creates an Actor and it's supporting objects.
func (c *Core) AddActor(actorID object.LocalID, actorType string) (err error) {
	actorID = object.EnforceLocalIDSanity(actorID)
	if !IsActor(actorType) {
		return object.BadType
	}
	if c.Prefix != "" {
		temp := object.LocalID(c.Prefix) + actorID
		actorID = object.EnforceLocalIDSanity(temp)
	}
	// TODO: check for empty actor
	actor := map[string]interface{}{"@id": actorID, "@type": actorType,
		"attributedTo": actorID}
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	newCol := func(ordered bool, name string) (err error) {
		id := object.LocalID(string(actorID) + "/" + name)
		colType := map[bool]string{true: "OrderedCollection",
			false: "Collection"}[ordered]
		box := map[string]interface{}{"@id": id, "@type": colType,
			"attributedTo": actorID}
		actor[name] = id
		if err = tx.StoreObject(box); err != nil {
			return err
		}
		err = tx.SetProtected(id, true)
		return err
	}
	// Outbox
	if err = newCol(true, "outbox"); err != nil {
		return err
	}
	// Inbox
	if err = newCol(true, "inbox"); err != nil {
		return err
	}
	// Liked
	if err = newCol(false, "liked"); err != nil {
		return err
	}
	// Followers
	if err = newCol(false, "followers"); err != nil {
		return err
	}
	// Following
	if err = newCol(false, "following"); err != nil {
		return err
	}
	// Blocked
	if err = newCol(false, "blocked"); err != nil {
		return err
	}
	// Actor
	if err = tx.StoreObject(actor); err != nil {
		return err
	}
	return nil
}

// Creates the inverse of a Change. This is used to create changesets for Undo.
func (c *Core) buildInverseChange(tx Transaction, old Change) (inverse Change, err error) {
	switch old.Action {
	case CSListAppend:
		inverse.Action = CSListRemove
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = old.Value
	case CSListRemove:
		inverse.Action = CSListAppend
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = old.Value
	case CSAddField:
		inverse.Action = CSRemoveField
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
	case CSUpdateField:
		value, err := tx.LoadObjectField(old.ObjectID, old.FieldName)
		if err != nil {
			return Change{}, err
		}
		inverse.Action = CSUpdateField
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = value
	case CSRemoveField:
		value, err := tx.LoadObjectField(old.ObjectID, old.FieldName)
		if err != nil {
			return Change{}, err
		}
		inverse.Action = CSAddField
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = value
	case CSAddObject:
		inverse.Action = CSRemoveObject
		inverse.ObjectID = old.ObjectID
	case CSUpdateObject:
		value, err := tx.LoadObject(old.ObjectID)
		if err != nil {
			return Change{}, err
		}
		inverse.Action = CSUpdateObject
		inverse.ObjectID = old.ObjectID
		inverse.Value = value
	case CSRemoveObject:
		value, err := tx.LoadObject(old.ObjectID)
		if err != nil {
			return Change{}, err
		}
		inverse.Action = CSAddObject
		inverse.ObjectID = old.ObjectID
		inverse.Value = value
	case CSColAdd:
		inverse.Action = CSColRM
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = old.Value
	case CSColRM:
		inverse.Action = CSColAdd
		inverse.ObjectID = old.ObjectID
		inverse.FieldName = old.FieldName
		inverse.Value = old.Value
	case CSColSet:
		inverse.Action = CSColRMAll
		inverse.ObjectID = old.ObjectID
	case CSColRMAll:
		value, err := tx.ObjcolGetAll(old.ObjectID)
		if err != nil {
			return Change{}, err
		}
		inverse.Action = CSColSet
		inverse.ObjectID = old.ObjectID
		inverse.Value = value
	default:
		return Change{}, BadChangeset
	}
	return inverse, nil
}

// Executes the operation defined by a Change structure.
func (c *Core) runChange(tx Transaction, chg Change) (err error) {
	switch chg.Action {
	case CSListAppend:
		err = tx.ListAppend(chg.ObjectID, chg.FieldName, chg.Value)
	case CSListRemove:
		err = tx.ListRemove(chg.ObjectID, chg.FieldName, chg.Value)
	case CSAddField:
		err = tx.AddField(chg.ObjectID, chg.FieldName, chg.Value)
	case CSUpdateField:
		err = tx.UpdateField(chg.ObjectID, chg.FieldName, chg.Value)
	case CSRemoveField:
		err = tx.RemoveField(chg.ObjectID, chg.FieldName)
	case CSAddObject:
		switch chg.Value.(type) {
		case map[string]interface{}:
			err = tx.StoreObject(chg.Value.(map[string]interface{}))
		default:
			err = object.BadType
		}
	case CSUpdateObject:
		err = tx.RemoveObject(chg.ObjectID)
		if err == nil {
			switch chg.Value.(type) {
			case map[string]interface{}:
				err = tx.StoreObject(chg.Value.(map[string]interface{}))
			default:
				err = object.BadType
			}
		}
	case CSRemoveObject:
		err = tx.RemoveObject(chg.ObjectID)
	case CSColAdd:
		err = tx.ObjcolAdd(chg.ObjectID, chg.FieldName, chg.Value)
	case CSColRM:
		err = tx.ObjcolRM(chg.ObjectID, chg.FieldName, chg.Value)
	case CSColSet:
		switch chg.Value.(type) {
		case map[string][]interface{}:
			err = tx.ObjcolSet(chg.ObjectID,
				chg.Value.(map[string][]interface{}))
		default:
			err = object.BadType
		}
	case CSColRMAll:
		err = tx.ObjcolRMAll(chg.ObjectID)
	default:
		err = BadChangeset
	}
	return err
}

// Sorts an undifferentiated list of propagation targets into local and remote
// targets.
func (c *Core) splitPropagation(tx Transaction, targets []interface{}) (local []object.LocalID, remote []PropRetryTarget) {
	local = make([]object.LocalID, 0)
	remote = make([]PropRetryTarget, 0)
	for _, target := range targets {
		subLocal, subRemote := c.sortPropagationTarget(tx, target,
			RECURSE_MAX)
		if subLocal != nil {
			local = append(local, subLocal...)
		}
		if subRemote != nil {
			remote = append(remote, subRemote...)
		}
	}
	return local, remote
}

// Sorts a specific propagation target into local, local-cluster, or remote.
// Handles recursive local collection targets.
func (c *Core) sortPropagationTarget(tx Transaction, target interface{}, recurse uint) (local []object.LocalID, remote []PropRetryTarget) {
	local = make([]object.LocalID, 0)
	remote = make([]PropRetryTarget, 0)
	// The target should be either a string or a LocalID. If it is a LocalID
	// we can go straight to the next stage. If it is a string then we need
	// to figure out if it is native to this server or on a foreign server.
	// Native strings are converted to LocalIDs and passed to the next stage,
	// foreign strings turned into remote targets.
	var localTarget object.LocalID
	switch target.(type) {
	case string:
		// Could be remote or native, break it apart and see
		loc, host, port, err := object.LocalIDFromGlobalID(target.(string))
		if err != nil {
			return nil, nil // Not a URL
		}
		if host == c.Hostname && port == c.Port {
			// This is a native ID
			localTarget = loc
		} else {
			// This is a foreign ID
			newTarget := NewPropRetryGlobal(target.(string))
			remote = append(remote, newTarget)
			return []object.LocalID{}, remote
		}
	case object.LocalID:
		localTarget = target.(object.LocalID)
	default:
		return nil, nil
	}
	temp, err := tx.LoadObjectField(localTarget, "@type")
	if err == object.NoObject { // this might be present in a cluster
		if c.IsSubserver() { // are we a cluster?
			// dead end at a possible cluster-local
			newTarget := NewPropRetryCluster(localTarget)
			return []object.LocalID{}, []PropRetryTarget{newTarget}
		} else {
			return nil, nil
		}
	} else if err != nil {
		return nil, nil
	}
	var targetType string
	switch temp.(type) {
	case string:
		targetType = temp.(string)
	default:
		return nil, nil
	}
	if IsCollection(targetType) && recurse > 0 {
		targetObj, err := tx.LoadObject(localTarget)
		if err != nil {
			return nil, nil
		}
		items, _, err := object.GetRefObjList(targetObj, "items")
		if err != nil {
			return nil, nil
		}
		for _, item := range items {
			switch item.(type) {
			case string:
				subLocal, subRemote := c.sortPropagationTarget(
					tx, item, recurse-1)
				if subLocal != nil {
					local = append(local, subLocal...)
				}
				if subRemote != nil {
					remote = append(remote, subRemote...)
				}
			}
		}
	} else if IsActor(targetType) {
		local = append(local, localTarget)
	} // Not Actor, not Collection. Skip over.
	return local, remote
}

// Returns the owning actor of a native object.
func (c *Core) NativeObjectOwner(tx Transaction, objectID object.LocalID) (actorID object.LocalID, err error) {
	temp, err := tx.LoadObjectField(objectID, "attributedTo")
	if err != nil {
		return object.LocalID(""), err
	}
	switch temp.(type) {
	case object.LocalID:
		return temp.(object.LocalID), nil
	default:
		return object.LocalID(""), object.BrokenObject
	}
}

func (c *Core) distributeReplies(tx Transaction, replyID interface{}, targetActor object.LocalID, replyTos interface{}) (err error) {
	// force into list form for ease of handling
	var targets []interface{}
	switch replyTos.(type) {
	case []interface{}:
		targets = replyTos.([]interface{})
	default:
		targets = []interface{}{replyTos}
	}
	// Loop on targets
	for _, temp := range targets {
		var target object.LocalID
		// if not valid type: skip
		fmt.Println("distributing to:", temp)
		switch temp.(type) {
		case string:
			globalTarget := temp.(string)
			target = object.ExtractNativeFromGlobal(globalTarget, c.Hostname,
				c.Port)
			fmt.Println("  string:", target)
			if target == "" {
				continue
			}
		case object.LocalID:
			fmt.Println("  localid")
			target = temp.(object.LocalID)
		default:
			fmt.Println("  default")
			continue
		}
		fmt.Println("adding reply:", target, replyID)
		// #### This is dummied out because I don't know why it was ever there
		owningActor, _ := c.NativeObjectOwner(tx, target)
		// We can ignore errors here: the comparison will skip any problems
		if owningActor != targetActor {
			fmt.Println("target failure:", owningActor, targetActor)
			continue
		}
		// Have a match; add it.
		err = tx.ObjcolAdd(target, "replies", replyID)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Core) PendingPropagations() (objects map[object.LocalID]map[string]interface{}, props map[object.LocalID][]PropRetryTarget, err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return nil, nil, err
	}
	defer tx.EndTransaction(&err)
	// grab all IDs
	objIDs, err := tx.GetPropIDs()
	if err != nil {
		return nil, nil, err
	}
	objects = make(map[object.LocalID]map[string]interface{}, len(objIDs))
	props = make(map[object.LocalID][]PropRetryTarget, len(objIDs))
	// for ID
	for _, objID := range objIDs {
		object, err := tx.GetPropObject(objID)
		if err != nil {
			// skip, or bail?
			continue
		}
		GlobalizeObject(object, c.Hostname, c.Port)
		objects[objID] = object
		targets, err := tx.GetPropTargets(objID)
		if err != nil {
			// skip, or bail?
			continue
		}
		props[objID] = targets
	}
	return objects, props, nil
}

func (c *Core) UpdatePropagation(updates map[object.LocalID][]PropRetryTarget, removals map[object.LocalID][]string) (err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	// == updates
	for objID, targets := range updates {
		for _, target := range targets {
			tx.UpdatePropTarget(objID, target)
		}
	}
	// == removals
	for objID, targets := range removals {
		for _, targetURL := range targets {
			tx.RemovePropTarget(objID, targetURL)
		}
	}
	return nil
}

// --- Accounts --- //

func (c *Core) NewAccount(acct Account) (err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	err = tx.StoreAccount(acct)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) LoadAccount(user string) (acct Account, err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return Account{}, err
	}
	defer tx.EndTransaction(&err)
	acct, err = tx.LoadAccount(user)
	if err != nil {
		return Account{}, nil
	}
	// globalize ActorID and use it to fill in ActorURL
	if c.Port == 0 {
		acct.ActorURL = object.GlobalIDFromLocalID(acct.ActorID, c.Hostname)
	} else {
		acct.ActorURL = object.GlobalIDFromLocalIDPorted(acct.ActorID,
			c.Hostname, c.Port)
	}
	return acct, nil
}

func (c *Core) UpdateAccount(user string, acct Account) (err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	err = tx.UpdateAccount(user, acct)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) DeleteAccount(user string) (err error) {
	tx, err := c.db.StartTransaction()
	if err != nil {
		return err
	}
	defer tx.EndTransaction(&err)
	err = tx.DeleteAccount(user)
	if err != nil {
		return err
	}
	return nil
}
