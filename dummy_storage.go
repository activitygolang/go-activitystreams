package activitystreams

import (
	//"fmt"
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
)

type DummyStorage struct {
	objects     map[object.LocalID]map[string]interface{}
	collections map[object.LocalID]map[string][]interface{}
	changesets  map[string][]Change
	protected   map[object.LocalID]bool
	cache       map[string]map[string]interface{}
	propObj     map[object.LocalID]map[string]interface{}
	propTgt     map[object.LocalID]map[string]PropRetryTarget
	accounts    map[string]Account
	closed      bool
}

func NewDummyStorage() (d *DummyStorage) {
	d = &DummyStorage{}
	d.objects = make(map[object.LocalID]map[string]interface{}, 0)
	d.collections = make(map[object.LocalID]map[string][]interface{}, 0)
	d.changesets = make(map[string][]Change, 0)
	d.protected = make(map[object.LocalID]bool, 0)
	d.cache = make(map[string]map[string]interface{}, 0)
	d.propObj = make(map[object.LocalID]map[string]interface{}, 0)
	d.propTgt = make(map[object.LocalID]map[string]PropRetryTarget, 0)
	d.accounts = make(map[string]Account, 0)
	d.closed = false
	return d
}

func (d *DummyStorage) Close() (err error) {
	d.closed = true
	return nil
}

func (d *DummyStorage) checkClosed() (err error) {
	if d.closed == true {
		return ClosedStorage
	} else {
		return nil
	}
}

func (d *DummyStorage) StartTransaction() (t Transaction, err error) {
	if err = d.checkClosed(); err != nil {
		return nil, err
	}
	t = &dummyTransaction{db: d}
	return t, nil
}

type dummyTransaction struct {
	db     *DummyStorage
	closed bool
}

func (t *dummyTransaction) checkClosed() (err error) {
	if t.closed == true {
		return InvalidTransaction
	} else if err = t.db.checkClosed(); err != nil {
		return err
	} else {
		return nil
	}
}

// Not actually functional in DummyStorage
func (t *dummyTransaction) RevertTransaction() (err error) {
	return t.EndTransaction(nil)
}

func (t *dummyTransaction) EndTransaction(errIn *error) (err error) {
	if err = t.checkClosed(); err != nil {
		return err
	}
	//t.db.changesets
	t.closed = true
	return nil
}

// ======= Primary Object Store -- object level =======
func (t *dummyTransaction) StoreObject(obj map[string]interface{}) (err error) {
	if err = t.checkClosed(); err != nil {
		return err
	}
	objID, err := object.GetLocalID(obj, "@id")
	if err == object.NoField {
		return object.NoObjID
	} else if err != nil {
		return err
	}
	if _, ok := t.db.objects[objID]; ok {
		return object.ObjectPresent
	}
	t.db.objects[objID] = obj
	return nil
}

func (t *dummyTransaction) LoadObject(objectID object.LocalID) (obj map[string]interface{}, err error) {
	if err = t.checkClosed(); err != nil {
		return nil, err
	}
	obj, ok := t.db.objects[objectID]
	if !ok {
		return nil, object.NoObject
	}
	return obj, nil
}

func (t *dummyTransaction) RemoveObject(objectID object.LocalID) (err error) {
	if err = t.checkClosed(); err != nil {
		return err
	}
	if _, ok := t.db.objects[objectID]; !ok {
		return object.NoObject
	}
	delete(t.db.objects, objectID)
	delete(t.db.collections, objectID)
	delete(t.db.protected, objectID)
	return nil
}

func (t *dummyTransaction) SetProtected(objectID object.LocalID, protected bool) (err error) {
	if _, ok := t.db.objects[objectID]; !ok {
		return object.NoObject
	}
	t.db.protected[objectID] = protected
	return nil
}

func (t *dummyTransaction) IsProtected(objectID object.LocalID) (protected bool, err error) {
	_, ok := t.db.objects[objectID]
	if !ok {
		return false, object.NoObject
	}
	protected, ok = t.db.protected[objectID]
	if !ok {
		return false, nil
	} else {
		return protected, nil
	}
}

func (t *dummyTransaction) BasicObjectData(objectID object.LocalID) (objType string, owner object.LocalID, err error) {
	obj, ok := t.db.objects[objectID]
	if !ok {
		return "", object.LocalID(""), object.NoObject
	}
	objType, err = object.GetStr(obj, "@type")
	if err != nil {
		return "", object.LocalID(""), err
	}
	owner, err = object.GetLocalID(obj, "attributedTo")
	if err != nil {
		return "", object.LocalID(""), err
	}
	return objType, owner, nil
}

func (t *dummyTransaction) Exists(objectID object.LocalID) (exists bool, err error) {
	_, ok := t.db.objects[objectID]
	return ok, nil
}

// ======= Primary Object Store -- field level =======
func (t *dummyTransaction) LoadObjectField(objectID object.LocalID, fieldName string) (data interface{}, err error) {
	if err = t.checkClosed(); err != nil {
		return nil, err
	}
	// get object
	obj, ok := t.db.objects[objectID]
	if !ok || obj == nil {
		return nil, object.NoObject
	}
	// get field
	var field interface{}
	field, ok = obj[fieldName]
	if !ok {
		return nil, object.NoField
	}
	return field, nil
}

func (t *dummyTransaction) ListAppend(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	if err = t.checkClosed(); err != nil {
		return err
	}
	// get object
	obj, ok := t.db.objects[objectID]
	if !ok || obj == nil {
		return object.NoObject
	}
	// get field
	var field interface{}
	field, ok = obj[fieldName]
	if !ok {
		// no field, add it
		field = []interface{}{}
	}
	// check for scalar data
	switch field.(type) {
	case []interface{}:
		// Already a list
	default:
		// Scalar, change to list
		field = []interface{}{field}
	}
	// change field
	field = append(field.([]interface{}), data)
	// store
	obj[fieldName] = field
	t.db.objects[objectID] = obj
	return nil
}

func (t *dummyTransaction) ListRemove(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	if err = t.checkClosed(); err != nil {
		return err
	}
	// Load list
	obj, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	list, ok := obj[fieldName]
	if !ok {
		return object.NoField
	}
	// scan list
	newList := make([]interface{}, 0)
	switch list.(type) {
	case []interface{}:
		for _, item := range list.([]interface{}) {
			if !reflect.DeepEqual(item, data) {
				newList = append(newList, item)
			}
		}
		obj[fieldName] = newList
		t.db.objects[objectID] = obj
		return nil
	default:
		return object.BadType
	}
}

func (t *dummyTransaction) AddField(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	// Check that object exists
	obj, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	// Check that field doesn't already exist
	_, ok = obj[fieldName]
	if ok {
		return DuplicateItem
	}
	// Set object field
	obj[fieldName] = data
	t.db.objects[objectID] = obj
	return nil
}

func (t *dummyTransaction) UpdateField(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	// Check that object exists
	obj, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	// Check that field exists
	_, ok = obj[fieldName]
	if !ok {
		return object.NoField
	}
	// Store new field value
	obj[fieldName] = data
	t.db.objects[objectID] = obj
	return nil
}

func (t *dummyTransaction) RemoveField(objectID object.LocalID, fieldName string) (err error) {
	// Check that object exists
	obj, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	// Check that field exists
	_, ok = obj[fieldName]
	if !ok {
		return object.NoField
	}
	delete(obj, fieldName)
	t.db.objects[objectID] = obj
	return nil
}

func (t *dummyTransaction) ValueInObject(objectID object.LocalID, fieldName string, data interface{}) (found bool, err error) {
	// Get object
	obj, ok := t.db.objects[objectID]
	if !ok {
		return false, object.NoObject
	}
	// Get field
	field, ok := obj[fieldName]
	if !ok {
		return false, object.NoField
	}
	// look for value
	fieldList := SlicifyScalar(field)
	for _, item := range fieldList {
		if reflect.DeepEqual(data, item) {
			return true, nil
		}
	}
	return false, nil
}

// ======= Ancilary collection handling =======

func (t *dummyTransaction) ObjcolAdd(objectID object.LocalID, fieldName string, value interface{}) (err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	// Check for existing object entry
	obj, ok := t.db.collections[objectID]
	if !ok {
		obj = make(map[string][]interface{}, 0)
	}
	// Check for existing field entry
	col, ok := obj[fieldName]
	if !ok {
		col = make([]interface{}, 0)
	}
	// We don't check for duplicates in the dummy
	// Append to list
	col = append(col, value)
	obj[fieldName] = col
	t.db.collections[objectID] = obj
	return nil
}

func (t *dummyTransaction) ObjcolRM(objectID object.LocalID, fieldName string, value interface{}) (err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	// Check that object has collections
	obj, ok := t.db.collections[objectID]
	if !ok {
		return object.NoField
	}
	// Check that field has an entry
	col, ok := obj[fieldName]
	if !ok {
		return object.NoField
	}
	newCol := make([]interface{}, 0)
	// Look for delete target and remove; may appear multiple times
	// Copy inefficient. Doesn't matter for dummy.
	for _, item := range col {
		if reflect.DeepEqual(item, value) {
			continue
		}
		newCol = append(newCol, item)
	}
	obj[fieldName] = newCol
	t.db.collections[objectID] = obj
	return nil
}

func (t *dummyTransaction) ObjcolGetAllField(objectID object.LocalID, fieldName string) (values []interface{}, err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return nil, object.NoObject
	}
	// Check that object has collections
	obj, ok := t.db.collections[objectID]
	if !ok {
		return nil, object.NoField
	}
	// Check that field has an entry
	col, ok := obj[fieldName]
	if !ok {
		return nil, object.NoField
	}
	return col, nil
}

func (t *dummyTransaction) ObjcolGetFields(objectID object.LocalID) (fields []string, err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return nil, object.NoObject
	}
	// Check that object has collections
	obj, ok := t.db.collections[objectID]
	if !ok {
		return nil, object.NoField
	}
	// Build field list
	for key, _ := range obj {
		fields = append(fields, key)
	}
	return fields, nil
}

func (t *dummyTransaction) ObjcolRMAll(objectID object.LocalID) (err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	delete(t.db.collections, objectID)
	return nil
}

func (t *dummyTransaction) ObjcolGetAll(objectID object.LocalID) (data map[string][]interface{}, err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return nil, object.NoObject
	}
	data, ok = t.db.collections[objectID]
	if !ok {
		return map[string][]interface{}{}, nil
	}
	return data, nil
}

func (t *dummyTransaction) ObjcolSet(objectID object.LocalID, data map[string][]interface{}) (err error) {
	// Check that object exists
	_, ok := t.db.objects[objectID]
	if !ok {
		return object.NoObject
	}
	t.db.collections[objectID] = data
	return nil
}

// ======= Changeset Handling =======
func (t *dummyTransaction) StoreChangeset(changeID string, changeset []Change) (err error) {
	t.db.changesets[changeID] = changeset
	return nil
}

func (t *dummyTransaction) LoadChangeset(changeID string) (changeset []Change, err error) {
	changeset, ok := t.db.changesets[changeID]
	if !ok {
		return nil, NoChangeset
	}
	return changeset, nil
}

// ======= Cache Handling =======
func (t *dummyTransaction) StoreCache(obj map[string]interface{}) (err error) {
	objID, err := object.GetStr(obj, "@id")
	if err == object.NoField {
		return object.NoObjID
	} else if err != nil {
		return err
	}
	t.db.cache[objID] = obj
	return nil
}

func (t *dummyTransaction) LoadCache(objID string) (obj map[string]interface{}, err error) {
	obj, ok := t.db.cache[objID]
	if !ok {
		return nil, object.NoObject
	}
	return obj, nil
}

// ======= Propagation Handling =======
func (t *dummyTransaction) StoreProp(obj map[string]interface{}, targets []PropRetryTarget) (err error) {
	objID, err := object.GetLocalID(obj, "@id")
	if err == object.NoField {
		return object.NoObjID
	} else if err != nil {
		return err
	}
	t.db.propObj[objID] = obj
	t.db.propTgt[objID] = make(map[string]PropRetryTarget, 0)
	for _, target := range targets {
		t.db.propTgt[objID][target.Url] = target
	}
	return nil
}

func (t *dummyTransaction) GetPropIDs() (ids []object.LocalID, err error) {
	numProps := len(t.db.propObj)
	ids = make([]object.LocalID, numProps)
	i := 0
	for id, _ := range t.db.propObj {
		ids[i] = id
		i++
	}
	return ids, nil
}

func (t *dummyTransaction) GetPropObject(objID object.LocalID) (obj map[string]interface{}, err error) {
	obj, ok := t.db.propObj[objID]
	if !ok {
		return nil, object.NoObject
	}
	return obj, nil
}

func (t *dummyTransaction) GetPropTargets(objID object.LocalID) (targets []PropRetryTarget, err error) {
	temp, ok := t.db.propTgt[objID]
	if !ok {
		return nil, object.NoObject
	}
	targets = make([]PropRetryTarget, 0)
	for _, target := range temp {
		targets = append(targets, target)
	}
	return targets, nil
}

func (t *dummyTransaction) UpdatePropTarget(objID object.LocalID, target PropRetryTarget) (err error) {
	targets, ok := t.db.propTgt[objID]
	if !ok {
		return object.NoObject
	}
	targets[target.Url] = target
	t.db.propTgt[objID] = targets
	return nil
}

func (t *dummyTransaction) RemovePropTarget(objID object.LocalID, targetURL string) (err error) {
	targets, ok := t.db.propTgt[objID]
	if !ok {
		return object.NoObject
	}
	delete(targets, targetURL)
	// check target count. If none remove the object
	if len(targets) > 0 {
		t.db.propTgt[objID] = targets
	} else {
		delete(t.db.propTgt, objID)
		delete(t.db.propObj, objID)
	}
	return nil
}

// ======= Account Data =======
func (t *dummyTransaction) StoreAccount(acct Account) (err error) {
	t.db.accounts[acct.User] = acct
	return nil
}

func (t *dummyTransaction) LoadAccount(user string) (acct Account, err error) {
	acct, ok := t.db.accounts[user]
	if !ok {
		return Account{}, object.NoObject
	} else {
		return acct, nil
	}
}

func (t *dummyTransaction) UpdateAccount(user string, acct Account) (err error) {
	// check it exists
	if _, err := t.LoadAccount(user); err == object.NoObject {
		return object.NoObject
	}
	// delete old version
	t.DeleteAccount(user)
	// store new
	t.StoreAccount(acct)
	return nil
}

func (t *dummyTransaction) DeleteAccount(user string) (err error) {
	delete(t.db.accounts, user)
	return nil
}
