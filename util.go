package activitystreams

import (
	"reflect"
	"time"
	//"fmt"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

// Fields that get assigned to IDs by the Core, which doesn't know anything
// about the host. These fields have to be translated on the fly.
var globalizationFields = []string{"@id", "id", "attributedTo", "object",
	"items"}

func MakeNowTimestamp() (datetime string) {
	now := time.Now()
	datetime = now.Format(time.RFC3339Nano)
	return datetime
}

// Synchronizes a field between two objects.
func SyncFields(objA map[string]interface{}, objB map[string]interface{}, fieldName string) {
	// Extract raw data of both fields
	fieldA, okA := objA[fieldName]
	fieldB, okB := objB[fieldName]
	// If both are blank there is nothing to sync
	if okA == false && okB == false {
		return
	}
	// If one is blank then set to an empty list
	if okA == false {
		fieldA = []interface{}{}
	} else if okB == false {
		fieldB = []interface{}{}
	}
	// Check for scalar values
	sliceA := SlicifyScalar(fieldA)
	sliceB := SlicifyScalar(fieldB)
	// Merge fields
	union := make([]interface{}, 0)
	union = listExtractor(sliceA, union)
	union = listExtractor(sliceB, union)
	// Replace merged fields
	objA[fieldName] = union
	objB[fieldName] = union
}

// Extracts every unique element from source that is not present in union
func listExtractor(source []interface{}, union []interface{}) []interface{} {
	for _, item := range source {
		// already exists?
		dup := false
		for _, comp := range union {
			if reflect.DeepEqual(item, comp) {
				dup = true
				break
			}
		}
		if dup == false {
			union = append(union, item)
		}
	}
	return union
}

// Forces data to be a list. Used to simplify code that wants to deal with
// lists and may be passed single items.
func SlicifyScalar(data interface{}) []interface{} {
	kind := reflect.TypeOf(data).Kind()
	if kind == reflect.Slice {
		switch data.(type) {
		case []interface{}:
			return data.([]interface{})
		default:
			// TODO: Convert other slice types to []interface{}
			return nil
		}
	} else {
		// Must be a scalar
		return []interface{}{data}
	}
}

// Recursively trawls an object and replaces all LocalIDs with global IDs.
func GlobalizeObject(obj map[string]interface{}, hostname string, port uint16) {
	// Loop through fields
	for key, value := range obj {
		// switch field type
		switch value.(type) {
		case object.LocalID:
			obj[key] = GlobalizeLocal(value.(object.LocalID), hostname, port)
		case []interface{}:
			GlobalizeList(value.([]interface{}), hostname, port)
		case map[string]interface{}:
			GlobalizeObject(value.(map[string]interface{}), hostname, port)
		}
	}
}

// Recursively trawls a list and replaces all LocalIDs with globalIDs.
func GlobalizeList(list []interface{}, hostname string, port uint16) {
	for index, item := range list {
		switch item.(type) {
		case object.LocalID:
			list[index] = GlobalizeLocal(item.(object.LocalID), hostname, port)
		case map[string]interface{}:
			GlobalizeObject(item.(map[string]interface{}), hostname, port)
		case []interface{}:
			GlobalizeList(item.([]interface{}), hostname, port)
		}
	}
}

// Converts a LocalID to a global ID, taking ports or lack therof into account.
func GlobalizeLocal(value object.LocalID, hostname string, port uint16) string {
	if port == 0 {
		return object.GlobalIDFromLocalID(value, hostname)
	} else {
		return object.GlobalIDFromLocalIDPorted(value, hostname, port)
	}
}

// Constructs the Tombstone for an object that is being deleted.
func CarveTombstone(cadaver map[string]interface{}) (tombstone map[string]interface{}) {
	// Always present fields
	tombstone = map[string]interface{}{
		"@type":      "Tombstone",
		"@id":        cadaver["@id"],
		"formerType": cadaver["@type"],
		"deleted":    MakeNowTimestamp(),
	}
	if tmp, ok := cadaver["to"]; ok {
		tombstone["to"] = tmp
	}
	if tmp, ok := cadaver["bto"]; ok {
		tombstone["bto"] = tmp
	}
	if tmp, ok := cadaver["cc"]; ok {
		tombstone["cc"] = tmp
	}
	if tmp, ok := cadaver["bcc"]; ok {
		tombstone["bcc"] = tmp
	}
	if tmp, ok := cadaver["audience"]; ok {
		tombstone["audience"] = tmp
	}
	return tombstone
}

// Extracts propagation targets from the standard locations of an object.
func ExtractCorePropagation(obj map[string]interface{}, sourceActor object.LocalID, host string, port uint16) (targets []interface{}) {
	targets = make([]interface{}, 0)
	dedup := make(map[string]bool, 0)
	extract := func(source interface{}) {
		var sourceList []interface{}
		switch source.(type) {
		case []interface{}:
			sourceList = source.([]interface{})
		case interface{}:
			sourceList = []interface{}{source}
		}
		for _, item := range sourceList {
			// dedupValue is used so we can directly compare local targets
			// to globals in the dedup code.
			dedupValue := ""
			switch typedV := item.(type) {
			case string:
				dedupValue = typedV
			case object.LocalID:
				dedupValue = string(typedV)
			}
			if dedupValue == "" {
				continue // Invalid type
			}
			if _, ok := dedup[dedupValue]; ok {
				continue // Already saw this
			}
			if !object.CompareIDsNative(item, sourceActor, host, port) {
				targets = append(targets, item)
				dedup[dedupValue] = true
			}
		}
	}
	if temp, ok := obj["to"]; ok {
		extract(temp)
	}
	if temp, ok := obj["bto"]; ok {
		extract(temp)
	}
	if temp, ok := obj["cc"]; ok {
		extract(temp)
	}
	if temp, ok := obj["bcc"]; ok {
		extract(temp)
	}
	if temp, ok := obj["audience"]; ok {
		extract(temp)
	}
	return targets
}

var ProtectedFields = []string{"@id", "id", "@type", "type", "attributedTo"}

// Update non-protected fields of an object from a diff.
func UpdateNormalObject(obj map[string]interface{}, diff map[string]interface{}) (updated map[string]interface{}) {
	updated = make(map[string]interface{}, 0)
	for key, value := range obj {
		updated[key] = value
	}
KeyLoop:
	for key, value := range diff {
		for _, prot := range ProtectedFields {
			if prot == key {
				continue KeyLoop
			}
		}
		if value == nil {
			delete(updated, key)
		} else {
			updated[key] = value
		}
	}
	return updated
}

var ActorProtectedFields = []string{"outbox", "inbox", "liked", "followers",
	"following"}

// Update non-protected fields of an actor from a diff.
func UpdateActor(actor map[string]interface{}, diff map[string]interface{}) (updated map[string]interface{}) {
	filteredDiff := make(map[string]interface{}, 0)
KeyLoop:
	for key, value := range diff {
		for _, prot := range ActorProtectedFields {
			if prot == key {
				continue KeyLoop
			}
		}
		// Copy everything *not* actor-protected
		filteredDiff[key] = value
	}
	return UpdateNormalObject(actor, filteredDiff)
}

func IsActor(objType string) (actor bool) {
	switch objType {
	case "Person":
		fallthrough
	case "Group":
		fallthrough
	case "Application":
		fallthrough
	case "Service":
		fallthrough
	case "Organization":
		return true
	default:
		return false
	}
}

func IsCollection(objType string) (collection bool) {
	switch objType {
	case "Collection":
		fallthrough
	case "OrderedCollection":
		return true
	default:
		return false
	}
}

var Activities = []string{"Accept", "Add", "Announce", "Arrive", "Block",
	"Create", "Delete", "Dislike", "Flag", "Follow", "Ignore", "Invite",
	"Join", "Leave", "Like", "Listen", "Move", "Offer", "Question", "Reject",
	"Read", "Remove", "TentativeReject", "TentativeAccept", "Travel", "Undo",
	"Update", "View"}

func IsActivity(objType string) (activity bool) {
	for _, activity := range Activities {
		if objType == activity {
			return true
		}
	}
	return false
}

func IsBlocked(blockList []interface{}, source string) (blocked bool) {
	for _, blockItem := range blockList {
		switch blockItem.(type) {
		case string:
			if source == blockItem.(string) {
				return true
			}
		}
	}
	return false
}

// Wrap a bare object in a Create activity.
func CreateWrap(object map[string]interface{}) (activity map[string]interface{}) {
	activity = map[string]interface{}{"@type": "Create", "object": object}
	return activity
}
