package activitystreams

import (
	"testing"
	"reflect"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func TestSyncFields(t *testing.T) {
	a := map[string]interface{}{"blankA":"a", "scalar":1,
		"normal":[]interface{}{10, 20}}
	b := map[string]interface{}{"blankB":"b", "scalar":2,
		"normal":[]interface{}{20, 30}}
	// Test no fields
	SyncFields(a, b, "none")
	fieldA, okA := a["none"]
	fieldB, okB := b["none"]
	if okA || okB {
		t.Fatal("SyncFields no field error:", fieldA, fieldB, okA, okB)
	}
	// Test A blank
	SyncFields(a, b, "blankA")
	fieldA, okA = a["blankA"]
	fieldB, okB = b["blankA"]
	if !okA || !okB || reflect.DeepEqual(fieldA, []interface{}{"a"}) != true || reflect.DeepEqual(fieldB, []interface{}{"a"}) != true {
		t.Fatal("SyncFields blank A error:", fieldA, fieldB, okA, okB)
	}
	// Test B blank
	SyncFields(a, b, "blankB")
	fieldA, okA = a["blankB"]
	fieldB, okB = b["blankB"]
	if !okA || !okB || reflect.DeepEqual(fieldA, []interface{}{"b"}) != true || reflect.DeepEqual(fieldB, []interface{}{"b"}) != true {
		t.Fatal("SyncFields blank B error:", fieldA, fieldB, okA, okB)
	}
	// Test scalar
	SyncFields(a, b, "scalar")
	fieldA, okA = a["scalar"]
	fieldB, okB = b["scalar"]
	if !okA || !okB || reflect.DeepEqual(fieldA, []interface{}{1, 2}) != true || reflect.DeepEqual(fieldB, []interface{}{1, 2}) != true {
		t.Fatal("SyncFields scalar error:", fieldA, fieldB, okA, okB)
	}
	// Test normal
	SyncFields(a, b, "normal")
	fieldA, okA = a["normal"]
	fieldB, okB = b["normal"]
	if !okA || !okB || reflect.DeepEqual(fieldA, []interface{}{10, 20, 30}) != true || reflect.DeepEqual(fieldB, []interface{}{10, 20, 30}) != true {
		t.Fatal("SyncFields normal error:", fieldA, fieldB, okA, okB)
	}
}

func Test_listExtractor(t *testing.T) {
	a := []interface{}{"foo", "bar", 23}
	b := []interface{}{"bar", "baz", 1, 23}
	union := make([]interface{}, 0)
	union = listExtractor(a, union)
	union = listExtractor(b, union)
	if reflect.DeepEqual(union, []interface{}{"foo", "bar", 23, "baz", 1}) != true {
		t.Fatal("listExtractor failure:", union)
	}
}

func TestSlicifyScalar(t *testing.T) {
	// Test scalar
	l := SlicifyScalar("foo")
	if reflect.DeepEqual(l, []interface{}{"foo"}) != true {
		t.Fatal("SlicifyScalar bad scalar:", l)
	}
	// Test []interface{}
	l = SlicifyScalar([]interface{}{"a", "b"})
		if reflect.DeepEqual(l, []interface{}{"a", "b"}) != true {
		t.Fatal("SlicifyScalar bad []interface{}:", l)
	}
}

func TestGlobalizeObject(t *testing.T) {
	obj := map[string]interface{}{"@id":object.LocalID("/foo"),
		"attributedTo":"https://other.test/baz",
		"object":map[string]interface{}{"id":object.LocalID("asdf")},
		"items":[]interface{}{object.LocalID("/a"), object.LocalID("/b")}}
	expectedSubObject := map[string]interface{}{"id":"https://quux.test/asdf"}
	expectedItems := []interface{}{"https://quux.test/a", "https://quux.test/b"}
	GlobalizeObject(obj, "quux.test", 0)
	if len(obj) != 4 {
		t.Fatal("GlobalizeObject failure:", obj)
	} else if obj["@id"].(string) != "https://quux.test/foo" {
		t.Fatal("GlobalizeObject failure:", obj)
	} else if obj["attributedTo"].(string) != "https://other.test/baz" {
		t.Fatal("GlobalizeObject failure:", obj)
	} else if !reflect.DeepEqual(obj["object"], expectedSubObject) {
		t.Fatal("GlobalizeObject failure:", obj)
	} else if !reflect.DeepEqual(obj["items"], expectedItems) {
		t.Fatal("GlobalizeObject failure:", obj)
	}
}

func TestGlobalizeList(t *testing.T) {
	lst := []interface{}{object.LocalID("/local"),
		map[string]interface{}{"a":1, "@id":object.LocalID("/foo")},
		[]interface{}{object.LocalID("/bar")}, 5}
	expectedSubObject := map[string]interface{}{"a":1,
		"@id":"https://quux.test:23/foo"}
	expectedSubList := []interface{}{"https://quux.test:23/bar"}
	GlobalizeList(lst, "quux.test", 23)
	if len(lst) != 4 {
		t.Fatal("GlobalizeList failure:", lst)
	} else if lst[0].(string) != "https://quux.test:23/local" {
		t.Fatal("GlobalizeList failure:", lst)
	} else if !reflect.DeepEqual(lst[1], expectedSubObject) {
		t.Fatal("GlobalizeList failure:", lst)
	} else if !reflect.DeepEqual(lst[2], expectedSubList) {
		t.Fatal("GlobalizeList failure:", lst, expectedSubList)
	} else if lst[3].(int) != 5 {
		t.Fatal("GlobalizeList failure:", lst)
	}
}

func TestGlobalizeLocal(t *testing.T) {
	// Test local, no port
	str := GlobalizeLocal("/foo", "quux.test", 0)
	if str != "https://quux.test/foo" {
		t.Fatal("GlobalizeLocal local no port failure:", str)
	}
	// Test local, ported
	str = GlobalizeLocal("/foo", "quux.test", 23)
	if str != "https://quux.test:23/foo" {
		t.Fatal("GlobalizeLocal local ported failure:", str)
	}
}

func TestCarveTombstone(t *testing.T) {
	cadaver := map[string]interface{}{
		"@type":"Note", "@id": "/foo/1234",
	}
	// Test
	tomb := CarveTombstone(cadaver)
	if len(tomb) != 4 {
		t.Fatal("CarveTombstone bad length:", tomb)
	} else if d, e := object.GetStr(tomb, "@type"); d != "Tombstone" || e != nil {
		t.Fatal("CarveTombstone bad @type:", d, e)
	} else if d, e := object.GetStr(tomb, "@id"); d != "/foo/1234" || e != nil {
		t.Fatal("CarveTombstone bad @id:", d, e)
	} else if d, e := object.GetStr(tomb, "formerType"); d != "Note" || e != nil {
		t.Fatal("CarveTombstone bad formerType:", d, e)
	} else if d, e := object.GetStr(tomb, "deleted"); len(d) < 30 || e != nil {
		t.Fatal("CarveTombstone bad deleted:", d, e)
	}
}

func TestExtractCorePropagation(t *testing.T) {
	obj := map[string]interface{}{"@id":"jabber", "to":"foo", "bto":23,
		"cc":[]interface{}{"bar", "baz", "https://foo.test/yes"},
		"bcc":object.LocalID("quux"), "audience":"https://quux.test/fred"}
	prop := ExtractCorePropagation(obj, "baz", "foo.test", 0)
	expected := []interface{}{"foo", "bar", "baz", "https://foo.test/yes",
		object.LocalID("quux"), "https://quux.test/fred"}
	if !reflect.DeepEqual(prop, expected) {
		t.Fatal("ExtractCorePropagation failure:", prop)
	}
}

func TestUpdateNormalObject(t *testing.T) {
	obj := map[string]interface{}{"@id":"a", "id":"b", "@type":"c", "type":"d",
		"attributedTo":"e", "foo":"f", "bar":"g", "baz":"h"}
	diff := map[string]interface{}{"@id":1, "id":2, "@type":3, "type":4,
		"attributedTo":5, "bar":"change", "baz":nil}
	expected := map[string]interface{}{"@id":"a", "id":"b", "@type":"c",
		"type":"d", "attributedTo":"e", "foo":"f", "bar":"change"}
	updated := UpdateNormalObject(obj, diff)
	if !reflect.DeepEqual(updated, expected) {
		t.Fatal("UpdateNormalObject failure:", updated)
	}
}

func TestUpdateActor(t *testing.T) {
	obj := map[string]interface{}{"@id":"a", "id":"b", "@type":"c", "type":"d",
		"attributedTo":"e", "foo":"f", "bar":"g", "baz":"h", "outbox":"i",
		"inbox":"j", "liked":"k", "followers":"l", "following":"m"}
	diff := map[string]interface{}{"@id":1, "id":2, "@type":3, "type":4,
		"attributedTo":5, "bar":"change", "baz":nil, "outbox":6,
		"inbox":7, "liked":8, "followers":9, "following":10}
	expected := map[string]interface{}{"@id":"a", "id":"b", "@type":"c",
		"type":"d", "attributedTo":"e", "foo":"f", "bar":"change",
		"outbox":"i", "inbox":"j", "liked":"k", "followers":"l",
		"following":"m"}
	updated := UpdateActor(obj, diff)
	if !reflect.DeepEqual(updated, expected) {
		t.Fatal("UpdateActor failure:", updated)
	}
}
