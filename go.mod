module gitlab.com/activitygolang/go-activitystreams

go 1.12

require (
	github.com/mattn/go-sqlite3 v1.14.6
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
