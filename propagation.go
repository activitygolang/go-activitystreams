package activitystreams

import (
	//"fmt"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func (c *Core) activityBeenSighted(tx Transaction, activity map[string]interface{}) (sighted bool, err error) {
	activityID, err := object.GetStr(activity, "@id")
	if err != nil {
		return false, err
	}
	_, err = tx.LoadCache(activityID)
	if err == nil {
		// Already seen this Activity
		return true, nil
	} else if err != object.NoObject {
		// Real error, bail
		return false, err
	} else {
		// Not seen
		return false, nil
	}
}

// Checks for the ghost replies problem and generates propagation targets
// if it is present.
func (c *Core) ghostReplyTargets(tx Transaction, activity map[string]interface{}) (targets []interface{}, err error) {
	// How do we trigger the ghost-replies fix?
	// All of the following must be true:
	// * we need to see if we have seen this activity before
	//    -- will be checked by the layer above
	// * we need to check top level to/cc/audience for Collections that we own
	// * we need to check inReplyTo/object/target/tag for objects we own
	//     recurse on these N times. DO NOT pick up more to/cc/audience
	// ======== CHECK TARGETS OWNED =========
	unfilteredTargets := HasGhostTos(activity, c.Hostname, c.Port)
	if unfilteredTargets == nil {
		return nil, nil
	}
	// ========= CHECK reply-like-targeting ===========
	hasTarget := HasGhostTargets(tx, activity, c.Hostname, c.Port, RECURSE_MAX)
	if !hasTarget {
		return nil, nil
	}
	// At this point we know we need to propagate
	// Get anything local that is a Collection.
	// Remotes don't matter: any server can see them.
	for _, target := range unfilteredTargets {
		// check for Collection
		objType, _, err := tx.BasicObjectData(target)
		if err != nil || IsCollection(objType) {
			// if err, add anyway so it doesn't get dropped
			// extra adds won't hurt, they just get dropped eventually
			targets = append(targets, target)
		}
	}
	return targets, nil
}

// Checks if this server owns anything that is in the propagation fields.
func HasGhostTos(activity map[string]interface{}, host string, port uint16) (localTargets []object.LocalID) {
	extract := func(name string) {
		fieldData, _, err := object.GetRefObjList(activity, name)
		if err != nil {
			// May or may not be a real error, but we are done here.
			return
		}
		// loop through items, appending as we go
		for _, item := range fieldData {
			// No way for a LocalID to appear; this is a remote object.
			switch item.(type) {
			case string:
				localTarget := object.ExtractNativeFromGlobal(item.(string),
					host, port)
				if localTarget != "" {
					// append
					localTargets = append(localTargets, localTarget)
				}
			default:
				// skip, we don't care about anything that isn't an ID
				continue
			}
		}
	}
	// Extract all of the contents into the seen map
	extract("to")
	extract("cc")
	extract("audience")
	return localTargets
}

// Checks if this server owns anything in the target fields.
func HasGhostTargets(tx Transaction, activity map[string]interface{}, host string, port uint16, recursion uint) (hasTargets bool) {
	// loop on field contents
	check := func(name string) (found bool) {
		fieldData, _, err := object.GetRefObjList(activity, name)
		if err != nil {
			// May or may not be a real error, but we are done here.
			return false
		}
		for _, item := range fieldData {
			// fieldData, and thus item could have anything in them
			// We only extract strings and LocalIDs
			itemStr, ok := item.(string)
			local := object.LocalID("")
			if ok {
				// attempt to get a native ID
				local = object.ExtractNativeFromGlobal(itemStr, host, port)
			} else {
				local, ok = item.(object.LocalID)
				if !ok {
					continue
				}
			}
			if local != "" {
				if recursion > 0 {
					// get object
					targetObj, err := tx.LoadObject(local)
					if err == nil {
						hasTargets = HasGhostTargets(tx, targetObj, host, port,
							recursion-1)
						if hasTargets {
							return hasTargets
						}
					}
				}
				return true
			}
		}
		return false
	}
	hasTargets = check("inReplyTo")
	if hasTargets {
		return hasTargets
	}
	hasTargets = check("object")
	if hasTargets {
		return hasTargets
	}
	hasTargets = check("target")
	if hasTargets {
		return hasTargets
	}
	hasTargets = check("tag")
	return hasTargets
}
