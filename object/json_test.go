package object

import (
	"testing"
)

func TestEncodeDecodeJSON(t *testing.T) {
	start := map[string]interface{}{
		"foo":float64(3),
		"bar":map[string]interface{}{"baz":"quux?"}}
	byteVersion, err := EncodeJSON(start)
	if err != nil {
		t.Fatal("EncodeJSON error:", err)
	}
	end, err := DecodeJSON(byteVersion)
	if err != nil {
		t.Fatal("DecodeJSON error:", err)
	}
	if len(start) != len(end) {
		t.Fatal("Roundtrip length error:", start, end)
	}
	if start["foo"].(float64) != end["foo"].(float64) {
		t.Fatal("Roundtrip foo error:", start, end)
	}
	startSub := start["bar"].(map[string]interface{})
	endSub := start["bar"].(map[string]interface{})
	if len(startSub) != len(endSub) {
		t.Fatal("Roundtrip bar length error:", startSub, endSub)
	}
	if startSub["baz"] != endSub["baz"] {
		t.Fatal("Roundtrip bar.baz error:", startSub, endSub)
	}
}
