package object

import (
	"encoding/json"
	//"fmt"
)


// Converters between map and JSON string forms

// Tokenizes the raw JSON string into a dict which can be easily parsed.
func DecodeJSON(data []byte) (obj map[string]interface{}, err error) {
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}
	obj = tmp.(map[string]interface{})
	return obj, nil
}


// map to json

func EncodeJSON(obj map[string]interface{}) (data []byte, err error) {
	data, err = json.Marshal(obj)
	if err != nil {
		return nil, err
	}
	return data, nil
}
