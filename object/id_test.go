package object

import (
	"testing"
	//"fmt"
)

func TestEnforceLocalIDSanity(t *testing.T) {
	// Test no changes
	local := EnforceLocalIDSanity(LocalID("/foo/bar"))
	if local != LocalID("/foo/bar") {
		t.Fatal("EnforceLocalIDSanity no change error:", local)
	}
	// Test too many slashes
	local = EnforceLocalIDSanity(LocalID("////foo/bar"))
	if local != LocalID("/foo/bar") {
		t.Fatal("EnforceLocalIDSanity too many slashes error:", local)
	}
	// Test no slashes
	local = EnforceLocalIDSanity(LocalID("foo/bar"))
	if local != LocalID("/foo/bar") {
		t.Fatal("EnforceLocalIDSanity no slashes error:", local)
	}
}

func TestGlobalIDFromLocalIDPorted(t *testing.T) {
	global := GlobalIDFromLocalIDPorted("foo/bar", "ap.quux.test", 23)
	if global != "https://ap.quux.test:23/foo/bar" {
		t.Fatal("GlobalIDFromLocalIDPorted failure:", global)
	}
}

func TestGlobalIDFromLocalID(t *testing.T) {
	global := GlobalIDFromLocalID("foo/bar", "ap.quux.test")
	if global != "https://ap.quux.test/foo/bar" {
		t.Fatal("GlobalIDFromLocalID failure:", global)
	}
}

func TestLocalIDFromGlobalID(t *testing.T) {
	// Test working
	local, host, port, err := LocalIDFromGlobalID("https://foo.test:23/bar/q")
	if err != nil {
		t.Fatal("LocalIDFromGlobalID error:", err)
	} else if local != "/bar/q" {
		t.Fatal("LocalIDFromGlobalID localID failure:", local)
	} else if host != "foo.test" {
		t.Fatal("LocalIDFromGlobalID host failure:", host)
	} else if port != 23 {
		t.Fatal("LocalIDFromGlobalID port failure:", port)
	}
	// Test failure: local ID
	local, host, port, err = LocalIDFromGlobalID("/bar/q")
	if err != nil {
		t.Fatal("LocalIDFromGlobalID error:", err)
	} else if local != "/bar/q" {
		t.Fatal("LocalIDFromGlobalID localID failure:", local)
	} else if host != "" {
		t.Fatal("LocalIDFromGlobalID host failure:", host)
	} else if port != 0 {
		t.Fatal("LocalIDFromGlobalID port failure:", port)
	}
}

func TestIsIDGlobal(t *testing.T) {
	// Test global
	global := IsIDGlobal("https://foo.test:0/baz")
	if global != true {
		t.Fatal("IsIDGlobal global test failure:", global)
	}
	// Test local
	global = IsIDGlobal("/blah")
	if global != false {
		t.Fatal("IsIDGlobal local test failure:", global)
	}
}

func TestIsLocal(t *testing.T) {
	if IsLocal("foo") {
		t.Fatal("IsLocal string failure")
	}
	if IsLocal(3) {
		t.Fatal("IsLocal random type failure")
	}
	if !IsLocal(LocalID("foo")) {
		t.Fatal("IsLocal LocalID failure")
	}
}

func TestCompareIDGlobalLocal(t *testing.T) {
	// Test equal
	g := "https://foo.test:42/bar"
	l := LocalID("/bar")
	equal, host, port := CompareIDGlobalLocal(g, l)
	if equal != true && host != "foo.test" && port != 42 {
		t.Fatal("CompareIDGlobalLocal equal failure:", equal, host, port)
	}
	// Test unequal
	g = "https://foo.test:42/baz"
	equal, host, port = CompareIDGlobalLocal(g, l)
	if equal != false && host != "" && port != 0 {
		t.Fatal("CompareIDGlobalLocal unequal failure:", equal, host, port)
	}
	// Test error
	g = "blah*foo^test/bar"
	equal, host, port = CompareIDGlobalLocal(g, l)
	if equal != false && host != "" && port != 0 {
		t.Fatal("CompareIDGlobalLocal error failure:", equal, host, port)
	}
}

func TestCompareIDs(t *testing.T) {
	// Test invalid type a
	e, h, p := CompareIDs(1, "foo")
	if e != false || h != "" || p != 0 {
		t.Fatal("CompareIDs invalid A failure:", e, h, p)
	}
	// Test invalid type b
	e, h, p = CompareIDs("foo", 1)
	if e != false || h != "" || p != 0 {
		t.Fatal("CompareIDs invalid B failure:", e, h, p)
	}
	// Test (global, global) equal
	e, h, p = CompareIDs("https://foo.test/bar", "https://foo.test/bar")
	if e != true || h != "" || p != 0 {
		t.Fatal("CompareIDs global-global equal failure:", e, h, p)
	}
	// Test (global, global) unequal
	e, h, p = CompareIDs("https://foo.test/bar", "https://quux.test/bar")
	if e != false || h != "" || p != 0 {
		t.Fatal("CompareIDs global-global unequal failure:", e, h, p)
	}
	// Test (global, local) equal
	e, h, p = CompareIDs("https://foo.test:42/bar", LocalID("/bar"))
	if e != true || h != "foo.test" || p != 42 {
		t.Fatal("CompareIDs global-local equal failure:", e, h, p)
	}
	// Test (global, local) unequal
	e, h, p = CompareIDs("https://foo.test:42/baz", LocalID("/bar"))
	if e != false || h != "foo.test" || p != 42 {
		t.Fatal("CompareIDs global-local unequal failure:", e, h, p)
	}
	// Test (local, global) equal
	e, h, p = CompareIDs(LocalID("/bar"), "https://foo.test:42/bar")
	if e != true || h != "foo.test" || p != 42 {
		t.Fatal("CompareIDs local-global equal failure:", e, h, p)
	}
	// Test (local, global) unequal
	e, h, p = CompareIDs(LocalID("/bar"), "https://foo.test:42/baz")
	if e != false || h != "foo.test" || p != 42 {
		t.Fatal("CompareIDs local-global unequal failure:", e, h, p)
	}
	// Test (local, local) equal
	e, h, p = CompareIDs(LocalID("/bar"), LocalID("/bar"))
	if e != true || h != "" || p != 0 {
		t.Fatal("CompareIDs local-local equal failure:", e, h, p)
	}
	// Test (local, local) unequal
	e, h, p = CompareIDs(LocalID("/bar"), LocalID("/baz"))
	if e != false || h != "" || p != 0 {
		t.Fatal("CompareIDs local-local unequal failure:", e, h, p)
	}
}

func TestExtractNativeFromGlobal(t *testing.T) {
	// Test gibbersh
	local := ExtractNativeFromGlobal("jo78%&^$&%Fgoi", "foo.test", 42)
	if local != LocalID("") {
		t.Fatal("ExtractNativeFromGlobal failure:", local)
	}
	// Test non-native
	local = ExtractNativeFromGlobal("https://baz.test/quux", "foo.test", 42)
	if local != LocalID("") {
		t.Fatal("ExtractNativeFromGlobal non-native failure:", local)
	}
	// Test native
	local = ExtractNativeFromGlobal("https://foo.test:42/quux", "foo.test", 42)
	if local != LocalID("/quux") {
		t.Fatal("ExtractNativeFromGlobal native failure:", local)
	}
	// Test
	local = ExtractNativeFromGlobal("https://127.0.0.1:8081/secondman",
		"127.0.0.1", 8081)
	if local != LocalID("/secondman") {
		t.Fatal("ExtractNativeFromGlobal failure:", local)
	}
}
