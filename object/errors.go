package object

import (
	"errors"
)

// Errors
var ObjectPresent = errors.New("Object already exists in this location")
var NoObject = errors.New("Object is nil")
var NoField = errors.New("No field")
var NoObjID = errors.New("No @id field")
var NoObjType = errors.New("No @type field")
var NoObjOwner = errors.New("No attributedTo field")
var BadType = errors.New("Field type incorrect")
var BadSign = errors.New("Signed value in unsigned field")
var InvalidContext = errors.New("InvalidContext")
var BrokenObject = errors.New("Object is broken")
