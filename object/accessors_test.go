package object

import (
	"testing"
	"reflect"
)

func Test_preliminaryGet(t *testing.T) {
	obj := map[string]interface{}{"foo":"bar"}
	// Get basic valid
	value, err := preliminaryGet(obj, "foo")
	if err != nil || value != "bar" {
		t.Fatal("preliminaryGet valid error:", value, err)
	}
	// Get non-existent field
	value, err = preliminaryGet(obj, "quux")
	if err != NoField || value != nil {
		t.Fatal("preliminaryGet non-existent field error:", value, err)
	}
	// Get from null object
	value, err = preliminaryGet(nil, "foo")
	if err != NoObject || value != nil {
		t.Fatal("preliminaryGet nil object error:", value, err)
	}
}

func TestGetStr(t *testing.T) {
	obj := map[string]interface{}{"foo":"bar", "baz":5}
	// Get valid
	value, err := GetStr(obj, "foo")
	if err != nil || value != "bar" {
		t.Fatal("GetStr valid key error:", value, err)
	}
	// Get invalid type
	value, err = GetStr(obj, "baz")
	if err != BadType || value != "" {
		t.Fatal("GetStr invalid type error:", value, err)
	}
}

func TestGetLocalID(t *testing.T) {
	obj := map[string]interface{}{"foo":LocalID("bar"), "baz":"quux"}
	// Get valid
	value, err := GetLocalID(obj, "foo")
	if err != nil || value != LocalID("bar") {
		t.Fatal("GetLocalID valid key error:", value, err)
	}
	// Get invalid type
	value, err = GetLocalID(obj, "baz")
	if err != BadType || value != LocalID("") {
		t.Fatal("GetLocalID invalid type error:", value, err)
	}
}

func TestGetInt(t *testing.T) {
	obj := map[string]interface{}{"foo":-5.0, "baz":"bar"}
	// Get valid
	value, err := GetInt(obj, "foo")
	if err != nil || value != -5 {
		t.Fatal("GetInt valid key error:", value, err)
	}
	// Get invalid type
	value, err = GetInt(obj, "baz")
	if err != BadType || value != 0 {
		t.Fatal("GetInt invalid type error:", value, err)
	}
}

func TestGetUInt(t *testing.T) {
	obj := map[string]interface{}{"foo":5.0, "baz":"bar", "xyzzy":-5.0}
	// Get valid
	value, err := GetUInt(obj, "foo")
	if err != nil || value != 5 {
		t.Fatal("GetUInt valid key error:", value, err)
	}
	// Get signed field
	value, err = GetUInt(obj, "xyzzy")
	if err != BadSign || value != 0 {
		t.Fatal("GetUInt signed field error:", value, err)
	}
	// Get invalid type
	value, err = GetUInt(obj, "baz")
	if err != BadType || value != 0 {
		t.Fatal("GetUInt invalid type error:", value, err)
	}
}

func TestGetFloat(t *testing.T) {
	obj := map[string]interface{}{"foo":-5.0, "baz":"bar"}
	// Get valid
	value, err := GetFloat(obj, "foo")
	if err != nil || value != -5.0 {
		t.Fatal("GetFloat valid key error:", value, err)
	}
	// Get invalid type
	value, err = GetFloat(obj, "baz")
	if err != BadType || value != 0 {
		t.Fatal("GetFloat invalid type error:", value, err)
	}
}

func TestGetDatetime(t *testing.T) { // TODO
}

func TestGetDuration(t *testing.T) { // TODO
}

func TestGetRef(t *testing.T) {
	object := map[string]interface{}{"str":"foo", "invalid":5,
		"obj":map[string]interface{}{"a":1}, "lst":[]interface{}{"a"}}
	// Test string
	str, obj, lst, err := GetRef(object, "str")
	if str != "foo" || obj != nil || lst != nil || err != nil {
		t.Fatal("GetRef string failure:", str, obj, lst, err)
	}
	// Test object
	str, obj, lst, err = GetRef(object, "obj")
	if str != "" || !reflect.DeepEqual(obj, map[string]interface{}{"a":1}) || lst != nil || err != nil {
		t.Fatal("GetRef object failure:", str, obj, lst, err)
	}
	// Test list
	str, obj, lst, err = GetRef(object, "lst")
	if str != "" || obj != nil || !reflect.DeepEqual(lst, []interface{}{"a"}) || err != nil {
		t.Fatal("GetRef list failure:", str, obj, lst, err)
	}
	// Test invalid
	str, obj, lst, err = GetRef(object, "invalid")
	if str != "" || obj != nil || lst != nil || err != BadType {
		t.Fatal("GetRef BadType failure:", str, obj, lst, err)
	}
}

func TestGetRefObj(t *testing.T) {
	object := map[string]interface{}{"str":"foo", "inv":[]interface{}{"bar"},
		"obj":map[string]interface{}{"a":1}}
	// Test string
	str, obj, err := GetRefObj(object, "str")
	if str != "foo" || obj != nil || err != nil {
		t.Fatal("GetRefObj string failure:", str, obj, err)
	}
	// Test object
	str, obj, err = GetRefObj(object, "obj")
	if str != "" || !reflect.DeepEqual(obj, map[string]interface{}{"a":1}) || err != nil {
		t.Fatal("GetRefObj object failure:", str, obj, err)
	}
	// Test invalid lise
	str, obj, err = GetRefObj(object, "inv")
	if str != "" || obj != nil || err != BadType {
		t.Fatal("GetRefObj invalid list failure:", str, obj, err)
	}
}

func TestGetRefList(t *testing.T) {
	obj := map[string]interface{}{"foo":"a", "bar":[]string{"b", "c"},
		"baz":[]interface{}{"d", "e"}, "quux":[]interface{}{"f", 3},
		"asdf":3}
	// Get valid bare scalar
	value, scalar, err := GetRefList(obj, "foo")
	if err != nil || !scalar || reflect.DeepEqual(value, []string{"a"}) != true {
		t.Fatal("GetRefList valid scalar error:", value, scalar, err)
	}
	// Get valid string list
	value, scalar, err = GetRefList(obj, "bar")
	if err != nil || scalar || reflect.DeepEqual(value, []string{"b", "c"}) != true {
		t.Fatal("GetRefList valid string list error:", value, scalar, err)
	}
	// Get valid interface list
	value, scalar, err = GetRefList(obj, "baz")
	if err != nil || scalar || reflect.DeepEqual(value, []string{"d", "e"}) != true {
		t.Fatal("GetRefList valid interface list error:", value, scalar, err)
	}
	// Get invalid interface list
	value, scalar, err = GetRefList(obj, "quux")
	if err != BadType || scalar || value != nil {
		t.Fatal("GetRefList invalid interface list error:", value, scalar, err)
	}
	// Get invalid type
	value, scalar, err = GetRefList(obj, "asdf")
	if err != BadType || scalar || value != nil {
		t.Fatal("GetRefList invalid type error:", value, scalar, err)
	}
}

func TestGetRefObjList(t *testing.T) {
	obj := map[string]interface{}{"foo":"a",
		"bar":map[string]interface{}{"b":"c"}, "baz":[]interface{}{"d", "e"},
		"quux":3}
	// Get valid bare string
	value, bare, err := GetRefObjList(obj, "foo")
	if err != nil || !bare || !reflect.DeepEqual(value, []interface{}{"a"}) {
		t.Fatal("GetRefObjList valid string failure:", value, bare, err)
	}
	// Get valid bare object
	value, bare, err = GetRefObjList(obj, "bar")
	if err != nil || !bare || !reflect.DeepEqual(value, []interface{}{map[string]interface{}{"b":"c"}}) {
		t.Fatal("GetRefObjList valid object failure:", value, bare, err)
	}
	// Get valid list
	value, bare, err = GetRefObjList(obj, "baz")
	if err != nil || bare || !reflect.DeepEqual(value, []interface{}{"d", "e"}) {
		t.Fatal("GetRefObjList valid list failure:", value, bare, err)
	}
	// Get invalid
	value, bare, err = GetRefObjList(obj, "quux")
	if err != BadType || bare || value != nil {
		t.Fatal("GetRefObjList invalid failure:", value, bare, err)
	}
}

func TestGetObj(t *testing.T) {
	obj := map[string]interface{}{"foo":map[string]interface{}{"bar":23},
		"baz":"quux"}
	// Get valid
	value, err := GetObj(obj, "foo")
	if err != nil || reflect.DeepEqual(value, map[string]interface{}{"bar":23}) != true {
		t.Fatal("GetObj valid key error:", value, err)
	}
	// Get invalid type
	value, err = GetObj(obj, "baz")
	if err != BadType || value != nil {
		t.Fatal("GetObj invalid key error:", value, err)
	}
}
