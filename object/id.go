package object

import (
	"fmt"
	"strings"
	"strconv"
	"net/url"
)

func EnforceLocalIDSanity(localID LocalID) (saneLocalID LocalID) {
	// Force a single slash at the beginning.
	temp := strings.TrimLeft(string(localID), "/")
	temp = "/" + temp
	return LocalID(temp)
}

func GlobalIDFromLocalIDPorted(localID LocalID, hostname string, port uint16) (globalID string) {
	host := fmt.Sprintf("%s:%d", hostname, port)
	return GlobalIDFromLocalID(localID, host)
}

func GlobalIDFromLocalID(localID LocalID, hostname string) (globalID string) {
	localID = EnforceLocalIDSanity(localID)
	return fmt.Sprintf("https://%s%s", hostname, string(localID))
}

func LocalIDFromGlobalID(globalID string) (localID LocalID, server string, port uint16, err error) {
	parsed, err := url.Parse(globalID)
	if err != nil {
		return "", "", 0, err
	}
	server = parsed.Hostname()
	p := parsed.Port()
	if p == "" {
		port = 0
	} else {
		// convert port to number
		port64, err := strconv.ParseUint(p, 0, 16)
		if err != nil {
			port = 0
		} else {
			port = uint16(port64)
		}
	}
	localID = LocalID(parsed.EscapedPath())
	localID = EnforceLocalIDSanity(localID)
	return localID, server, port, nil
}

func IsIDGlobal(questionedID string) (global bool) {
	_, host, _, err := LocalIDFromGlobalID(questionedID)
	if host != "" && err == nil {
		return true
	}
	return false
}

type LocalID string

func IsLocal(candidate interface{}) (local bool) {
	switch candidate.(type) {
	case LocalID:
		return true
	default:
		return false
	}
}

func CompareIDGlobalLocal(global string, local LocalID) (equalPath bool, host string, port uint16) {
	gLoc, host, port, err := LocalIDFromGlobalID(global)
	if err != nil {
		return false, "", 0
	}
	if gLoc == local {
		return true, host, port
	}
	return false, host, port
}

func CompareIDs(a interface{}, b interface{}) (equalPath bool, host string, port uint16) {
	var aGlobal, bGlobal bool
	// We don't use IsLocal because we need to distinguish invalid types
	switch a.(type) {
	case string:
		aGlobal = true
	case LocalID:
		aGlobal = false
	default:
		return false, "", 0
	}
	switch b.(type) {
	case string:
		bGlobal = true
	case LocalID:
		bGlobal = false
	default:
		return false, "", 0
	}
	if aGlobal == bGlobal {
		// Both the same type; we can directly compare
		if aGlobal { // both strings
			return a.(string) == b.(string), "", 0
		} else { // both LocalIDs
			return a.(LocalID) == b.(LocalID), "", 0
		}
	} else {
		// Not the same type. Use the special comparator.
		// We return a host and port when the IDs are different types
		// so that the caller can check nativity.
		if aGlobal {
			return CompareIDGlobalLocal(a.(string), b.(LocalID))
		} else {
			return CompareIDGlobalLocal(b.(string), a.(LocalID))
		}
	}
}

func CompareIDsNative(a interface{}, b interface{}, host string, port uint16) (equal bool) {
	eq, h, p := CompareIDs(a, b)
	hostValid := h == "" || h == host
	portValid := p == 0 || p == port
	return eq && hostValid && portValid
}

func ExtractNativeFromGlobal(global string, host string, port uint16) (local LocalID) {
	local, urlHost, urlPort, err := LocalIDFromGlobalID(global)
	if err != nil { // Not a proper URL
		return LocalID("")
	}
	if urlHost != host || urlPort != port {
		return LocalID("")
	}
	return local
}
