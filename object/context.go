package object

// Checks that a value has a valid type for @context data
func TypecheckContext(context interface{}) (ok bool) {
	switch context.(type) {
	case string:
		return true
	case map[string]interface{}:
		return true
	case []interface{}:
		return true
	default:
		return false
	}
}

// Splits the @context field out of an object
func ExtractContext(object map[string]interface{}) (context interface{}, err error) {
	// check for context
	context, ok := object["@context"]
	if !ok {
		// no context, error
	}
	// check context type
	validCType := TypecheckContext(context)
	if !validCType {
		return nil, InvalidContext
	}
	// delete context from blob
	delete(object, "@context")
	// return
	return context, nil
}

// squish context into map
func InjectContext(object map[string]interface{}, context interface{}) (err error) {
	ok := TypecheckContext(context)
	if !ok {
		return InvalidContext
	}
	object["@context"] = context
	return nil
}
