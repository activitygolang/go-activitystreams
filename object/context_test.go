package object

import (
	"testing"
	"reflect"
)

func TestExtractContext(t *testing.T) {
	// Test valid string type
	obj := map[string]interface{}{"foo":"bar",
		"@context":"context-string"}
	context, err := ExtractContext(obj)
	if err != nil {
		t.Fatal("Valid string error:", err)
	}
	if context.(string) != "context-string" {
		t.Fatal("Incorrect context data:", context)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar"}) != true {
		t.Fatal("Mangled object:", obj)
	}
	// Test valid map type
	obj = map[string]interface{}{"foo":"bar",
		"@context":map[string]interface{}{"as":"context-string"}}
	context, err = ExtractContext(obj)
	if err != nil {
		t.Fatal("Valid string error:", err)
	}
	if reflect.DeepEqual(context, map[string]interface{}{"as":"context-string"}) != true {
		t.Fatal("Incorrect context data:", context)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar"}) != true {
		t.Fatal("Mangled object:", obj)
	}
	// Test valid list type
	obj = map[string]interface{}{"foo":"bar",
		"@context":[]interface{}{"context-string", "baz"}}
	context, err = ExtractContext(obj)
	if err != nil {
		t.Fatal("Valid string error:", err)
	}
	if reflect.DeepEqual(context, []interface{}{"context-string", "baz"}) != true {
		t.Fatal("Incorrect context data:", context)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar"}) != true {
		t.Fatal("Mangled object:", obj)
	}
	// Test invalid
	obj = map[string]interface{}{"foo":"bar",
		"@context":5}
	context, err = ExtractContext(obj)
	if err == nil || err != InvalidContext {
		t.Fatal("Bad error:", err)
	}
}

func TestInjectContext(t *testing.T) {
	// Test valid string
	obj := map[string]interface{}{"foo":"bar"}
	err := InjectContext(obj, "jabberjabberjabber")
	if err != nil {
		t.Fatal("String injection error:", err)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar", "@context":"jabberjabberjabber"}) != true {
		t.Fatal("String injection failure:", obj)
	}
	// Test valid map
	obj = map[string]interface{}{"foo":"bar"}
	injectedMap := map[string]interface{}{"baz":"quux"}
	err = InjectContext(obj, injectedMap)
	if err != nil {
		t.Fatal("Map injection error:", err)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar", "@context":injectedMap}) != true {
		t.Fatal("Map injection failure:", obj)
	}
	// Test valid list
	obj = map[string]interface{}{"foo":"bar"}
	injectedList := []interface{}{"baz", "quuz"}
	err = InjectContext(obj, injectedList)
	if err != nil {
		t.Fatal("List injection error:", err)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"foo":"bar", "@context":injectedList}) != true {
		t.Fatal("List injection failure:", obj)
	}
	// Test invalid
	obj = map[string]interface{}{"foo":"bar"}
	err = InjectContext(obj, 5)
	if err == nil || err != InvalidContext {
		t.Fatal("Bad error:", err)
	}
}
