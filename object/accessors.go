package object

func preliminaryGet(obj map[string]interface{}, fieldName string) (value interface{}, err error) {
	if obj == nil {
		return nil, NoObject
	}
	value, ok := obj[fieldName]
	if !ok {
		return nil, NoField
	}
	return value, nil
}

func GetStr(obj map[string]interface{}, fieldName string) (value string, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return "", err
	}
	// check type
	switch bare.(type) {
	case string:
		// cast and return
		return bare.(string), nil
	default:
		return "", BadType
	}
}

func GetLocalID(obj map[string]interface{}, fieldName string) (value LocalID, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return LocalID(""), err
	}
	// check type
	switch bare.(type) {
	case LocalID:
		// cast and return
		return bare.(LocalID), nil
	default:
		return LocalID(""), BadType
	}
}

func GetInt(obj map[string]interface{}, fieldName string) (value int64, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return 0, err
	}
	switch bare.(type) {
	case float64: // Most likely type to encounter, json.Unmarshal emits this.
		return int64(bare.(float64)), nil
	case int64:
		return bare.(int64), nil
	default:
		return 0, BadType
	}
}

func GetUInt(obj map[string]interface{}, fieldName string) (value uint64, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return 0, err
	}
	switch bare.(type) {
	case float64: // Most likely type to encounter, json.Unmarshal emits this.
		tmp := int64(bare.(float64))
		if tmp < 0 {
			return 0, BadSign
		} else {
			return uint64(tmp), nil
		}
	case int64:
		if bare.(int64) < 0 {
			return 0, BadSign
		} else {
			return uint64(bare.(int64)), nil
		}
	default:
		return 0, BadType
	}
}

func GetFloat(obj map[string]interface{}, fieldName string) (value float64, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return 0.0, err
	}
	switch bare.(type) {
	case float64:
		return bare.(float64), nil
	default:
		return 0.0, BadType
	}
}

// Unsure of proper return type
func GetDatetime(obj map[string]interface{}, fieldName string) (value string, err error) {
	_, err = preliminaryGet(obj, fieldName)
	if err != nil {
		return "", err
	}
	return "", nil
}

// Unsure of proper return type
func GetDuration(obj map[string]interface{}, fieldName string) (value string, err error) {
	_, err = preliminaryGet(obj, fieldName)
	if err != nil {
		return "", err
	}
	return "", nil
}

// Used for fields which can be any sort of ref
func GetRef(obj map[string]interface{}, fieldName string) (strVal string, objVal map[string]interface{}, lstVal []interface{}, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return "", nil, nil, err
	}
	// Check type
	switch bare.(type) {
	case string:
		return bare.(string), nil, nil, nil
	case map[string]interface{}:
		return "", bare.(map[string]interface{}), nil, nil
	case []interface{}:
		return "", nil, bare.([]interface{}), nil
	default:
		return "", nil, nil, BadType
	}
}

// Used for fields which can be an IRI or an embedded object
func GetRefObj(obj map[string]interface{}, fieldName string) (strVal string, objVal map[string]interface{}, err error) {
	strVal, objVal, lstVal, err := GetRef(obj, fieldName)
	if err != nil {
		return "", nil, err
	} else if lstVal != nil {
		return "", nil, BadType
	} else {
		return strVal, objVal, nil
	}
}

// Used for fields which can be lists of IRI strings.
// bareSingle indicates if the value was a single non-list IRI
func GetRefList(obj map[string]interface{}, fieldName string) (value []string, bareSingle bool, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return nil, false, err
	}
	switch bare.(type) {
	case string: // Scalar... return as list
		return []string{bare.(string)}, true, nil
	case []string:
		return bare.([]string), false, nil
	case []interface{}:
		value = make([]string, 0)
		for _, item := range bare.([]interface{}) {
			switch item.(type) {
			case string:
				value = append(value, item.(string))
			default:
				// FIXME: not certain this is the correct way
				return nil, false, BadType
			}
		}
		return value, false, nil
	default:
		return nil, false, BadType
	}
	return nil, false, nil
}

// Used for fields which can be lists of IRI strings or embedded objects.
// bareSingle indicates if the value was a single non-list IRI
func GetRefObjList(obj map[string]interface{}, fieldName string) (value []interface{}, bareSingle bool, err error) {
	bare, err := preliminaryGet(obj, fieldName)
	if err != nil {
		return nil, false, err
	}
	switch bare.(type) {
	case string:
		return []interface{}{bare.(string)}, true, nil
	case map[string]interface{}:
		return []interface{}{bare.(map[string]interface{})}, true, nil
	case []interface{}:
		return bare.([]interface{}), false, nil
	default:
		return nil, false, BadType
	}
	return nil, false, nil
}

// Used for fields which are expected to be an object
func GetObj(obj map[string]interface{}, fieldName string) (objVal map[string]interface{}, err error) {
	strVal, objVal, lstVal, err := GetRef(obj, fieldName)
	if err != nil {
		return
	} else if strVal != "" || lstVal != nil {
		return nil, BadType
	} else {
		return objVal, nil
	}
}
