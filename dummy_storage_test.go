package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	"testing"
)

func TestNewCloseDummyStorage(t *testing.T) {
	sto := NewDummyStorage()
	err := sto.Close()
	if err != nil {
		t.Fatal("DummyStorage Close error:", err)
	}
	tx, err := sto.StartTransaction()
	if err != ClosedStorage || tx != nil {
		t.Fatal("DummyStorage failed close check:", err)
	}
}

func TestDummyStorage_Transactions(t *testing.T) {
	sto := NewDummyStorage()
	// start
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// end
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}
	// end invalid
	err = tx.EndTransaction(nil)
	if err != InvalidTransaction {
		t.Fatal("EndTransaction, invalid error:", err)
	}
}

func TestDummyStorage_Store(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// store
	err = tx.StoreObject(map[string]interface{}{"@id": object.LocalID("foo"),
		"bar": 1})
	if err != nil {
		t.Fatal("StoreObject error:", err)
	}
	// no id
	err = tx.StoreObject(map[string]interface{}{"bar": 1})
	if err != object.NoObjID {
		t.Fatal("StoreObject error:", err)
	}
	// present
	err = tx.StoreObject(map[string]interface{}{"@id": object.LocalID("foo"),
		"bar": 1})
	if err != object.ObjectPresent {
		t.Fatal("StoreObject error:", err)
	}
}

func TestDummyStorage_Load(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreObject(map[string]interface{}{"@id": object.LocalID("foo"),
		"bar": 1})
	if err != nil {
		t.Fatal("LoadObject store error:", err)
	}
	// load
	obj, err := tx.LoadObject("foo")
	if err != nil {
		t.Fatal("LoadObject error:", err)
	}
	if reflect.DeepEqual(obj, map[string]interface{}{"@id": object.LocalID("foo"), "bar": 1}) != true {
		t.Fatal("LoadObject load fail:", obj)
	}
	// no obj
	obj, err = tx.LoadObject(object.LocalID("bar"))
	if err != object.NoObject {
		t.Fatal("LoadObject error:", err)
	}
	if obj != nil {
		t.Fatal("LoadObject load fail:", obj)
	}
}

func TestRemoveObject(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	// Remove invalid
	err = tx.RemoveObject(object.LocalID("/bar"))
	if err != object.NoObject {
		t.Fatal("RemoveObject invalid failure:", err)
	}
	// Remove valid
	err = tx.RemoveObject(object.LocalID("/foo"))
	if err != nil {
		t.Fatal("RemoveObject valid failure:", err)
	}
	if _, ok := sto.objects[object.LocalID("/foo")]; ok {
		t.Fatal("RemoveObject failed to delete")
	}
}

func TestSetProtected(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	// Protect invalid
	err = tx.SetProtected(object.LocalID("/bar"), true)
	if err != object.NoObject {
		t.Fatal("SetProtected invalid failure:", err)
	}
	// Protect valid
	err = tx.SetProtected(object.LocalID("/foo"), true)
	if err != nil {
		t.Fatal("SetProtected valid failure:", err)
	}
	if prot, ok := sto.protected[object.LocalID("/foo")]; !ok || !prot {
		t.Fatal("SetProtected failed to protect:", prot, ok)
	}
}

func TestIsProtected(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.protected["/foo"] = true
	// Check invalid
	prot, err := tx.IsProtected(object.LocalID("/bar"))
	if err != object.NoObject || prot {
		t.Fatal("IsProtected invalid failure:", err, prot)
	}
	// Check valid
	prot, err = tx.IsProtected(object.LocalID("/foo"))
	if err != nil || !prot {
		t.Fatal("IsProtected valid failure:", err, prot)
	}
}

func TestBasicObjectData(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo/bar"] = map[string]interface{}{
		"@id": object.LocalID("/foo/bar"), "@type": "Note",
		"attributedTo": object.LocalID("/foo")}
	// Test no object
	objType, owner, err := tx.BasicObjectData(object.LocalID("/foo/quux"))
	if objType != "" || owner != object.LocalID("") || err != object.NoObject {
		t.Fatal("BasicObjectData no object failure:", objType, owner, err)
	}
	// Test working
	objType, owner, err = tx.BasicObjectData(object.LocalID("/foo/bar"))
	if objType != "Note" || owner != object.LocalID("/foo") || err != nil {
		t.Fatal("BasicObjectData failure:", objType, owner, err)
	}
}

func TestExists(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo/bar"] = map[string]interface{}{
		"@id": object.LocalID("/foo/bar"), "@type": "Note",
		"attributedTo": object.LocalID("/foo")}
	// Test no object
	exists, err := tx.Exists(object.LocalID("/foo/quux"))
	if exists || err != nil {
		t.Fatal("Exists no object failure:", exists, err)
	}
	// Test object present
	exists, err = tx.Exists(object.LocalID("/foo/bar"))
	if !exists || err != nil {
		t.Fatal("Exists no object failure:", exists, err)
	}
}

func TestLoadObjectField(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	// Test no object
	data, err := tx.LoadObjectField(object.LocalID("/bar"), "@type")
	if data != nil || err != object.NoObject {
		t.Fatal("LoadObjectField no object failure:", err, data)
	}
	// Test no field
	data, err = tx.LoadObjectField(object.LocalID("/foo"), "blah")
	if data != nil || err != object.NoField {
		t.Fatal("LoadObjectField no field failure:", err, data)
	}
	// Test working
	data, err = tx.LoadObjectField(object.LocalID("/foo"), "@type")
	if err != nil || !reflect.DeepEqual(data, "Note") {
		t.Fatal("LoadObjectField working failure:", err, data)
	}
}

func TestDummyStorage_Append(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreObject(map[string]interface{}{"@id": object.LocalID("foo"),
		"bar": 1, "quux": []interface{}{1}})
	// append
	err = tx.ListAppend(object.LocalID("foo"), "quux", 2)
	if err != nil {
		t.Fatal("ListAppend error:", err)
	}
	// no obj
	err = tx.ListAppend(object.LocalID("qwerty"), "quux", 2)
	if err != object.NoObject {
		t.Fatal("ListAppend error:", err)
	}
	// no field
	err = tx.ListAppend(object.LocalID("foo"), "asdf", 2)
	if err != nil {
		t.Fatal("ListAppend error:", err)
	}
	_, ok := sto.objects[object.LocalID("foo")]["asdf"]
	if !ok {
		t.Fatal("ListAppend new field failure:", sto.objects["foo"])
	}
	// scalar
	err = tx.ListAppend(object.LocalID("foo"), "bar", 2)
	if err != nil {
		t.Fatal("ListAppend error:", err)
	}
	f := sto.objects[object.LocalID("foo")]["bar"]
	if reflect.DeepEqual(f, []interface{}{1, 2}) != true {
		t.Fatal("ListAppend scalar failure:", f)
	}
}

func TestAddField(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note", "quux": "existance"}
	// Test no object
	err = tx.AddField(object.LocalID("/bar"), "bah", "a")
	if err != object.NoObject {
		t.Fatal("AddField no object failure:", err)
	}
	// Test field already exists
	err = tx.AddField(object.LocalID("/foo"), "quux", "a")
	if err != DuplicateItem {
		t.Fatal("AddField existing field failure:", err)
	}
	// Test working
	err = tx.AddField(object.LocalID("/foo"), "baz", "a")
	if err != nil {
		t.Fatal("AddField working error:", err)
	}
	data, ok := sto.objects[object.LocalID("/foo")]["baz"]
	if !ok || !reflect.DeepEqual(data, "a") {
		t.Fatal("AddField working failure:", data, ok)
	}
}

func TestUpdateField(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note", "quux": "existance"}
	// Test no object
	err = tx.UpdateField(object.LocalID("/bar"), "bah", "a")
	if err != object.NoObject {
		t.Fatal("UpdateField no object failure:", err)
	}
	// Test no field
	err = tx.UpdateField(object.LocalID("/foo"), "baz", "a")
	if err != object.NoField {
		t.Fatal("UpdateField no field failure:", err)
	}
	// Test working
	err = tx.UpdateField(object.LocalID("/foo"), "quux", "a")
	if err != nil {
		t.Fatal("UpdateField no field error:", err)
	}
	data, ok := sto.objects[object.LocalID("/foo")]["quux"]
	if !ok || !reflect.DeepEqual(data, "a") {
		t.Fatal("UpdateField working failure:", data, ok)
	}
}

func TestRemoveField(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note", "quux": "existance"}
	// Test no object
	err = tx.RemoveField(object.LocalID("/bar"), "bah")
	if err != object.NoObject {
		t.Fatal("RemoveField no object failure:", err)
	}
	// Test no field
	err = tx.RemoveField(object.LocalID("/foo"), "bah")
	if err != object.NoField {
		t.Fatal("RemoveField no object failure:", err)
	}
	// Test working
	err = tx.RemoveField(object.LocalID("/foo"), "quux")
	if err != nil {
		t.Fatal("RemoveField no object failure:", err)
	}
	if _, ok := sto.objects[object.LocalID("/foo")]["quux"]; ok {
		t.Fatal("RemoveField failed to delete")
	}
}

func TestObjcolAdd(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	// Test no object
	err = tx.ObjcolAdd(object.LocalID("/bar"), "stuff", "widget")
	if err != object.NoObject {
		t.Fatal("ObjcolAdd failure:", err)
	}
	// Test working
	err = tx.ObjcolAdd(object.LocalID("/foo"), "stuff", "widget")
	if err != nil {
		t.Fatal("ObjcolAdd err:", err)
	}
	col, ok := sto.collections[object.LocalID("/foo")]
	expected := map[string][]interface{}{"stuff": []interface{}{"widget"}}
	if !ok || !reflect.DeepEqual(col, expected) {
		t.Fatal("ObjcolAdd failure:", ok, col)
	}
}

func TestObjcolRM(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.collections[object.LocalID("/foo")] = map[string][]interface{}{
		"stuff": []interface{}{"a", "widget", "x", "widget", "z", "q"}}
	// Test no object
	err = tx.ObjcolRM(object.LocalID("/bar"), "stuff", "widget")
	if err != object.NoObject {
		t.Fatal("ObjcolRM failure:", err)
	}
	// Test no field
	err = tx.ObjcolRM(object.LocalID("/foo"), "quux", "widget")
	if err != object.NoField {
		t.Fatal("ObjcolRM failure:", err)
	}
	// Test working
	err = tx.ObjcolRM(object.LocalID("/foo"), "stuff", "widget")
	if err != nil {
		t.Fatal("ObjcolRM error:", err)
	}
	col, ok := sto.collections[object.LocalID("/foo")]
	expected := map[string][]interface{}{
		"stuff": []interface{}{"a", "x", "z", "q"}}
	if !ok || !reflect.DeepEqual(col, expected) {
		t.Fatal("ObjcolRM failure:", ok, col)
	}
}

func TestObjcolGetAllField(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.collections[object.LocalID("/foo")] = map[string][]interface{}{
		"stuff": []interface{}{"a", "widget", "x", "widget", "z", "q"}}
	// Test no object
	_, err = tx.ObjcolGetAllField(object.LocalID("/bar"), "stuff")
	if err != object.NoObject {
		t.Fatal("ObjcolGetAllField failure:", err)
	}
	// Test no field
	_, err = tx.ObjcolGetAllField(object.LocalID("/foo"), "quux")
	if err != object.NoField {
		t.Fatal("ObjcolGetAllField failure:", err)
	}
	// Test working
	data, err := tx.ObjcolGetAllField(object.LocalID("/foo"), "stuff")
	if err != nil {
		t.Fatal("ObjcolGetAllField error:", err)
	}
	expected := []interface{}{"a", "widget", "x", "widget", "z", "q"}
	if !reflect.DeepEqual(data, expected) {
		t.Fatal("ObjcolGetAllfield failure:", data)
	}
}

func TestObjcolGetFields(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.collections[object.LocalID("/foo")] = map[string][]interface{}{
		"stuff": []interface{}{"a"}, "other": []interface{}{"b"},
	}
	// Test no object
	_, err = tx.ObjcolGetFields(object.LocalID("/bar"))
	if err != object.NoObject {
		t.Fatal("ObjcolGetFields failure:", err)
	}
	// Test working
	fields, err := tx.ObjcolGetFields(object.LocalID("/foo"))
	if err != nil {
		t.Fatal("ObjcolGetFields error:", err)
	}
	exp1 := []string{"stuff", "other"}
	exp2 := []string{"other", "stuff"}
	if !(reflect.DeepEqual(fields, exp1) || reflect.DeepEqual(fields, exp2)) {
		t.Fatal("ObjcolGetFields failure:", fields)
	}
}

func TestObjcolRMAll(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.collections[object.LocalID("/foo")] = map[string][]interface{}{
		"stuff": []interface{}{"a"}, "other": []interface{}{"b"},
	}
	// Test no object
	err = tx.ObjcolRMAll(object.LocalID("/bar"))
	if err != object.NoObject {
		t.Fatal("ObjcolRMAll failure:", err)
	}
	// Test working
	err = tx.ObjcolRMAll("/foo")
	if err != nil {
		t.Fatal("ObjcolRMAll error:", err)
	}
	if len(sto.collections["/foo"]) != 0 {
		t.Fatal("ObjcolRMAll failure:", sto.collections["/foo"])
	}
}

func TestObjcolGetAll(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	sto.collections[object.LocalID("/foo")] = map[string][]interface{}{
		"stuff": []interface{}{"a"}, "other": []interface{}{"b"},
	}
	// Test no object
	_, err = tx.ObjcolGetAll(object.LocalID("/bar"))
	if err != object.NoObject {
		t.Fatal("ObjcolGetAll failure:", err)
	}
	// Test working
	data, err := tx.ObjcolGetAll(object.LocalID("/foo"))
	if err != nil {
		t.Fatal("ObjcolGetAll error:", err)
	}
	expected := map[string][]interface{}{
		"stuff": []interface{}{"a"}, "other": []interface{}{"b"},
	}
	if !reflect.DeepEqual(data, expected) {
		t.Fatal("ObjcolGetAll failure:", data)
	}
}

func TestObjcolSet(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.objects["/foo"] = map[string]interface{}{"@id": object.LocalID("/foo"),
		"@type": "Note"}
	// Test no object
	err = tx.ObjcolSet(object.LocalID("/bar"),
		map[string][]interface{}{
			"stuff": []interface{}{"a"}, "other": []interface{}{"b"}})
	if err != object.NoObject {
		t.Fatal("ObjcolSet failure:", err)
	}
	// Test working
	err = tx.ObjcolSet(object.LocalID("/foo"),
		map[string][]interface{}{
			"stuff": []interface{}{"a"}, "other": []interface{}{"b"}})
	if err != nil {
		t.Fatal("ObjcolSet err:", err)
	}
	expected := map[string][]interface{}{
		"stuff": []interface{}{"a"}, "other": []interface{}{"b"},
	}
	if !reflect.DeepEqual(sto.collections["/foo"], expected) {
		t.Fatal("ObjcolSet failure:", sto.collections["/foo"])
	}
}

func TestStoreCache(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreCache(map[string]interface{}{"@id": "foo", "bar": 1})
	if err != nil {
		t.Fatal("StoreCache error:", err)
	}
	expected := map[string]interface{}{"@id": "foo", "bar": 1}
	if !reflect.DeepEqual(sto.cache["foo"], expected) {
		t.Fatal("StoreCache failure:", sto.cache)
	}
}

func TestLoadCache(t *testing.T) {
	sto := NewDummyStorage()
	sto.cache["foo"] = map[string]interface{}{"@id": "foo", "bar": 1}
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.LoadCache("foo")
	if err != nil {
		t.Fatal("LoadCache error:", err)
	}
	if !reflect.DeepEqual(obj, map[string]interface{}{"@id": "foo", "bar": 1}) {
		t.Fatal("LoadCache failure:", obj)
	}
}

func TestStoreProp(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreProp(map[string]interface{}{
		"@id": object.LocalID("foo"),
		"bar": 1},
		[]PropRetryTarget{
			NewPropRetryGlobal("foo.test"),
			NewPropRetryGlobal("bar.test")})
	if err != nil {
		t.Fatal("StoreProp error:", err)
	}
	expected := map[string]interface{}{"@id": object.LocalID("foo"), "bar": 1}
	if !reflect.DeepEqual(sto.propObj["foo"], expected) {
		t.Fatal("StoreProp object not stored:", sto.propObj)
	}
	if !reflect.DeepEqual(sto.propTgt["foo"]["foo.test"], NewPropRetryGlobal("foo.test")) || !reflect.DeepEqual(sto.propTgt["foo"]["bar.test"], NewPropRetryGlobal("bar.test")) {
		t.Fatal("StoreProp targets not stored:", sto.propTgt)
	}
}

func TestGetPropIDs(t *testing.T) {
	sto := NewDummyStorage()
	sto.propObj["foo"] = map[string]interface{}{"@id": "foo"}
	sto.propObj["bar"] = map[string]interface{}{"@id": "bar"}
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	ids, err := tx.GetPropIDs()
	if err != nil {
		t.Fatal("GetPropIDs error:", err)
	}
	// Random ordering because of maps
	if !reflect.DeepEqual(ids, []object.LocalID{"foo", "bar"}) && !reflect.DeepEqual(ids, []object.LocalID{"bar", "foo"}) {
		t.Fatal("GetPropIDs failure:", ids)
	}
}

func TestGetPropObject(t *testing.T) {
	sto := NewDummyStorage()
	sto.propObj["foo"] = map[string]interface{}{"@id": "foo"}
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.GetPropObject("foo")
	if err != nil {
		t.Fatal("GetPropObject error:", err)
	}
	if !reflect.DeepEqual(obj, map[string]interface{}{"@id": "foo"}) {
		t.Fatal("GetPropObject failure:", obj)
	}
}

func TestGetPropTargets(t *testing.T) {
	sto := NewDummyStorage()
	sto.propTgt["foo"] = make(map[string]PropRetryTarget, 0)
	sto.propTgt["foo"]["asdf.test"] = NewPropRetryGlobal("asdf.test")
	sto.propTgt["foo"]["zxcv.test"] = NewPropRetryGlobal("zxcv.test")
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	targets, err := tx.GetPropTargets("foo")
	if err != nil {
		t.Fatal("GetPropTarget error:", err)
	}
	expectedA := PropRetryTarget{"asdf.test", false, 0, ""}
	expectedB := PropRetryTarget{"zxcv.test", false, 0, ""}
	if !reflect.DeepEqual(targets, []PropRetryTarget{expectedA, expectedB}) && !reflect.DeepEqual(targets, []PropRetryTarget{expectedB, expectedA}) {
		t.Fatal("GetPropTarget failure:", targets)
	}
}

func TestUpdatePropTarget(t *testing.T) {
	sto := NewDummyStorage()
	sto.propTgt["foo"] = make(map[string]PropRetryTarget, 0)
	sto.propTgt["foo"]["asdf.test"] = NewPropRetryGlobal("asdf.test")
	newTarget := PropRetryTarget{"asdf.test", false, 5, "time"}
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.UpdatePropTarget("foo", newTarget)
	if err != nil {
		t.Fatal("UpdatePropTarget error:", err)
	}
	target := sto.propTgt["foo"]["asdf.test"]
	if !reflect.DeepEqual(target, PropRetryTarget{"asdf.test", false, 5, "time"}) {
		t.Fatal("UpdatePropTarget failure:", target)
	}
}

func TestRemovePropTarget(t *testing.T) {
	sto := NewDummyStorage()
	sto.propTgt["foo"] = make(map[string]PropRetryTarget, 0)
	sto.propObj["foo"] = map[string]interface{}{"@id": "foo"}
	sto.propTgt["foo"]["asdf.test"] = NewPropRetryGlobal("asdf.test")
	sto.propTgt["foo"]["zxcv.test"] = NewPropRetryGlobal("zxcv.test")
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test remove
	err = tx.RemovePropTarget("foo", "asdf.test")
	if err != nil {
		t.Fatal("RemovePropTarget error:", err)
	}
	if v, ok := sto.propTgt["foo"]; !ok || len(v) != 1 {
		t.Fatal("RemovePropTarget failure:", v)
	}
	if _, ok := sto.propObj["foo"]; !ok {
		t.Fatal("RemovePropTarget missing object:", sto.propObj)
	}
	// Test final remove
	err = tx.RemovePropTarget("foo", "zxcv.test")
	if err != nil {
		t.Fatal("RemovePropTarget error:", err)
	}
	if v, ok := sto.propTgt["foo"]; ok {
		t.Fatal("RemovePropTarget failure:", v)
	}
	if _, ok := sto.propObj["foo"]; ok {
		t.Fatal("RemovePropTarget object not deleted:", sto.propObj)
	}
}

func TestListRemove(t *testing.T) {
	sto := NewDummyStorage()
	target := map[string]interface{}{"@id": "foo",
		"items": []interface{}{"a", "b", 3}}
	sto.objects["foo"] = target
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test
	err = tx.ListRemove("foo", "items", "b")
	if err != nil {
		t.Fatal("ListRemove error:", err)
	}
	list := sto.objects["foo"]["items"]
	if !reflect.DeepEqual(list, []interface{}{"a", 3}) {
		t.Fatal("ListRemove failure:", list)
	}
}

func TestStoreChangeset(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test
	err = tx.StoreChangeset("foo",
		[]Change{Change{CSListAppend, "foo", "bar", 1}})
	if err != nil {
		t.Fatal("StoreChangeset error:", err)
	}
	cset, ok := sto.changesets["foo"]
	if !ok {
		t.Fatal("StoreChangeset no changeset")
	}
	expected := []Change{Change{CSListAppend, "foo", "bar", 1}}
	if !reflect.DeepEqual(cset, expected) {
		t.Fatal("StoreChangeset failure:", cset)
	}
}

func TestLoadChangeset(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	sto.changesets["/foo"] = []Change{
		Change{CSListAppend, "foo", "a", 1},
		Change{CSRemoveObject, "bar", "a", 1}}
	// Test no changeset
	_, err = tx.LoadChangeset("/bar")
	if err != NoChangeset {
		t.Fatal("LoadChangeset failure:", err)
	}
	// Test working
	cset, err := tx.LoadChangeset("/foo")
	if err != nil {
		t.Fatal("LoadChangeset failure:", err)
	}
	expected := []Change{Change{CSListAppend, "foo", "a", 1},
		Change{CSRemoveObject, "bar", "a", 1}}
	if !reflect.DeepEqual(cset, expected) {
		t.Fatal("LoadChangeset failure:", cset)
	}
}

func TestStoreAccount(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	acct := Account{
		"foo",
		[16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		1, 2, 3,
		[]byte{1, 2, 3, 4},
		"/actorman",
		"",
		"",
	}
	tx.StoreAccount(acct)
	if !reflect.DeepEqual(acct, sto.accounts["foo"]) {
		t.Fatal("StoreAccount failure:", sto.accounts)
	}
}

func TestLoadAccount(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	acct := Account{
		"foo",
		[16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		1, 2, 3,
		[]byte{1, 2, 3, 4},
		"/actorman",
		"",
		"",
	}
	sto.accounts["foo"] = acct
	// Test load account
	loaded, err := tx.LoadAccount("foo")
	if err != nil {
		t.Fatal("LoadAccount error:", err, loaded)
	}
	if !reflect.DeepEqual(loaded, acct) {
		t.Fatal("LoadAccount failure:", loaded)
	}
	// Test load non-existing account
	loaded, err = tx.LoadAccount("bar")
	if err != object.NoObject {
		t.Fatal("LoadAccount error:", err, loaded)
	}
	if !reflect.DeepEqual(loaded, Account{}) {
		t.Fatal("LoadAccount failure:", loaded)
	}
}

func TestUpdateAccount(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	acct := Account{
		"foo",
		[16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		1, 2, 3,
		[]byte{1, 2, 3, 4},
		"/actorman",
		"",
		"",
	}
	sto.accounts["foo"] = acct
	newAcct := Account{
		"bar",
		[16]byte{15, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0},
		1, 2, 3,
		[]byte{1, 2, 3, 4},
		"/actorman",
		"",
		"",
	}
	err = tx.UpdateAccount("foo", newAcct)
	if err != nil {
		t.Fatal("UpdateAccount error:", err)
	}
	if _, ok := sto.accounts["foo"]; ok {
		t.Fatal("UpdateAccount failure: foo still present")
	}
	if !reflect.DeepEqual(sto.accounts["bar"], newAcct) {
		t.Fatal("UpdateAccount failure: bar not present")
	}
}

func TestDeleteAccount(t *testing.T) {
	sto := NewDummyStorage()
	tx, err := sto.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	acct := Account{
		"foo",
		[16]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		1, 2, 3,
		[]byte{1, 2, 3, 4},
		"/actorman",
		"",
		"",
	}
	sto.accounts["foo"] = acct
	tx.DeleteAccount("foo")
	if !reflect.DeepEqual(sto.accounts, map[string]Account{}) {
		t.Fatal("DeleteAccount failure:", sto.accounts)
	}
}
