
#
# Makefile for go-activitystreams
#

build:
	go build ./...
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...
#golint:
