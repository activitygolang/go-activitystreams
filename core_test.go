package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	"testing"
	//"fmt"
)

func TestGet_normal(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// inject test object
	objID := object.LocalID("/jrandom/testobj")
	obj := map[string]interface{}{"@id": objID, "@type": "Note",
		"attributedTo": object.LocalID("/jrandom"),
		"subject": "Test of obj-cols"}
	storage.objects[objID] = obj
	storage.collections[objID] = map[string][]interface{}{
		"plow": []interface{}{"baz"},
	}
	// Test
	gotten, err := core.Get(objID, "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{
		"@id": "https://foo.test/jrandom/testobj", "@type": "Note",
		"attributedTo": "https://foo.test/jrandom",
		"subject": "Test of obj-cols",
		"plow": "https://foo.test/jrandom/testobj/plow",
	}
	if !reflect.DeepEqual(gotten, expected) {
		t.Fatal("Get failure:", gotten)
	}
}

func TestGet_objcol(t *testing.T) {
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// inject test object
	objID := object.LocalID("/jrandom/testobj")
	obj := map[string]interface{}{"@id": objID, "@type": "Note",
		"attributedTo": object.LocalID("/jrandom"),
		"subject": "Test of obj-cols"}
	storage.objects[objID] = obj
	storage.collections[objID] = map[string][]interface{}{
		"plow": []interface{}{"baz"},
	}
	objcol, err := core.Get("/jrandom/testobj/plow", "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{
		"@id": "https://foo.test/jrandom/testobj/plow", "@type": "Collection",
		"attributedTo": "https://foo.test/jrandom",
		"items": []interface{}{"baz"},
	}
	if !reflect.DeepEqual(objcol, expected) {
		t.Fatal("Get objcol failure:", objcol)
	}
}
func TestGetLocal_normal(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// inject test object
	objID := object.LocalID("/jrandom/testobj")
	obj := map[string]interface{}{"@id": objID, "@type": "Note",
		"attributedTo": object.LocalID("/jrandom"),
		"subject": "Test of obj-cols"}
	storage.objects[objID] = obj
	storage.collections[objID] = map[string][]interface{}{
		"plow": []interface{}{"baz"},
	}
	// Test
	gotten, err := core.GetLocal(objID, "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{
		"@id": object.LocalID("/jrandom/testobj"), "@type": "Note",
		"attributedTo": object.LocalID("/jrandom"),
		"subject": "Test of obj-cols",
		"plow": object.LocalID("/jrandom/testobj/plow"),
	}
	if !reflect.DeepEqual(gotten, expected) {
		t.Fatal("Get failure:", gotten)
	}
}

func TestGetLocal_objcol(t *testing.T) {
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// inject test object
	objID := object.LocalID("/jrandom/testobj")
	obj := map[string]interface{}{"@id": objID, "@type": "Note",
		"attributedTo": object.LocalID("/jrandom"),
		"subject": "Test of obj-cols"}
	storage.objects[objID] = obj
	storage.collections[objID] = map[string][]interface{}{
		"plow": []interface{}{"baz"},
	}
	objcol, err := core.GetLocal("/jrandom/testobj/plow", "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{
		"@id": object.LocalID("/jrandom/testobj/plow"), "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items": []interface{}{"baz"},
	}
	if !reflect.DeepEqual(objcol, expected) {
		t.Fatal("Get objcol failure:", objcol)
	}
}

func TestPostOutbox(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// splice in test handler
	spliceResults := map[string]interface{}{
		"activityID": object.LocalID(""), "prop": nil, "err": nil}
	spliceArguments := map[string]interface{}{}
	core.outboxHandlers["Spliced"] = func(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
		core.preliminaryOutboxTweaks(actorID, activity)
		spliceArguments["actorID"] = actorID
		spliceArguments["outboxID"] = outboxID
		spliceArguments["activity"] = activity
		activityID = spliceResults["activityID"].(object.LocalID)
		temp := spliceResults["prop"]
		if temp != nil {
			prop = temp.([]interface{})
		} else {
			prop = nil
		}
		temp = spliceResults["err"]
		if temp != nil {
			err = temp.(error)
		} else {
			err = nil
		}
		return activityID, prop, nil, err
	}
	activity := map[string]interface{}{"@type": "Spliced", "blah": "foo"}
	// Test no actor for outbox
	activityID, err := core.PostOutbox("/foo/outbox", activity)
	if activityID != "" || err != InvalidOutbox {
		t.Fatal("PostOutbox no actor failure:", activityID, err)
	}
	// Test bad outbox
	activityID, err = core.PostOutbox("/jrandom/outbox5", activity)
	if activityID != "" || err != InvalidOutbox {
		t.Fatal("PostOutbox bad outbox failure:", activityID, err)
	}
	// Test failing handler
	spliceResults["err"] = UnownedTarget
	activityID, err = core.PostOutbox("/jrandom/outbox", activity)
	if activityID != "" || err != UnownedTarget {
		t.Fatal("PostOutbox handler failure:", activityID, err)
	}
	// Test working
	spliceResults["err"] = nil
	spliceResults["activityID"] = object.LocalID("asdf")
	activityID, err = core.PostOutbox("/jrandom/outbox", activity)
	if activityID != "https://foo.test/asdf" || err != nil {
		t.Fatal("PostOutbox working failure:", activityID, err)
	}
}

func Test_postInboxCore(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.AddActor("jrandom", "Person")
	core.AddActor("blocker", "Person")
	core.AddActor("blocked", "Person")
	// splice in test handler
	var spliceResult error
	spliceArguments := map[string]interface{}{}
	core.inboxHandlers["Splice"] = func(tx Transaction, actorID object.LocalID, activity map[string]interface{}) (err error) {
		spliceArguments["actorID"] = actorID
		spliceArguments["activity"] = activity
		return spliceResult
	}
	// setup environment
	nonActor := map[string]interface{}{"@type": "Quux"}
	storage.objects["/baz/foo"] = nonActor
	tx, _ := storage.StartTransaction()
	// Test no target
	activity := map[string]interface{}{}
	err := core.postInboxCore(tx, "foo/inbox", activity)
	if err != object.NoObject {
		t.Fatal("postInboxCore no target failure:", err)
	}
	// Test target not actor
	activity = map[string]interface{}{"@type": "Splice"}
	err = core.postInboxCore(tx, "/baz/foo/inbox", activity)
	if err != object.NoObject {
		t.Fatal("postInboxCore target not actor failure:", err)
	}
	// Test blocked
	activity = map[string]interface{}{"@type": "Splice",
		"attributedTo": "/jrandom"}
	storage.objects["/blocker/blocked"]["items"] = []interface{}{"/jrandom"}
	err = core.postInboxCore(tx, "/blocker/inbox", activity)
	if err != BlockedActor {
		t.Fatal("postInboxCore block failure:", err)
	}
	// Test failing handler
	activity = map[string]interface{}{"@type": "Splice", "@id": "/quux",
		"attributedTo": "/jrandom"}
	spliceResult = UnownedTarget
	err = core.postInboxCore(tx, "/jrandom/inbox", activity)
	if err != UnownedTarget {
		t.Fatal("postInboxCore handler failure:", err)
	}
	// Test working
	delete(storage.objects["/jrandom/inbox"], "items")
	spliceResult = nil
	err = core.postInboxCore(tx, "/jrandom/inbox", activity)
	if err != nil {
		t.Fatal("postInboxCore working failure:", err)
	}
	expectedInbox := []interface{}{"/quux"}
	items := storage.objects["/jrandom/inbox"]["items"]
	if !reflect.DeepEqual(items, expectedInbox) {
		t.Fatal("postInboxCore inbox failure:", items)
	}
}

func Test_splitPropagation(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.SubserverName = "alpha"
	core.AddActor("jrandom", "Person")
	tx, _ := storage.StartTransaction()
	// Run test
	local, remote := core.splitPropagation(tx,
		[]interface{}{"https://baz.test/1", "https://quux.test/2",
			"https://foo.test/jrandom", "https://foo.test/distant"})
	if !reflect.DeepEqual(local, []object.LocalID{object.LocalID("/jrandom")}) {
		t.Fatal("splitPropagation local failure:", local)
	}
	expectedRemote := []PropRetryTarget{
		NewPropRetryGlobal("https://baz.test/1"),
		NewPropRetryGlobal("https://quux.test/2"),
		NewPropRetryCluster("/distant")}
	if !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("splitPropagation remote failure:", remote)
	}
}

func Test_sortPropagationTarget(t *testing.T) {
	// setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	tx, _ := storage.StartTransaction()
	// setup environment
	noTypeID := object.LocalID("/badtypetarget")
	noType := map[string]interface{}{"@id": noTypeID}
	storage.objects[noTypeID] = noType
	badTypeID := object.LocalID("/badtypetarget")
	badType := map[string]interface{}{"@type": "Note", "@id": badTypeID}
	storage.objects[badTypeID] = badType
	recurseID := object.LocalID("/recurse")
	recurse := map[string]interface{}{"@type": "Collection", "@id": recurseID,
		"items": []interface{}{"one"}}
	storage.objects[recurseID] = recurse
	topCollectionID := object.LocalID("/topCollection")
	topCollection := map[string]interface{}{"@type": "Collection",
		"@id":   topCollectionID,
		"items": []interface{}{recurseID, "https://foo.test/jrandom"}}
	storage.objects[topCollectionID] = topCollection
	var expectedLocal []object.LocalID
	var expectedRemote []PropRetryTarget
	// Test native, no object
	local, remote := core.sortPropagationTarget(tx, "https://foo.test/xyzzy",
		RECURSE_MAX)
	expectedLocal = nil
	expectedRemote = nil
	if local != nil || remote != nil {
		t.Fatal("sortPropagationTarget native no object failure:", local,
			remote)
	}
	// Test native, no type
	local, remote = core.sortPropagationTarget(tx,
		"https://foo.test"+noTypeID, RECURSE_MAX)
	if local != nil || remote != nil {
		t.Fatal("sortPropagationTarget native no type failure:", local, remote)
	}
	// This is set here so that the previous tests would work
	// Cluster handling changes how missing targets are handled
	core.SubserverName = "alpha"
	// Test native, non-usable type
	local, remote = core.sortPropagationTarget(tx,
		string("https://foo.test"+badTypeID), RECURSE_MAX)
	expectedLocal = []object.LocalID{}
	expectedRemote = []PropRetryTarget{}
	if !reflect.DeepEqual(local, expectedLocal) || !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("sortPropagationTarget native bad type failure:", local,
			remote)
	}
	// Test native, is actor
	local, remote = core.sortPropagationTarget(tx,
		"https://foo.test/jrandom", RECURSE_MAX)
	expectedLocal = []object.LocalID{"/jrandom"}
	expectedRemote = []PropRetryTarget{}
	if !reflect.DeepEqual(local, expectedLocal) || !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("sortPropagationTarget native actor failure:", local, remote)
	}
	// Test native collection
	local, remote = core.sortPropagationTarget(tx,
		string("https://foo.test"+topCollectionID), RECURSE_MAX)
	if !reflect.DeepEqual(local, expectedLocal) || !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("sortPropagationTarget collection failure:", local, remote)
	}
	// Test cluster-local
	local, remote = core.sortPropagationTarget(tx,
		"https://foo.test/distant", RECURSE_MAX)
	expectedLocal = []object.LocalID{}
	expectedRemote = []PropRetryTarget{NewPropRetryCluster("/distant")}
	if !reflect.DeepEqual(local, expectedLocal) || !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("sortPropagationTarget cluster-local failure:", local, remote)
	}
	// Test non-native
	local, remote = core.sortPropagationTarget(tx,
		"https://baz.test/luser", RECURSE_MAX)
	expectedLocal = []object.LocalID{}
	expectedRemote = []PropRetryTarget{
		NewPropRetryGlobal("https://baz.test/luser")}
	if !reflect.DeepEqual(local, expectedLocal) || !reflect.DeepEqual(remote, expectedRemote) {
		t.Fatal("sortPropagationTarget non-native failure:", local, remote)
	}
}

func Test_distrbuteReplies(t *testing.T) {
	// Setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	tx, _ := storage.StartTransaction()
	// Setup environment
	fooID := object.LocalID("/jrandom/foo")
	firstID := object.LocalID("/whoson/first")
	targets := []interface{}{"https://foo.test/jrandom/noexist", 5,
		string("https://foo.test/" + fooID), "https://other.place/blah",
		string("https://foo.test/" + firstID)}
	storage.objects[fooID] = map[string]interface{}{"@id": fooID,
		"@type": "Note", "attributedTo": object.LocalID("/jrandom")}
	storage.objects[firstID] = map[string]interface{}{"@id": firstID,
		"@type": "Note", "attributedTo": object.LocalID("/whoson")}
	// Run test
	err := core.distributeReplies(tx, "ReplyOfDoom",
		object.LocalID("/jrandom"), targets)
	if err != nil {
		t.Fatal("distributeReplies error:", err)
	}
	expected := map[object.LocalID]map[string][]interface{}{
		"/jrandom/foo": map[string][]interface{}{
			"replies": []interface{}{"ReplyOfDoom"}}}
	if !reflect.DeepEqual(storage.collections, expected) {
		t.Fatal("distributeReplies failure:", storage.collections)
	}
}

// --- Accounts --- //

func TestNewAccount(t *testing.T) {
	// Setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	//tx, _ := storage.StartTransaction()
	// Run test
	err := core.NewAccount(Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},
		"/foo", "", ""})
	if err != nil {
		t.Fatal("NewAccount error:", err)
	}
	expected := map[string]Account{
		"Foo":Account{"Foo", [16]byte{}, 1, 2, 3, []byte{}, "/foo", "", ""},
		}
	if !reflect.DeepEqual(expected, storage.accounts) {
		t.Fatal("NewAccount failure:", storage.accounts)
	}
}

func TestLoadAccountExternal(t *testing.T) {
	// Setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// Run test
	storage.accounts["Foo"] = Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},
		"/foo", "", ""}
	acct, err := core.LoadAccount("Foo")
	if err != nil {
		t.Fatal("LoadAccount error:", err)
	}
	expected := Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},
		"/foo", "https://foo.test/foo", ""}
	if !reflect.DeepEqual(acct, expected) {
		t.Fatal("LoadAccount failure:", acct)
	}
}

func TestUpdateAccountExternal(t *testing.T) {
	// Setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// Run test
	storage.accounts["Foo"] = Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},
		"/foo", "", ""}
	err := core.UpdateAccount("Foo", Account{"Foo", [16]byte{}, 1, 2, 3,
		[]byte{},"/foobar", "", ""})
	if err != nil {
		t.Fatal("UpdateAccount error:", err)
	}
	expected := Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},"/foobar", "", ""}
	if !reflect.DeepEqual(storage.accounts["Foo"], expected) {
		t.Fatal("UpdateAccount failure:", storage.accounts)
	}
}

func TestDeleteAccountExternal(t *testing.T) {
	// Setup core
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	core.AddActor("jrandom", "Person")
	// Run test
	storage.accounts["Foo"] = Account{"Foo", [16]byte{}, 1, 2, 3, []byte{},
		"/foo", "", ""}
	err := core.DeleteAccount("Foo")
	if err != nil {
		t.Fatal("DeleteAccount error:", err)
	}
	if len(storage.accounts) > 0 {
		t.Fatal("DeleteAccount failure:", storage.accounts)
	}
}
