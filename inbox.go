package activitystreams

import (
	//"fmt"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func (c *Core) InboxAccept(tx Transaction, targetID object.LocalID, activity map[string]interface{}) (err error) {
	temp, ok := activity["@id"]
	if !ok {
		return object.BrokenObject
	}
	// The activityID is used for comparison and changeset storage.
	// For simplicity we force it to be a global.
	var activityID string
	switch temp.(type) {
	case string: // Must be from another server
		activityID = temp.(string)
	case object.LocalID: // Must be a local propagation
		activityID = GlobalizeLocal(temp.(object.LocalID), c.Hostname, c.Port)
	default:
		return object.BrokenObject
	}
	// Load the accepted object, see what type it was
	temp, ok = activity["object"]
	if !ok {
		return object.BrokenObject
	}
	var respondeeID object.LocalID
	switch temp.(type) {
	case string:
		loc, host, port, err := object.LocalIDFromGlobalID(temp.(string))
		if err != nil { // No object we have
			return nil
		}
		if host != c.Hostname || port != c.Port {
			return nil
		}
		respondeeID = loc
	case object.LocalID:
		respondeeID = temp.(object.LocalID)
	default:
		return object.BrokenObject
	}
	respondee, err := tx.LoadObject(respondeeID)
	if err == nil {
		// check that the target was an object this actor sent
		respondeeSource, _ := object.GetLocalID(respondee, "attributedTo")
		eq := object.CompareIDsNative(respondeeSource, targetID, c.Hostname,
			c.Port)
		if !eq {
			// The actor of this inbox is not the source of the responded
			// to object.
			return nil
		}
		respondeeType, _ := object.GetStr(respondee, "@type")
		switch respondeeType {
		case "Follow":
			// Check that the respondee was targeting the actor we are getting
			// this Activity from.
			// originalTarget is the Actor we tried to follow.
			originalTarget, err := object.GetStr(respondee, "object")
			if err != nil {
				return object.BrokenObject
			}
			// This Accept needs to be the same Actor that we sent Follow to.
			activitySource, ok := activity["attributedTo"]
			if !ok {
				return object.BrokenObject
			}
			// Compare the original target with the activity source
			if !object.CompareIDsNative(originalTarget, activitySource, c.Hostname, c.Port) {
				// In-applicable responder
				return nil
			}
			// We now know that originalTarget is native. Get the local.
			// Add to following list
			temp2, err := tx.LoadObjectField(targetID, "following")
			if err != nil {
				return err
			}
			var followingID object.LocalID
			switch temp2.(type) {
			case object.LocalID:
				followingID = temp2.(object.LocalID)
			default:
				return BadActorConstruction
			}
			chg := Change{Action: CSListRemove, ObjectID: followingID,
				FieldName: "items", Value: originalTarget}
			err = tx.ListAppend(followingID, "items", originalTarget)
			if err != nil {
				return err
			}
			err = tx.StoreChangeset(activityID, []Change{chg})
			if err != nil {
				return err
			}
		case "Invite": // TODO inbox
		case "Join": // TODO inbox
		default:
		}
	} else if err != object.NoObject {
		// Real error, fail
		return err
	}
	// If err == NoObject then we have nothing to target, store and move on.
	return nil
}

// Flag
// Follow
// Ignore
// Invite
// Join
// Leave

func (c *Core) InboxLike(tx Transaction, inboxActor object.LocalID, activity map[string]interface{}) (err error) {
	// Find our target object
	temp, err := object.GetStr(activity, "object")
	if err != nil {
		return err
	}
	likeTarget := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if likeTarget == object.LocalID("") {
		return object.NoObject
	}
	// Check object ownership
	owningActor, err := c.NativeObjectOwner(tx, likeTarget)
	if err != nil {
		return err
	}
	if owningActor != inboxActor {
		// We aren't the important target, skip
		return nil
	}
	// Add this Like id to its "likes" collection
	err = tx.ObjcolAdd(likeTarget, "likes", activity["@id"])
	if err != nil {
		return err
	}
	return nil
}

// Undo
// Update
