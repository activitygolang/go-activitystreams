package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"testing"
	"reflect"
)

func Test_activityBeenSighted(t *testing.T) {
	// Set up environment
	storage := NewDummyStorage()
	tx, err := storage.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error", err)
	}
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	// Set up seen in cache
	seenID := "https://bar.test/seen"
	storage.cache[seenID] = map[string]interface{}{"@id": seenID,
		"@type": "Note"}
	// Test already seen
	sighted, err := core.activityBeenSighted(tx,
		map[string]interface{}{"@id":seenID})
	if err != nil {
		t.Fatal("activityBeenSighted error:", err)
	}
	if !sighted {
		t.Fatal("activityBeenSighted already seen failure")
	}
	// Test not seen
	sighted, err = core.activityBeenSighted(tx,
		map[string]interface{}{"@id":"someOtherID"})
	if err != nil {
		t.Fatal("activityBeenSighted error:", err)
	}
	if sighted {
		t.Fatal("activityBeenSighted not seen failure")
	}
}

func Test_ghostReplyTargets(t *testing.T) {
	// Set up environment
	storage := NewDummyStorage()
	tx, err := storage.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error", err)
	}
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	// Test
	testObj := map[string]interface{}{"@id": "https://bar.test",
		"@type": "Note",
		"to": "https://foo.test/one/followers", // Trigger ghost tos
		"cc": "https://bar.test/them", // propagation, this and above
		"inReplyTo": "https://foo.test/one/1234", // Trigger ghost replies
	}
	local, err := core.ghostReplyTargets(tx, testObj)
	if err != nil {
		t.Fatal("ghostReplyTargets error:", err)
	}
	if local == nil {
		t.Fatal("ghostReplyTargets no results failure")
	}
	expectedLocal := []interface{}{object.LocalID("/one/followers")}
	if !reflect.DeepEqual(local, expectedLocal) {
		t.Fatal("ghostReplyTargets local failure:", local,)
	}
}

func TestHasGhostTos(t *testing.T) {
	// Test no targets
	falseObj := map[string]interface{}{"to": "https://baz.test/blah",
		"cc":       []interface{}{"https://baz.test/bluh"},
		"audience": "https://baz.test/bloh"}
	targets := HasGhostTos(falseObj, "foo.test", 0)
	if targets != nil {
		t.Fatal("HasGhostTos no targets failure", targets)
	}
	// Test targets available
	trueObj := map[string]interface{}{"to": "", "cc": nil,
		"audience": []interface{}{"https://foo.test/localUser"}}
	targets = HasGhostTos(trueObj, "foo.test", 0)
	expected := []object.LocalID{"/localUser"}
	if !reflect.DeepEqual(targets, expected) {
		t.Fatal("HasGhostTos has targets failure", targets)
	}
}

func TestGhostTargets(t *testing.T) {
	// Dummy database and transaction
	storage := NewDummyStorage()
	tx, err := storage.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error", err)
	}
	// Set up recursive object
	recursiveID := object.LocalID("/foo/bar")
	storage.objects[recursiveID] = map[string]interface{}{"@id": recursiveID,
		"@type": "Note", "inReplyTo": "https://foo.test/ding/dong"}
	// Test no targets
	falseObj := map[string]interface{}{"inReplyTo": nil, "object": "",
		"target": "", "tag": []interface{}{}}
	hasTargets := HasGhostTargets(tx, falseObj, "foo.test", 0, 5)
	if hasTargets {
		t.Fatal("HasGhostTargets no targets failure")
	}
	// Test recursive target available
	trueObj := map[string]interface{}{
		"inReplyTo": "https://foo.test" + string(recursiveID)}
	hasTargets = HasGhostTargets(tx, trueObj, "foo.test", 0, 5)
	if !hasTargets {
		t.Fatal("HasGhostTargets have targets failure")
	}
}
