package database

import (
	//"fmt"
	goap "gitlab.com/activitygolang/go-activitystreams"
	"gitlab.com/activitygolang/go-activitystreams/object"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"reflect"
	"testing"
)

func TestStoreProp(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// Object exists?
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo/bar", "prop").
		WillReturnRows(itemRows)
	// Store Propagation
	mock.ExpectExec("delete from PROPAGATION").
		WithArgs("/foo/bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("insert into PROPAGATION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "https://foo.test/someone", false, 0, "")
	mock.ExpectExec("insert into PROPAGATION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "/noone", true, 3, "someday")
	// Store Object
	itemRows = sqlmock.NewRows([]string{"ID"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo/bar", "prop").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into OBJECT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "Note", "/foo", "prop", 0)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar", "@id", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar$@id", 0, LOCALID, "/foo/bar", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar", "@type", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar$@type", 0, STRING, "Note", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar", "attributedTo", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar$attributedTo", 0, LOCALID, "/foo", 0, 0.0,
			false)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar", "blah", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("prop/foo/bar$blah", 0, STRING, "jabber", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	propObj := map[string]interface{}{"@id": object.LocalID("/foo/bar"),
		"@type": "Note", "attributedTo": object.LocalID("/foo"),
		"blah": "jabber"}
	targets := []goap.PropRetryTarget{
		goap.NewPropRetryGlobal("https://foo.test/someone"),
		goap.PropRetryTarget{Url: "/noone", Cluster: true,
			TryCount: 3, LastTry: "someday"}}
	err = tx.StoreProp(propObj, targets)
	if err != nil {
		t.Fatal("StoreProp error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreProp: unfulfilled expectations:", err)
	}
}

func TestGetPropIDs(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"ID"}).
		AddRow("/foo/abc").
		AddRow("/bar/def")
	mock.ExpectQuery("select ID from PROPAGATION").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	queue, err := tx.GetPropIDs()
	if err != nil {
		t.Fatal("GetPropIDs error:", err)
	}
	expected := []object.LocalID{object.LocalID("/foo/abc"),
		object.LocalID("/bar/def")}
	if !reflect.DeepEqual(queue, expected) {
		t.Fatal("GetPropIDs failure:", queue)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("GetPropIDs: unfulfilled expectations:", err)
	}
}

func TestGetPropObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo/bar", "prop").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("@id", 1).
		AddRow("@type", 1).
		AddRow("attributedTo", 1).
		AddRow("subject", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("prop/foo/bar").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(LOCALID, "FailID", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("prop/foo/bar$@id").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "FailType", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("prop/foo/bar$@type").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(LOCALID, "FailOwner", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("prop/foo/bar$attributedTo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "LtUaE", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("prop/foo/bar$subject").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.GetPropObject(object.LocalID("/foo/bar"))
	if err != nil {
		t.Fatal("GetPropObject error:", err)
	}
	expected := map[string]interface{}{"@id": object.LocalID("/foo/bar"),
		"@type": "Note", "attributedTo": object.LocalID("/foo"),
		"subject": "LtUaE"}
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("GetPropObject failure:", obj)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("GetPropObject: unfulfilled expectations:", err)
	}
}

func TestGetPropTargets(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"URL", "CLUSTER", "TRYCOUNT",
		"LASTTRY"}).
		AddRow("https://foo.test/someone", false, 0, "").
		AddRow("/noone", true, 3, "someday")
	mock.ExpectQuery("select URL, CLUSTER, TRYCOUNT, LASTTRY from PROPAGATION").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	queue, err := tx.GetPropTargets(object.LocalID("/foo/bar"))
	if err != nil {
		t.Fatal("GetPropTargets error:", err)
	}
	expected := []goap.PropRetryTarget{
		goap.NewPropRetryGlobal("https://foo.test/someone"),
		goap.PropRetryTarget{Url: "/noone", Cluster: true, TryCount: 3,
			LastTry: "someday"}}
	if !reflect.DeepEqual(queue, expected) {
		t.Fatal("GetPropTargets failure:", queue)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("GetPropTargets: unfulfilled expectations:", err)
	}
}

func TestUpdatePropTarget(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// Target doesn't exist
	itemRows := sqlmock.NewRows([]string{"TRYCOUNT"})
	mock.ExpectQuery("select TRYCOUNT from PROPAGATION").
		WithArgs("/foo/bar", "https://foo.test/someone").
		WillReturnRows(itemRows)
	// Target exists
	itemRows = sqlmock.NewRows([]string{"TRYCOUNT"}).
		AddRow(0)
	mock.ExpectQuery("select TRYCOUNT from PROPAGATION").
		WithArgs("/foo/bar", "https://foo.test/someone").
		WillReturnRows(itemRows)
	mock.ExpectExec("update PROPAGATION").
		WithArgs(3, "sometime", "/foo/bar", "https://foo.test/someone").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Target doesn't exist
	err = tx.UpdatePropTarget(object.LocalID("/foo/bar"),
		goap.PropRetryTarget{Url: "https://foo.test/someone", TryCount: 3,
			LastTry: "sometime"})
	if err != goap.BadPropagationTarget {
		t.Fatal("UpdatePropTarget error:", err)
	}
	// Target exists
	err = tx.UpdatePropTarget(object.LocalID("/foo/bar"),
		goap.PropRetryTarget{Url: "https://foo.test/someone", TryCount: 3,
			LastTry: "sometime"})
	if err != nil {
		t.Fatal("UpdatePropTarget error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("UpdatePropTarget: unfulfilled expectations:", err)
	}
}

func TestRemovePropTarget(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// Has remaining targets
	mock.ExpectExec("delete from PROPAGATION").
		WithArgs("/foo/bar", "https://foo.test/someone").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows := sqlmock.NewRows([]string{"URL", "CLUSTER", "TRYCOUNT",
		"LASTTRY"}).
		AddRow("https://foo.test/noone", false, 3, "someday")
	mock.ExpectQuery("select URL, CLUSTER, TRYCOUNT, LASTTRY from PROPAGATION").
		WillReturnRows(itemRows)
	// No remaining targets
	mock.ExpectExec("delete from PROPAGATION").
		WithArgs("/foo/bar", "https://foo.test/someone").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"URL", "CLUSTER", "TRYCOUNT",
		"LASTTRY"})
	mock.ExpectQuery("select URL, CLUSTER, TRYCOUNT, LASTTRY from PROPAGATION").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from OBJECT").
		WithArgs("/foo/bar", "prop").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("delete from FIELDS").
		WithArgs("prop/foo/bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"STRING"})
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("prop/foo/bar", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("prop/foo/bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Has remaining targets
	err = tx.RemovePropTarget(object.LocalID("/foo/bar"),
		"https://foo.test/someone")
	if err != nil {
		t.Fatal("RemovePropTarget error:", err)
	}
	// No remaining targets
	err = tx.RemovePropTarget(object.LocalID("/foo/bar"),
		"https://foo.test/someone")
	if err != nil {
		t.Fatal("RemovePropTarget error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("RemovePropTarget: unfulfilled expectations:", err)
	}
}
