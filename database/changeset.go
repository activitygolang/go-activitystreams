package database

import (
	"strconv"
	_ "github.com/mattn/go-sqlite3"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

func (t *DBTx) StoreChangeset(changeID string, changeset []goap.Change) (err error) {
	t.debug(DEBUG, "StoreChangeset", changeID, changeset)
	// check that change id doesn't exist
	data, err := t.tx.Query(
		"select ACTID from CHANGESETS where ACTID = $1 and CHGINDEX = $2",
		changeID, 0)
	if err != nil {
		return err
	}
	defer data.Close()
	if data.Next() {
		return goap.DuplicateItem
	}
	for index, change := range changeset {
		// store Change
		tC, vS, vI, vF, vB, err := ValueToTypes(change.Value)
		if err != nil {
			return err
		}
		if tC == EMBED {
			// generate ID
			vS = "cset" + changeID + "$" + strconv.Itoa(index)
			// store object
			err = t.storeObjectData(vS, change.Value.(map[string]interface{}))
			if err != nil {
				return err
			}
		} else if tC == LIST {
			// generate ID
			vS = "cset" + changeID + "$" + strconv.Itoa(index)
			// build fake object around list
			fake := map[string]interface{}{"list": change.Value}
			// store object
			err = t.storeObjectData(vS, fake)
			if err != nil {
				return err
			}
		}
		_ , err = t.tx.Exec("insert into CHANGESETS (ACTID, CHGINDEX, ACTION, OBJID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);",
			changeID, index, change.Action, change.ObjectID,
			change.FieldName, tC, vS, vI, vF, vB)
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *DBTx) LoadChangeset(changeID string) (set []goap.Change, err error) {
	t.debug(DEBUG, "LoadChangeset", changeID)
	// select where ACTID, ordered
	data, err := t.tx.Query("select CHGINDEX, ACTION, OBJID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL from CHANGESETS where ACTID = $1 order by CHGINDEX asc",
		changeID)
	if err != nil {
		return nil, err
	}
	defer data.Close()
	set = make([]goap.Change, 0)
	for data.Next() {
		var chg goap.Change
		var vType, index int
		var vString string
		var vInteger int64
		var vBoolean bool
		var vFloat float64
		err = data.Scan(&index, &chg.Action, &chg.ObjectID, &chg.FieldName,
			&vType, &vString, &vInteger, &vFloat, &vBoolean)
		if err != nil {
			return nil, err
		}
		if vType == EMBED {
			// load object
			embedID := "cset" + changeID + "$" + strconv.Itoa(index)
			chg.Value, err = t.loadObjectData(embedID)
			if err != nil {
				return nil, err
			}
		} else if vType == LIST {
			embedID := "cset" + changeID + "$" + strconv.Itoa(index)
			// load list fake object
			chg.Value, err = t.loadObjectData(embedID)
			if err != nil {
				return nil, err
			}
			// Extract list from fake object
			chg.Value = chg.Value.(map[string]interface{})["list"]
		} else {
			chg.Value, err = TypesToValue(vType, vString, vInteger, vFloat,
				vBoolean)
			if err != nil {
				return nil, err
			}
		}
		set = append(set, chg)
		index++
	}
	if len(set) == 0 {
		return nil, goap.NoChangeset
	} else {
		return set, nil
	}
}
