package database

import (
	"reflect"
	goap "gitlab.com/activitygolang/go-activitystreams"
	"gitlab.com/activitygolang/go-activitystreams/object"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func TestStoreAccount(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// existing account
	itemRows := sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"}).
		AddRow("salty", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	// succesful store
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	// insert
	mock.ExpectExec("insert into ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "abcdefghijklmnop", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectCommit()

	// Run tests
	var tx goap.Transaction
	tx, err = db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// existing account
	err = tx.StoreAccount(goap.Account{
		User: "foo",
		Salt: [16]byte{},
		HashPW: []byte{},
		ActorID: "",
	})
	if err != object.ObjectPresent {
		t.Fatal("StoreAccount error:", err)
	}
	// successful store
	err = tx.StoreAccount(goap.Account{
		User: "foo",
		Salt: [16]byte{
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		},
		Time: 1, Memory: 2, KeyLen: 3,
		HashPW: []byte{'b', 'l', 'a', 'h'},
		ActorID: "/foo",
		CreationDate: "2020",
	})
	if err != nil {
		t.Fatal("StoreAccount error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreAccount: unfulfilled expectations:", err)
	}
}

func TestLoadAccount(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// no account
	itemRows := sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	// existing account
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"}).
		AddRow("salty", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	var tx goap.Transaction
	tx, err = db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// no account
	acct, err := tx.LoadAccount("foo")
	if err != object.NoObject {
		t.Fatal("LoadAccount error:", err)
	}
	if !reflect.DeepEqual(acct, goap.Account{}) {
		t.Fatal("LoadAccount no account failure:", acct)
	}
	// existing account
	acct, err = tx.LoadAccount("foo")
	if err != nil {
		t.Fatal("LoadAccount error:", err)
	}
	expected := goap.Account{
		"foo",
		[16]byte{'s', 'a', 'l', 't', 'y'},
		1, 2, 3,
		[]byte{'b', 'l', 'a', 'h'},
		"/foo",
		"",
		"2020",
	}
	if !reflect.DeepEqual(acct, expected) {
		t.Fatal("LoadAccount no account failure:", acct)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadAccount: unfulfilled expectations:", err)
	}
}

func TestUpdateAccount(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// no account
	itemRows := sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	// existing account
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"}).
		AddRow("salty", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo", "abcdefghijklmnop", 1, 2, 3, "blah", "/foo", "2020")
	// existing account, change name
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"}).
		AddRow("salty", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("foo").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("bar").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	itemRows = sqlmock.NewRows([]string{"SALT", "HASHTIME", "MEMORY", "KEYLEN", "HASHED", "ACTOR", "CREATION"})
	mock.ExpectQuery("select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT").
		WithArgs("bar").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("bar", "abcdefghijklmnop", 1, 2, 3, "blah", "/foo", "2020")
	mock.ExpectCommit()

	// Run tests
	var tx goap.Transaction
	tx, err = db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// no account
	err = tx.UpdateAccount("foo", goap.Account{
		User: "foo",
		Salt: [16]byte{
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		},
		Time: 1, Memory: 2, KeyLen: 3,
		HashPW: []byte{'b', 'l', 'a', 'h'},
		ActorID: "",
		CreationDate: "2020",
	})
	if err != object.NoObject {
		t.Fatal("UpdateAccount error:", err)
	}
	// existing account
	err = tx.UpdateAccount("foo", goap.Account{
		User: "foo",
		Salt: [16]byte{
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		},
		Time: 1, Memory: 2, KeyLen: 3,
		HashPW: []byte{'b', 'l', 'a', 'h'},
		ActorID: "/foo",
		CreationDate: "2020",
	})
	if err != nil {
		t.Fatal("UpdateAccount error:", err)
	}
	// existing account, name change
	err = tx.UpdateAccount("foo", goap.Account{
		User: "bar",
		Salt: [16]byte{
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		},
		Time: 1, Memory: 2, KeyLen: 3,
		HashPW: []byte{'b', 'l', 'a', 'h'},
		ActorID: "/foo",
		CreationDate: "2020",
	})
	if err != nil {
		t.Fatal("UpdateAccount error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("UpdateAccount: unfulfilled expectations:", err)
	}
}

func TestDeleteAccount(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from ACCOUNT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("foo")
	mock.ExpectCommit()

	// Run tests
	var tx goap.Transaction
	tx, err = db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.DeleteAccount("foo")
	if err != nil {
		t.Fatal("DeleteAccount error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("DeleteAccount: unfulfilled expectations:", err)
	}
}
