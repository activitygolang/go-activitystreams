package database

import (
	//"fmt"
	//"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/activitygolang/go-activitystreams/object"
	//goap "gitlab.com/activitygolang/go-activitystreams"
)

func (t *DBTx) ObjcolAdd(objectID object.LocalID, fieldName string, value interface{}) (err error) {
	t.debug(DEBUG, "ObjcolAdd", objectID, fieldName, value)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	tC, vS, vI, vF, vB, err := ValueToTypes(value)
	if err != nil {
		return err
	}
	_, err = t.tx.Exec(
		"insert into COLLECTION (ID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL) values ($1, $2, $3, $4, $5, $6, $7)",
		string(objectID), fieldName, tC, vS, vI, vF, vB)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) ObjcolRM(objectID object.LocalID, fieldName string, value interface{}) (err error) {
	t.debug(DEBUG, "ObjcolRM", objectID, fieldName, value)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	// Get the internal form
	tC, vS, vI, vF, vB, err := ValueToTypes(value)
	if err != nil {
		return err
	}
	_, err = t.tx.Exec(
		"delete from COLLECTION where ID = $1 and FIELD = $2 and TYPE = $3 and STRING = $4 and INTEGER = $5 and FLOAT = $6 and BOOL = $7",
		string(objectID), fieldName, tC, vS, vI, vF, vB)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) ObjcolGetAllField(objectID object.LocalID, fieldName string) (values []interface{}, err error) {
	t.debug(DEBUG, "ObjcolGetAllField", objectID, fieldName)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return nil, err
	}
	data, err := t.tx.Query("select TYPE, STRING, INTEGER, FLOAT, BOOL from COLLECTION where ID = $1 and FIELD = $2",
		string(objectID), fieldName)
	if err != nil {
		return nil, err
	}
	defer data.Close()
	var vType int
	var temp interface{}
	// We return a list, but if single==1 the caller will end up turning it
	// into a scalar
	values = make([]interface{}, 0)
	for data.Next() {
		temp, vType, err = loadValue(data)
		if err != nil {
			return nil, err
		}
		if vType == EMBED {
			// Retrieve sub-object
			temp, err = t.loadObjectData(temp.(string))
			if err != nil {
				return nil, err
			}
		}
		values = append(values, temp)
	}
	return values, nil
}

func (t *DBTx) ObjcolGetFields(objectID object.LocalID) (fields []string, err error) {
	t.debug(DEBUG, "ObjcolGetFields", objectID)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return nil, err
	}
	// Get all fields
	data, err := t.tx.Query(
		"select distinct FIELD from COLLECTION where ID = $1",
		string(objectID))
	if err != nil {
		return nil, err
	}
	defer data.Close()
	for data.Next() {
		var temp string
		err = data.Scan(&temp)
		if err != nil {
			return nil, err
		}
		fields = append(fields, temp)
	}
	return fields, nil
}

func (t *DBTx) ObjcolRMAll(objectID object.LocalID) (err error) {
	t.debug(DEBUG, "ObjcolRMAll", objectID)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	_, err = t.tx.Exec("delete from COLLECTION where ID = $1", string(objectID))
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) ObjcolGetAll(objectID object.LocalID) (data map[string][]interface{}, err error) {
	t.debug(DEBUG, "ObjcolGetAll", objectID)
	fields, err := t.ObjcolGetFields(objectID)
	if err != nil {
		return nil, err
	}
	data = make(map[string][]interface{}, len(fields))
	for _, field := range fields {
		fieldData, err := t.ObjcolGetAllField(objectID, field)
		if err != nil {
			return nil, err
		}
		data[field] = fieldData
	}
	return data, nil
}

func (t *DBTx) ObjcolSet(objectID object.LocalID, data map[string][]interface{}) (err error) {
	t.debug(DEBUG, "ObjcolSet", objectID, data)
	// First clear the way
	err = t.ObjcolRMAll(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	for fieldName, fieldData := range data {
		for _, value := range fieldData {
			tC, vS, vI, vF, vB, err := ValueToTypes(value)
			if err != nil {
				return err
			}
			_, err = t.tx.Exec(
				"insert into COLLECTION (ID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL) values ($1, $2, $3, $4, $5, $6, $7)",
				string(objectID), fieldName, tC, vS, vI, vF, vB)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
