package database

import (
	_ "github.com/mattn/go-sqlite3"
	goap "gitlab.com/activitygolang/go-activitystreams"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func (t *DBTx) StoreAccount(acct goap.Account) (err error) {
	t.debug(DEBUG, "StoreAccount", acct)
	// check that account does not exist
	_, err = t.LoadAccount(acct.User)
	if err == nil {
		return object.ObjectPresent
	} else if err != object.NoObject {
		return err
	}
	// store account
	_, err = t.tx.Exec("insert into ACCOUNT (USER, SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION) values ($1, $2, $3, $4, $5, $6, $7, $8)",
		acct.User,
		string(acct.Salt[:]),
		acct.Time,
		acct.Memory,
		acct.KeyLen,
		string(acct.HashPW),
		acct.ActorID,
		acct.CreationDate,
	)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) LoadAccount(user string) (acct goap.Account, err error) {
	t.debug(DEBUG, "LoadAccount", user)
	rows, err := t.tx.Query(
		"select SALT, HASHTIME, MEMORY, KEYLEN, HASHED, ACTOR, CREATION from ACCOUNT where USER = $1",
		user,
	)
	if err != nil {
		return goap.Account{}, err
	}
	defer rows.Close()
	if !rows.Next() {
		return goap.Account{}, object.NoObject
	} else {
		acct.User = user
		var salt string
		err = rows.Scan(&salt, &acct.Time, &acct.Memory, &acct.KeyLen,
			&acct.HashPW, &acct.ActorID, &acct.CreationDate)
		if err != nil {
			return goap.Account{}, err
		}
		temp := []byte(salt)
		for len(temp) < 16 {
			temp = append(temp, 0)
		}
		copy(acct.Salt[:], temp[:16])
	}
	return acct, nil
}

func (t *DBTx) UpdateAccount(user string, acct goap.Account) (err error) {
	t.debug(DEBUG, "UpdateAccount", user, acct)
	// check account exists, yes the is the dumb way, it is also simple
	_, err = t.LoadAccount(user)
	if err != nil { // if no object will bail here
		return err
	}
	if acct.User != user { // ensure we don't overwrite anything
		_, err = t.LoadAccount(acct.User)
		if err != object.NoObject {
			if err == nil {
				return object.ObjectPresent
			} else {
				return err
			}
		}
	}
	// delete account
	err = t.DeleteAccount(user)
	if err != nil {
		return err
	}
	// store new account
	err = t.StoreAccount(acct)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) DeleteAccount(user string) (err error) {
	t.debug(DEBUG, "DeleteAccount", user)
	_, err = t.tx.Exec("delete from ACCOUNT where USER = $1", user)
	if err != nil {
		return err
	}
	return nil
}
