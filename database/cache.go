package database

import (
	_ "github.com/mattn/go-sqlite3"
	//"gitlab.com/activitygolang/go-activitystreams/object"
	//goap "gitlab.com/activitygolang/go-activitystreams"
)

func (t *DBTx) StoreCache(obj map[string]interface{}) (err error) {
	// no debug here, handled by storeObject
	return t.storeObject(obj, "cache")
}

func (t *DBTx) LoadCache(objectID string) (obj map[string]interface{}, err error) {
	t.debug(DEBUG, "LoadCache", objectID)
	objType, owner, err := t.loadBasicObjectData(objectID, "cache")
	obj, err = t.loadObjectData("cache" + objectID)
	if err != nil {
		return nil, err
	}
	obj["@id"] = objectID
	obj["@type"] = objType
	obj["attributedTo"] = owner
	return obj, nil
}
