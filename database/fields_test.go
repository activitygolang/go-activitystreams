package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gitlab.com/activitygolang/go-activitystreams/object"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

func TestLoadObjectField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(true)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "quux", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("obj/foo$bar").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	value, err := tx.LoadObjectField(object.LocalID("/foo"), "bar")
	if err != nil {
		t.Fatal("LoadObjectField error:", err)
	}
	if !reflect.DeepEqual(value, "quux") {
		t.Fatal("LoadObjectField failure:", value)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadObjectField: unfulfilled expectations:", err)
	}
}

func TestListAppend(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// no-field ==============
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "none").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "none", 0)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$none", 0, STRING, "one", 0, 0.0, false)
	// single-field ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(1)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "single").
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDS").
		WithArgs(false, "obj/foo", "single").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$single", 1, STRING, "two", 0, 0.0, false)
	// list-field ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "list").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"MAXID"}).
		AddRow(1)
	mock.ExpectQuery("select max\\(ITEMINDEX\\) as MAXI from FIELDDATA").
		WithArgs("obj/foo$list").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$list", 2, STRING, "three", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test no-field
	err = tx.ListAppend(object.LocalID("/foo"), "none", "one")
	if err != nil {
		t.Fatal("ListAppend no-field error:", err)
	}
	// Test single-field
	err = tx.ListAppend(object.LocalID("/foo"), "single", "two")
	if err != nil {
		t.Fatal("ListAppend single-field error:", err)
	}
	// Test list-field
	err = tx.ListAppend(object.LocalID("/foo"), "list", "three")
	if err != nil {
		t.Fatal("ListAppend list-field error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ListAppend: unfulfilled expectations:", err)
	}
}

func TestListRemove(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// no-field ==============
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	// no-remove ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"})
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("obj/foo$bar", STRING, "quux", 0, 0.0, false).
		WillReturnRows(itemRows)
	// remove ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(2).
		AddRow(4)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("obj/foo$bar", STRING, "quux", 0, 0.0, false).
		WillReturnRows(itemRows)
	// delete 2, shift 3-5 down
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("obj/foo$bar", 2).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(3).
		AddRow(4).
		AddRow(5)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("obj/foo$bar", 3).
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDDATA").
		WithArgs(2, "obj/foo$bar", 3).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update FIELDDATA").
		WithArgs(3, "obj/foo$bar", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update FIELDDATA").
		WithArgs(4, "obj/foo$bar", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	// delete 4 (now 3), shift 4 down
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("obj/foo$bar", 3).
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(4)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("obj/foo$bar", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDDATA").
		WithArgs(3, "obj/foo$bar", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test no-field
	err = tx.ListRemove("/foo", "bar", "quux")
	if err != object.NoField {
		t.Fatal("ListRemove error:", err)
	}
	// Test nothing to remove
	err = tx.ListRemove("/foo", "bar", "quux")
	if err != nil {
		t.Fatal("ListRemove error:", err)
	}
	// Test removal
	err = tx.ListRemove("/foo", "bar", "quux")
	if err != nil {
		t.Fatal("ListRemove error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ListRemove: unfulfilled expectations:", err)
	}
}

func TestAddField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// field exists ==============
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	// add field ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "bar", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$bar", 0, STRING, "quux", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test field already exists
	err = tx.AddField("/foo", "bar", "quux")
	if err != goap.DuplicateItem {
		t.Fatal("AddField error:", err)
	}
	// Test add
	err = tx.AddField("/foo", "bar", "quux")
	if err != nil {
		t.Fatal("AddField error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("AddField: unfulfilled expectations:", err)
	}
}

func TestUpdateField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// field doesn't exist ==============
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	// field exists ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"STRING"})
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("obj/foo$bar", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("obj/foo$bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "bar", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$bar", 0, STRING, "quux", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test field already exists
	err = tx.UpdateField("/foo", "bar", "quux")
	if err != object.NoField {
		t.Fatal("RemoveField error:", err)
	}
	// Test add
	err = tx.UpdateField("/foo", "bar", "quux")
	if err != nil {
		t.Fatal("RemoveField error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("RemoveField: unfulfilled expectations:", err)
	}
}

func TestRemoveField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// field doesn't exist ==============
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	// field exists ==============
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(0)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"STRING"})
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("obj/foo$bar", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDS").
		WithArgs("obj/foo", "bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("obj/foo$bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test field already exists
	err = tx.RemoveField("/foo", "bar")
	if err != object.NoField {
		t.Fatal("RemoveField error:", err)
	}
	// Test add
	err = tx.RemoveField("/foo", "bar")
	if err != nil {
		t.Fatal("RemoveField error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("RemoveField: unfulfilled expectations:", err)
	}
}

func Test_loadSingle(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// field doesn't exist ==============
	itemRows := sqlmock.NewRows([]string{"SINGLE"})
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("/foo", "bar").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"SINGLE"}).
		AddRow(1)
	mock.ExpectQuery("select SINGLE from FIELDS").
		WithArgs("/foo", "bar").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test no field
	single, err := tx.(*DBTx).loadSingle("/foo", "bar")
	if err != object.NoField {
		t.Fatal("loadSingle error:", err)
	}
	// Test field exists and is single
	single, err = tx.(*DBTx).loadSingle("/foo", "bar")
	if err != nil {
		t.Fatal("loadSingle error:", err)
	}
	if !single {
		t.Fatal("loadSingle failure")
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("loadSingle: unfulfilled expectations:", err)
	}
}

func TestValueInObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(1)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("objfoo$bar", STRING, "quux", 0, 0.0, false).
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ITEMINDEX"})
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("objfoo$bar", STRING, "none", 0, 0.0, false).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test present
	found, err := tx.ValueInObject("foo", "bar", "quux")
	if err != nil {
		t.Fatal("ValueInObject present error:", err)
	}
	if !found {
		t.Fatal("ValueInObject present failure")
	}
	// Test not present
	found, err = tx.ValueInObject("foo", "bar", "none")
	if err != nil {
		t.Fatal("ValueInObject not present error:", err)
	}
	if found {
		t.Fatal("ValueInObject not present failure")
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ValueInObject: unfulfilled expectations:", err)
	}
}
