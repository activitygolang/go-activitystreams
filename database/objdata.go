package database

import (
	//"fmt"
	"sort"
	"strconv"
	"reflect"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func (t *DBTx) storeObject(obj map[string]interface{}, universe string) (err error) {
	t.debug(DEBUG, "storeObject", obj, universe)
	id, objType, owner, err := coreObjData(obj)
	if err != nil {
		return err
	}
	// check that object doesn't already exist
	// if objectExists
	exists, err := t.existsCore(object.LocalID(id), universe)
	if err != nil {
		return err
	}
	if exists {
		return object.ObjectPresent
	}
	// Store new OBJECT
	_, err = t.tx.Exec(
		`insert into OBJECT (ID, TYPE, OWNER, UNIVERSE, PROTECTED) values ($1, $2, $3, $4, $5);`,
		id, objType, owner, universe, 0)
	if err != nil {
		return err
	}
	err = t.storeObjectData(universe + id, obj)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) storeObjectData(objID string, obj map[string]interface{}) (err error) {
	t.debug(DEBUG, "storeObjectData", objID, obj)
	// Sort the fields for testability
	sortedFields := make([]string, len(obj))
	i := 0
	for fieldName, _ := range obj {
		sortedFields[i] = fieldName
		i++
	}
	sort.Strings(sortedFields)
	// Store each field
	for _, field := range sortedFields {
		err = t.storeField(objID, field, obj[field])
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *DBTx) storeField(objID string, fieldName string, value interface{}) (err error) {
	t.debug(DEBUG, "storeField", objID, fieldName, value)
	single := 1
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.Slice:
		fallthrough
	case reflect.Array:
		single = 0
	}
	// store FIELDS data
	_, err = t.tx.Exec(
		"insert into FIELDS (ID, NAME, SINGLE) values ($1, $2, $3)",
		objID, fieldName, single,
	)
	if err != nil {
		return err
	}
	idName := objID + "$" + fieldName
	// Store FIELDDATA
	if single == 1 {
		t.storeFieldItem(idName, 0, value)
		if err != nil {
			return err
		}
	} else {
		list, err := ListToInterfaceList(value)
		if err != nil {
			return err
		}
		for index, item := range list {
			err = t.storeFieldItem(idName, index, item)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (t *DBTx) storeFieldItem(idName string, index int, value interface{}) (err error) {
	t.debug(DEBUG, "storeFieldItem", idName, index, value)
	tC, vS, vI, vF, vB, err := ValueToTypes(value)
	if err != nil {
		return err
	}
	if tC == EMBED {
		// Generate internal ID
		embedID := "embed" + idName + "$" + strconv.Itoa(index)
		// Store the reference
		_, err = t.tx.Exec(
			`insert into FIELDDATA (IDNAME, ITEMINDEX, TYPE, STRING, INTEGER, FLOAT, BOOL) values ($1, $2, $3, $4, $5, $6, $7);`,
		idName, index, tC, embedID, vI, vF, vB)
		if err != nil {
			return err
		}
		// Recurse
		err = t.storeObjectData(embedID, value.(map[string]interface{}))
		if err != nil {
			return err
		}
	} else {
		// Basic value, store it
		_, err = t.tx.Exec(
			`insert into FIELDDATA (IDNAME, ITEMINDEX, TYPE, STRING, INTEGER, FLOAT, BOOL) values ($1, $2, $3, $4, $5, $6, $7);`,
		idName, index, tC, vS, vI, vF, vB)
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *DBTx) loadObjectData(storageID string) (obj map[string]interface{}, err error) {
	t.debug(DEBUG, "loadObjectData", storageID)
	// Get FIELDS data
	fields, err := t.tx.Query("select NAME, SINGLE from FIELDS where ID = $1",
		storageID)
	if err != nil {
		return nil, err
	}
	defer fields.Close()
	type fieldNode struct {
		name   string
		single int
	}
	fieldList := []fieldNode{}
	// loop, get field names and singles
	for fields.Next() {
		node := fieldNode{}
		err = fields.Scan(&node.name, &node.single)
		if err != nil {
			return nil, err
		}
		fieldList = append(fieldList, node)
	}
	fields.Close()
	// Field list loaded, now loop on that and load the data
	obj = make(map[string]interface{}, 0)
	for _, field := range fieldList {
		idName := storageID + "$" + field.name
		value, err := t.loadFieldItems(idName)
		if err != nil {
			return nil, err
		}
		if field.single > 0 {
			// Single item, return as scalar
			if len(value) > 0 {
				obj[field.name] = value[0]
			} else {
				obj[field.name] = nil
			}
		} else {
			// Possibly multi-item, return as list
			obj[field.name] = value
		}
	}
	return obj, nil
}

func (t *DBTx) loadFieldItems(idName string) (value []interface{}, err error) {
	t.debug(DEBUG, "loadFieldItems", idName)
	// get everything with ID, ordered by index
	query := "select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA where IDNAME = $1 order by ITEMINDEX asc;"
	data, err := t.tx.Query(query, idName)
	if err != nil {
		return nil, err
	}
	defer data.Close()
	var vType int
	var temp interface{}
	// We return a list, but if single==1 the caller will end up turning it
	// into a scalar
	value = make([]interface{}, 0)
	for data.Next() {
		temp, vType, err = loadValue(data)
		if err != nil {
			return nil, err
		}
		if vType == EMBED {
			// Retrieve sub-object
			temp, err = t.loadObjectData(temp.(string))
			if err != nil {
				return nil, err
			}
		}
		value = append(value, temp)
	}
	// Check for missing data
	if len(value) == 0 {
		return nil, object.NoField
	}
	return value, nil
}

func (t *DBTx) deleteObjectData(objID string) (err error) {
	t.debug(DEBUG, "deleteObjectData", objID)
	// delete FIELDS
	_, err = t.tx.Exec("delete from FIELDS where ID = $1;", objID)
	if err != nil {
		return err
	}
	// select where ID and EMBED
	embedIDs, err := t.selectEmbed(objID)
	if err != nil {
		return err
	}
	for _, embedID := range embedIDs {
		err = t.deleteObjectData(embedID)
		if err != nil {
			return err
		}
	}
	// delete FIELDDATA
	_, err = t.tx.Exec("delete from FIELDDATA where IDNAME = $1;", objID)
	if err != nil {
		return err
	}
	return nil
}

// Move everything from top through snipIndex up or down
func (t *DBTx) shiftEntries(idName string, snipIndex int64, movement int64) (err error) {
	t.debug(DEBUG, "shiftEntries", idName, snipIndex, movement)
	setExec := "update FIELDDATA set ITEMINDEX = $1 where IDNAME = $2 and ITEMINDEX = $3"
	queryRoot := "select ITEMINDEX from FIELDDATA where IDNAME = $1 and INDEX >= $2 order by ITEMINDEX "
	var data *sql.Rows
	// Get the block of rows that we are modifying
	if movement > 0 { // Move up
		// Get list
		data, err = t.tx.Query(queryRoot+"desc", idName, snipIndex)
		if err != nil {
			return err
		}
	} else if movement < 0 { // Move down
		data, err = t.tx.Query(queryRoot+"asc", idName, snipIndex)
		if err != nil {
			return err
		}
	} else { // movement == 0
		return nil // NOP, should this be an error?
	}
	defer data.Close()
	indices, err := convertBoxRowsToSlice(data)
	if err != nil {
		return err
	}
	// Have the rows in a convienent format, now move them around
	for _, index := range indices {
		newIndex := index + movement
		_, err = t.tx.Exec(setExec, newIndex, idName, index)
		if err != nil {
			return err
		}
	}
	return nil
}

// The caller is responsible for closing the rows
func convertBoxRowsToSlice(rows *sql.Rows) (indices []int64, err error) {
	indices = make([]int64, 0)
	for rows.Next() {
		var index int64
		err = rows.Scan(&index)
		if err != nil {
			return nil, err
		}
		indices = append(indices, index)
	}
	return indices, nil
}

func (t *DBTx) loadBasicObjectData(objectID string, universe string) (objType string, owner string, err error) {
	t.debug(DEBUG, "loadBasicObjectData", objectID, universe)
	data, err := t.tx.Query(
		"select TYPE, OWNER from OBJECT where ID = $1 and UNIVERSE = $2",
		string(objectID), universe)
	if err != nil {
		return "", "", err
	}
	defer data.Close()
	if !data.Next() {
		// No entry for this object.
		return "", "", object.NoObject
	}
	err = data.Scan(&objType, &owner)
	if err != nil {
		return "", "", err
	}
	data.Close()
	return objType, owner, nil
}

func (t *DBTx) deleteObject(objectID string, universe string) (err error) {
	t.debug(DEBUG, "deleteObject", objectID, universe)
	_, err = t.tx.Exec("delete from OBJECT where ID = $1 and UNIVERSE = $2;",
		objectID, universe)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) selectEmbed(idName string) (embedIDs []string, err error) {
	t.debug(DEBUG, "selectEmbed", idName)
	data, err := t.tx.Query(
		"select STRING from FIELDDATA where IDNAME = $1 and TYPE = $2",
		idName, EMBED)
	if err != nil {
		return nil, err
	}
	defer data.Close()
	embedIDs = make([]string, 0)
	for data.Next() {
		var eid string
		err = data.Scan(&eid)
		if err != nil {
			return nil, err
		}
		embedIDs = append(embedIDs, eid)
	}
	return embedIDs, nil
}
