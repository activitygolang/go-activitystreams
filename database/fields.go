package database

import (
	//"fmt"
	//"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/activitygolang/go-activitystreams/object"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

func (t *DBTx) LoadObjectField(objectID object.LocalID, fieldName string) (value interface{}, err error) {
	t.debug(DEBUG, "LoadObjectField", objectID, fieldName)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return nil, err
	}
	// load FIELDS
	single, err := t.loadSingle("obj" + string(objectID), fieldName)
	if err != nil {
		return nil, err
	}
	// loadFieldItems
	idName := "obj" + string(objectID) + "$" + fieldName
	temp, err := t.loadFieldItems(idName)
	if err != nil {
		return nil, err
	}
	if single {
		// Single item, return as scalar
		if len(temp) > 0 {
			value = temp[0]
		} else {
			value = nil
		}
	} else {
		value = temp
	}
	return value, nil
}

func (t *DBTx) ListAppend(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	t.debug(DEBUG, "ListAppend", objectID, fieldName, data)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	storageID := "obj" + string(objectID)
	// load FIELDS for the given field
	single, err := t.loadSingle(storageID, fieldName)
	if err == object.NoField {
		// No field, we can just store the field and have done with it
		err = t.storeField(storageID, fieldName, []interface{}{data})
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else {
		// determine if FIELDS:SINGLE needs to be changed
		var maxindex int
		idName := storageID + "$" + fieldName
		if single {
			// update SINGLE
			_, err = t.tx.Exec(
				"update FIELDS set SINGLE = $1 where ID = $2 and NAME = $3",
				false, storageID, fieldName)
			if err != nil {
				return err
			}
			// We know from SINGLE that the current max INDEX == 0
			maxindex = 0
		} else {
			// get max FIELDDATA index
			query := "select max(ITEMINDEX) as MAXI from FIELDDATA where IDNAME = $1"
			rows, err := t.tx.Query(query, idName)
			if err != nil {
				return err
			}
			defer rows.Close()
			if rows.Next() != true { // Nothing in DB, start at 0
				maxindex = -1
			} else {
				rows.Scan(&maxindex)
			}
		}
		// store FIELDDATA
		err = t.storeFieldItem(idName, maxindex + 1, data)
		if err != nil {
			return err
		}
	}
	return nil
}

func (t *DBTx) ListRemove(objectID object.LocalID, fieldName string, value interface{}) (err error) {
	t.debug(DEBUG, "ListRemove", objectID, fieldName, value)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	storageID := "obj" + string(objectID)
	// field exists?
	_, err = t.loadSingle(storageID, fieldName)
	if err != nil {
		return err
	}
	// convert check data to internal database format for matching
	tC, vS, vI, vF, vB, err := ValueToTypes(value)
	if err != nil {
		return err
	}
	idName := storageID + "$" + fieldName
	// select any match in FIELDDATA
	query := "select ITEMINDEX from FIELDDATA where IDNAME = $1 and TYPE = $2 and STRING = $3 and INTEGER = $4 and FLOAT = $5 and BOOL = $6 order by ITEMINDEX asc;"
	rows, err := t.tx.Query(query, idName, tC, vS, vI, vF, vB)
	if err != nil {
		return err
	}
	defer rows.Close()
	// extract indices
	indices, err := convertBoxRowsToSlice(rows)
	if err != nil {
		return err
	}
	// This removal-shift down loop is written for simplicity
	// A more efficient method would be to precalculate all the movements
	// so that no entry is ever moved more than once.
	indiceAdjustment := int64(0)
	for _, indice := range indices {
		targetIndex := indice + indiceAdjustment
		// delete match
		_, err = t.tx.Exec(
			"delete from FIELDDATA where IDNAME = $1 and ITEMINDEX = $2;",
			idName, targetIndex)
		if err != nil {
			return err
		}
		if tC == EMBED {
			err = t.deleteObjectData(vS)
			if err != nil {
				return err
			}
		}
		// shift everything down
		err = t.shiftEntries(idName, targetIndex + 1, -1)
		if err != nil {
			return err
		}
		indiceAdjustment--
	}
	return nil
}

func (t *DBTx)AddField(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	t.debug(DEBUG, "AddField", objectID, fieldName, data)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	storageID := "obj" + string(objectID)
	// load FIELDS for the given field
	_, err = t.loadSingle(storageID, fieldName)
	if err == object.NoField {
		// No field, we can add the new field
		err = t.storeField(storageID, fieldName, data)
		if err != nil {
			return err
		}
	} else if err != nil {
		return err
	} else {
		return goap.DuplicateItem
	}
	return nil
}

func (t *DBTx)UpdateField(objectID object.LocalID, fieldName string, data interface{}) (err error) {
	t.debug(DEBUG, "UpdateField", objectID, fieldName, data)
	err = t.RemoveField(objectID, fieldName)
	if err != nil {
		return err
	}
	err = t.AddField(objectID, fieldName, data)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx)RemoveField(objectID object.LocalID, fieldName string) (err error) {
	t.debug(DEBUG, "RemoveField", objectID, fieldName)
	_, _, err = t.BasicObjectData(objectID)
	if err != nil { // This will return NoObject if that is the case
		return err
	}
	storageID := "obj" + string(objectID)
	// load FIELDS for the given field
	_, err = t.loadSingle(storageID, fieldName)
	if err != nil {
		return err
	}
	idName := storageID + "$" + fieldName
	// Field exists, delete FIELDDATA and replace it
	embedIDs, err := t.selectEmbed(idName)
	for _, embedID := range embedIDs {
		// delete object data
		err = t.deleteObjectData(embedID)
		if err != nil {
			return err
		}
	}
	// delete field
	_, err = t.tx.Exec("delete from FIELDS where ID = $1 and NAME = $2;",
		storageID, fieldName)
	if err != nil {
		return err
	}
	_, err = t.tx.Exec("delete from FIELDDATA where IDNAME = $1;",
		idName)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) loadSingle(objectID string, fieldName string) (single bool, err error) {
	t.debug(DEBUG, "loadSingle", objectID, fieldName)
	fields, err := t.tx.Query(
		"select SINGLE from FIELDS where ID = $1 and NAME = $2",
		objectID, fieldName)
	if err != nil {
		return false, err
	}
	defer fields.Close()
	if !fields.Next() {
		return false, object.NoField
	} else {
		err = fields.Scan(&single)
		if err != nil {
			return false, err
		}
		return single, nil
	}
}

func (t *DBTx) ValueInObject(objectID object.LocalID, fieldName string, data interface{}) (found bool, err error) {
	t.debug(DEBUG, "ValueInObject", objectID, fieldName, data)
	storageID := "obj" + string(objectID)
	idName := storageID + "$" + fieldName
	// convert check data to internal database format for matching
	tC, vS, vI, vF, vB, err := ValueToTypes(data)
	if err != nil {
		return false, err
	}
	fields, err := t.tx.Query("select ITEMINDEX from FIELDDATA where IDNAME = $1 and TYPE = $2 and STRING = $3 and INTEGER = $4 and FLOAT = $5 and BOOL = $6",
		idName, tC, vS, vI, vF, vB)
	if err != nil {
		return false, err
	}
	defer fields.Close()
	return fields.Next(), nil
}
