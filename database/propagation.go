package database

import (
	_ "github.com/mattn/go-sqlite3"
	goap "gitlab.com/activitygolang/go-activitystreams"
	"gitlab.com/activitygolang/go-activitystreams/object"
	goobj "gitlab.com/activitygolang/go-activitystreams/object"
)

func (t *DBTx) StoreProp(obj map[string]interface{}, targets []goap.PropRetryTarget) (err error) {
	t.debug(DEBUG, "StoreProp", obj, targets)
	objID, err := object.GetLocalID(obj, "@id")
	if err != nil {
		return err
	}
	strID := string(objID)
	// Check that there isn't already an object here
	exists, err := t.existsCore(objID, "prop")
	if err != nil {
		t.debug(DEEPDEBUG, "StoreProp: prop object existence check:", err)
		return err
	}
	if exists {
		t.debug(DEEPDEBUG, "StoreProp: object already exists")
		return goobj.ObjectPresent
	}
	// Delete prop info
	_, err = t.tx.Exec("delete from PROPAGATION where ID = $1;", strID)
	if err != nil {
		return err
	}
	// Store prop info
	for _, target := range targets {
		_, err = t.tx.Exec("insert into PROPAGATION (ID, URL, CLUSTER, TRYCOUNT, LASTTRY) values ($1, $2, $3, $4, $5)",
			strID, target.Url, target.Cluster, target.TryCount, target.LastTry)
		if err != nil {
			return err
		}
	}
	err = t.storeObject(obj, "prop")
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) GetPropIDs() (propQueue []object.LocalID, err error) {
	t.debug(DEBUG, "GetPropIDs")
	// select every ID
	data, err := t.tx.Query("select ID from PROPAGATION")
	if err != nil {
		return nil, err
	}
	defer data.Close()
	propQueue = make([]object.LocalID, 0)
	for data.Next() {
		var id string
		err = data.Scan(&id)
		if err != nil {
			return nil, err
		}
		propQueue = append(propQueue, object.LocalID(id))
	}
	return propQueue, nil
}

func (t *DBTx) GetPropObject(objectID object.LocalID) (obj map[string]interface{}, err error) {
	t.debug(DEBUG, "GetPropObject", objectID)
	// TODO: this is a copy of LoadObject
	objType, ownerTemp, err := t.loadBasicObjectData(string(objectID), "prop")
	owner := object.LocalID(ownerTemp)
	// Now have ID, Type, Owner, Universe. Can load the rest of the object.
	obj, err = t.loadObjectData("prop" + string(objectID))
	if err != nil {
		return nil, err
	}
	obj["@id"] = objectID
	obj["@type"] = objType
	obj["attributedTo"] = owner
	return obj, nil
}

func (t *DBTx) GetPropTargets(objectID object.LocalID) (queue []goap.PropRetryTarget, err error) {
	t.debug(DEBUG, "GetPropTargets", objectID)
	data, err := t.tx.Query(
		"select URL, CLUSTER, TRYCOUNT, LASTTRY from PROPAGATION where ID = $1",
		string(objectID))
	if err != nil {
		return nil, err
	}
	defer data.Close()
	queue = make([]goap.PropRetryTarget, 0)
	for data.Next() {
		target := goap.PropRetryTarget{}
		err = data.Scan(&target.Url, &target.Cluster, &target.TryCount,
			&target.LastTry)
		if err != nil {
			return nil, err
		}
		queue = append(queue, target)
	}
	return queue, nil
}

func (t *DBTx) UpdatePropTarget(objectID object.LocalID, target goap.PropRetryTarget) (err error) {
	t.debug(DEBUG, "UpdatePropTarget", objectID, target)
	// Check that the target exists
	data, err := t.tx.Query(
		"select TRYCOUNT from PROPAGATION where ID = $1 and URL = $2",
		string(objectID), target.Url)
	if err != nil {
		return err
	}
	defer data.Close()
	if !data.Next() {
		return goap.BadPropagationTarget
	}
	// Update target
	_, err = t.tx.Exec("update PROPAGATION set TRYCOUNT = $1 and LASTTRY = $2 where ID = $3 and URL = $4",
		target.TryCount, target.LastTry, string(objectID), target.Url)
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) RemovePropTarget(objectID object.LocalID, targetURL string) (err error) {
	t.debug(DEBUG, "RemovePropTarget", objectID, targetURL)
	// delete where id, url
	_, err = t.tx.Exec("delete from PROPAGATION where ID = $1 and URL = $2;",
		string(objectID), targetURL)
	if err != nil {
		return err
	}
	// check if any remaining urls for this id
	queue, err := t.GetPropTargets(objectID)
	if err != nil {
		return err
	}
	if len(queue) == 0 {
		// if not then delete the prop object
		err = t.deleteObject(string(objectID), "prop")
		if err != nil {
			return err
		}
		err = t.deleteObjectData("prop" + string(objectID))
		if err != nil {
			return err
		}
	}
	return nil
}
