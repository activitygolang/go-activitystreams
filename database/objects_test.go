package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func TestStoreObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// existing object check
	itemRows := sqlmock.NewRows([]string{"ID"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo/bar", "obj").
		WillReturnRows(itemRows)
	// store OBJECT
	mock.ExpectExec("insert into OBJECT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "Note", "/foo", "obj", 0)
	// store field "@id"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar", "@id", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar$@id", 0, LOCALID, "/foo/bar", 0, 0.0, false)
	// store field "@type"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar", "@type", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar$@type", 0, STRING, "Note", 0, 0.0, false)
	// store field "attributedTo"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar", "attributedTo", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar$attributedTo", 0, LOCALID, "/foo", 0, 0.0, false)
	// store field "blah"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar", "blah", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo/bar$blah", 0, STRING, "jabber", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreObject(map[string]interface{}{
		"@id": object.LocalID("/foo/bar"), "@type": "Note",
		"attributedTo": object.LocalID("/foo"), "blah": "jabber"})
	if err != nil {
		t.Fatal("StoreObject error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreObject: unfulfilled expectations:", err)
	}
}

func TestLoadObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo/bar", "obj").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("@id", 1).
		AddRow("@type", 1).
		AddRow("attributedTo", 1).
		AddRow("subject", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("obj/foo/bar").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(LOCALID, "FailID", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("obj/foo/bar$@id").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "FailType", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("obj/foo/bar$@type").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(LOCALID, "FailOwner", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("obj/foo/bar$attributedTo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "LtUaE", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("obj/foo/bar$subject").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.LoadObject(object.LocalID("/foo/bar"))
	if err != nil {
		t.Fatal("LoadObject error:", err)
	}
	expected := map[string]interface{}{"@id": object.LocalID("/foo/bar"),
		"@type": "Note", "attributedTo": object.LocalID("/foo"),
		"subject": "LtUaE"}
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("LoadObject failure:", obj)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadObject: unfulfilled expectations:", err)
	}
}

func TestRemoveObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("delete from FIELDS").
		WithArgs("obj/foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows := sqlmock.NewRows([]string{"STRING"})
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("obj/foo", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("obj/foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.RemoveObject("/foo")
	if err != nil {
		t.Fatal("RemoveObject error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("RemoveObject: unfulfilled expectations:", err)
	}
}

func TestSetProtected(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"PROTECTED"}).
		AddRow(0)
	mock.ExpectQuery("select PROTECTED from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectExec("update OBJECT").
		WithArgs(true, "/foo", "obj").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.SetProtected("/foo", true)
	if err != nil {
		t.Fatal("SetProtected error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("SetProtected: unfulfilled expectations:", err)
	}
}

func TestIsProtected(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"PROTECTED"}).
		AddRow(1)
	mock.ExpectQuery("select PROTECTED from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	prot, err := tx.IsProtected("/foo")
	if err != nil {
		t.Fatal("IsProtected error:", err)
	}
	if prot != true {
		t.Fatal("IsProtected failure")
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("IsProtected: unfulfilled expectations:", err)
	}
}

func TestBasicObjectData(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	objType, owner, err := tx.BasicObjectData(object.LocalID("/foo"))
	if objType != "Note" || owner != object.LocalID("/foo") || err != nil {
		t.Fatal("BasicObjectData failure:", objType, owner, err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("BasicObjectData: unfulfilled expectations:", err)
	}
}

func TestExists(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"ID"}).
		AddRow("/foo")
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{"ID"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test existing object
	exists, err := tx.Exists("/foo")
	if !exists || err != nil {
		t.Fatal("Exists existing object error:", exists, err)
	}
	// Test non=existing object
	exists, err = tx.Exists("/foo")
	if exists || err != nil {
		t.Fatal("Exists non-existing object error:", exists, err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("BasicObjectData: unfulfilled expectations:", err)
	}
}
