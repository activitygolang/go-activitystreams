package database

import (
	"testing"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func TestValueToTypes(t *testing.T) {
	// Test null
	tC, vS, vI, vF, vB, err := ValueToTypes(nil)
	if tC != NULL || err != nil {
		t.Fatal("ValueToTypes null failure:", tC, vS, vI, vF, vB, err)
	}
	// Test string
	tC, vS, vI, vF, vB, err = ValueToTypes("foo")
	if tC != STRING || vS != "foo" || err != nil {
		t.Fatal("ValueToTypes string failure:", tC, vS, vI, vF, vB, err)
	}
	// Test LocalID
	tC, vS, vI, vF, vB, err = ValueToTypes(object.LocalID("foo"))
	if tC != LOCALID || vS != "foo" || err != nil {
		t.Fatal("ValueToTypes LocalID failure:", tC, vS, vI, vF, vB, err)
	}
	// Test integer
	tC, vS, vI, vF, vB, err = ValueToTypes(42)
	if tC != INTEGER || vI != 42 || err != nil {
		t.Fatal("ValueToTypes int failure:", tC, vS, vI, vF, vB, err)
	}
	// Test float
	tC, vS, vI, vF, vB, err = ValueToTypes(16.0)
	if tC != FLOAT || vF != 16.0 || err != nil {
		t.Fatal("ValueToTypes float failure:", tC, vS, vI, vF, vB, err)
	}
	// Test boolean
	tC, vS, vI, vF, vB, err = ValueToTypes(true)
	if tC != BOOL || vB != true || err != nil {
		t.Fatal("ValueToTypes bool failure:", tC, vS, vI, vF, vB, err)
	}
	// Test embedded
	tC, vS, vI, vF, vB, err = ValueToTypes(
		map[string]interface{}{"foo": "bar"})
	if tC != EMBED || err != nil {
		t.Fatal("ValueToTypes embed failure:", tC, vS, vI, vF, vB, err)
	}
	// Test list
	tC, vS, vI, vF, vB, err = ValueToTypes([]interface{}{"foo", "bar"})
	if tC != LIST || err != nil {
		t.Fatal("ValueToTypes list failure:", tC, vS, vI, vF, vB, err)
	}
	// Test invalid type
	tC, vS, vI, vF, vB, err = ValueToTypes(
		map[bool]bool{true: false, false: true})
	if tC != 0 || err == nil {
		t.Fatal("ValueToTypes invalid failure:", tC, vS, vI, vF, vB, err)
	}
}

func TestTypesToValue(t *testing.T) {
	var res interface{}
	// Test null
	res, err := TypesToValue(NULL, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue string failure", err)
	}
	if res != nil {
		t.Fatal("TypesToValue string failure:", res)
	}
	// Test string
	res, err = TypesToValue(STRING, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue string failure", err)
	}
	if res.(string) != "foo" {
		t.Fatal("TypesToValue string failure:", res)
	}
	// Test LocalID
	res, err = TypesToValue(LOCALID, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue string failure", err)
	}
	if res.(object.LocalID) != object.LocalID("foo") {
		t.Fatal("TypesToValue string failure:", res)
	}
	// Test integer
	res, err = TypesToValue(INTEGER, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue integer failure", err)
	}
	if res.(int64) != 42 {
		t.Fatal("TypesToValue integer failure:", res)
	}
	// Test float
	res, err = TypesToValue(FLOAT, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue float failure", err)
	}
	if res.(float64) != 16.0 {
		t.Fatal("TypesToValue float failure:", res)
	}
	// Test boolean
	res, err = TypesToValue(BOOL, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue boolean failure", err)
	}
	if res.(bool) != true {
		t.Fatal("TypesToValue boolean failure:", res)
	}
	// Test embed
	res, err = TypesToValue(EMBED, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue embed failure", err)
	}
	if res != "foo" {
		t.Fatal("TypesToValue embed failure:", res)
	}
	// Test embed
	res, err = TypesToValue(LIST, "foo", 42, 16.0, true)
	if err != nil {
		t.Fatal("TypesToValue list failure", err)
	}
	if res != "foo" {
		t.Fatal("TypesToValue list failure:", res)
	}
	// Test invalid type
	res, err = TypesToValue(-1, "foo", 42, 16.0, true)
	if err == nil {
		t.Fatal("TypesToValue integer failure", err)
	}
}
