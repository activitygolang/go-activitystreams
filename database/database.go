package database

import (
	"fmt"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

const (
	OFF = iota
	ERROR
	WARN
	INFO
	DEBUG
	DEEPDEBUG
)

type Database struct {
	db         *sql.DB
	DebugLevel int
}

func OpenDatabase(filename string) (db *Database, err error) {
	db = new(Database)
	db.db, err = sql.Open("sqlite3", filename)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func (d *Database) debug(threshold int, message ...interface{}) {
	// current debug level
	// threshold for this call
	// rest of data
	if d.DebugLevel >= threshold {
		fmt.Printf("db: ")
		fmt.Println(message...)
	}
}

func (d *Database) Close() (err error) {
	d.debug(DEBUG, "Close")
	err = d.db.Close()
	if err != nil {
		return err
	}
	d.db = nil
	return nil
}

func (d *Database) StartTransaction() (tx goap.Transaction, err error) {
	d.debug(DEBUG, "StartTransaction")
	tempTX, err := d.db.Begin()
	if err != nil {
		return nil, err
	}
	tx = new(DBTx)
	tx.(*DBTx).tx = tempTX
	tx.(*DBTx).DebugLevel = d.DebugLevel
	return tx, nil
}

// tables:
//   FIELD: field list/scalar switch, existence point
//   FIELDDATA: id, field, index, value-set
//   OBJECT: id, type, owner, universe
//   COLLECTION: id, field, value-set. Holds objcols
//   CHANGESETS: activityID, index, action, objectID, field, value-set
//   PROPAGATION: id, url, trycount, last-attempt

// value-set: string, int, float, bool, type

func (d *Database) Initialize() (err error) {
	d.debug(DEBUG, "Initialize")
	tx, err := d.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()
	_, err = d.db.Exec(`create table FIELDS (ID text, NAME text, SINGLE bit);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table FIELDDATA (IDNAME text, ITEMINDEX int, TYPE int, STRING text, INTEGER bigint, FLOAT decimal, BOOL bit);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table OBJECT (ID text, TYPE text, OWNER text, UNIVERSE text, PROTECTED bit);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table COLLECTION (ID text, FIELD text, TYPE int, STRING text, INTEGER bigint, FLOAT decimal, BOOL bit);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table CHANGESETS (ACTID text, CHGINDEX int, ACTION int, OBJID text, FIELD text, TYPE int, STRING text, INTEGER bigint, FLOAT decimal, BOOL bit);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table PROPAGATION (ID text, URL text, CLUSTER bool, TRYCOUNT int, LASTTRY text);`)
	if err != nil {
		return err
	}
	_, err = d.db.Exec(`create table ACCOUNT (USER text, SALT text, HASHTIME bigint, MEMORY bigint, KEYLEN bigint, HASHED text, ACTOR text, CREATION text);`)
	if err != nil {
		return err
	}
	return nil
}

type DBTx struct {
	tx         *sql.Tx
	DebugLevel int
}

func (t *DBTx) debug(threshold int, message ...interface{}) {
	// current debug level
	// threshold for this call
	// rest of data
	if t.DebugLevel >= threshold {
		fmt.Printf("db: ")
		fmt.Println(message...)
	}
}

func (t *DBTx) RevertTransaction() (err error) {
	t.debug(DEBUG, "RevertTransaction")
	err = t.tx.Rollback()
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) EndTransaction(errIn *error) (err error) {
	t.debug(DEBUG, "EndTransaction", errIn)
	if errIn == nil {
		t.debug(DEEPDEBUG, "...nil Commit")
		err = t.tx.Commit()
	} else {
		switch *errIn {
		case nil:
			t.debug(DEEPDEBUG, "...*nil Commit")
			err = t.tx.Commit()
		default:
			t.debug(DEEPDEBUG, "...Rollback:", *errIn)
			err = t.tx.Rollback()
		}
	}
	t.debug(DEEPDEBUG, "...err:", err)
	return err
}
