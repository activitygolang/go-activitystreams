package database

import (
	//"fmt"
	"database/sql"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

// Type codes
var STRING = 1
var LOCALID = 2
var INTEGER = 3
var FLOAT = 4
var BOOL = 5
var EMBED = 6
var LIST = 7
var NULL = 8

// Converts a value from an object into a type code and the set of variables
// needed by the datebase
func ValueToTypes(value interface{}) (typeCode int, vStr string, vInt int64, vFloat float64, vBool bool, err error) {
	vStr = ""
	vInt = 0
	vFloat = 0.0
	vBool = false
	if value == nil { // NULL type
		return NULL, "", 0, 0.0, false, nil
	}
	switch typedV := value.(type) {
	case string:
		typeCode = STRING
		vStr = typedV
	case object.LocalID:
		typeCode = LOCALID
		vStr = string(typedV)
	case int:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int8:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int16:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int32:
		typeCode = INTEGER
		vInt = int64(typedV)
	case int64:
		typeCode = INTEGER
		vInt = typedV
	case uint:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint8:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint16:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint32:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uint64:
		typeCode = INTEGER
		vInt = int64(typedV)
	case uintptr:
		typeCode = INTEGER
		vInt = int64(typedV)
	case float32:
		typeCode = FLOAT
		vFloat = float64(typedV)
	case float64:
		typeCode = FLOAT
		vFloat = typedV
	case bool:
		typeCode = BOOL
		vBool = typedV
	case map[string]interface{}:
		// Proper embed handling will have the caller extracting the data.
		// If the object has an ID we return that, otherwise the caller
		// will need to create an internal ID
		typeCode = EMBED
		// It is the caller's responsibility to generate an ID
	case []interface{}:
		typeCode = LIST
	default:
		return 0, "", 0, 0.0, false, object.BadType
	}
	return typeCode, vStr, vInt, vFloat, vBool, nil
}

func TypesToValue(typeCode int, vStr string, vInt int64, vFloat float64, vBool bool) (value interface{}, err error) {
	switch typeCode {
	case STRING:
		value = vStr
	case LOCALID:
		value = object.LocalID(vStr)
	case INTEGER:
		value = vInt
	case FLOAT:
		value = vFloat
	case BOOL:
		value = vBool
	case EMBED:
		value = vStr
	case LIST:
		value = vStr
	case NULL:
		value = nil
	default:
		return nil, object.BadType
	}
	return value, nil
}

func ListToInterfaceList(value interface{}) (result []interface{}, err error) {
	switch typedV := value.(type) {
	case []string:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int8:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int16:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []int64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint8:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint16:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uint64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []uintptr:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []float32:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []float64:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []bool:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []map[string]interface{}:
		result = make([]interface{}, len(typedV))
		for i, v := range typedV {
			result[i] = v
		}
	case []interface{}:
		result = typedV
	default:
		return nil, object.BadType
	}
	return result, nil
}

func loadValue(rows *sql.Rows) (value interface{}, vType int, err error) {
	var vString string
	var vInteger int64
	var vBoolean bool
	var vFloat float64
	err = rows.Scan(&vType, &vString, &vInteger, &vFloat, &vBoolean)
	if err != nil {
		return nil, 0, err
	}
	value, err = TypesToValue(vType, vString, vInteger, vFloat, vBoolean)
	if err != nil {
		return nil, 0, err
	}
	return value, vType, nil
}

func coreObjData(obj map[string]interface{}) (id string, objType string, owner string, err error) {
	temp, ok := obj["@id"]
	if !ok {
		return "", "", "", object.NoObjID
	}
	switch temp.(type) {
	case object.LocalID:
		id = string(temp.(object.LocalID))
	case string:
		id = temp.(string)
	default:
		return "", "", "", object.BadType
	}
	temp, ok = obj["@type"]
	if !ok {
		return "", "", "", object.NoObjType
	}
	switch temp.(type) {
	case string:
		objType = temp.(string)
	default:
		return "", "", "", object.BadType
	}
	temp, ok = obj["attributedTo"]
	if !ok {
		return "", "", "", object.NoObjOwner
	}
	switch temp.(type) {
	case object.LocalID:
		owner = string(temp.(object.LocalID))
	case string:
		owner = temp.(string)
	default:
		return "", "", "", object.BadType
	}
	return id, objType, owner, nil
}
