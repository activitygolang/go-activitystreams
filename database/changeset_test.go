package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	//"gitlab.com/activitygolang/go-activitystreams/object"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

func TestStoreChangeset(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	// ===== duplicate
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"ACTID"}).
		AddRow("/foo")
	mock.ExpectQuery("select ACTID from CHANGESETS").
		WithArgs("/foo", 0).
		WillReturnRows(itemRows)
	// ===== working
	itemRows = sqlmock.NewRows([]string{"ACTID"})
	mock.ExpectQuery("select ACTID from CHANGESETS").
		WithArgs("/foo", 0).
		WillReturnRows(itemRows)
	// first Change
	mock.ExpectExec("insert into CHANGESETS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", 0, goap.CSListAppend, "one", "A",
		INTEGER, "", 1, 0.0, false)
	// second Change
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cset/foo$1", "list", 0)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cset/foo$1$list", 0, INTEGER, "", 2, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cset/foo$1$list", 1, INTEGER, "", 3, 0.0, false)
	mock.ExpectExec("insert into CHANGESETS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", 1, goap.CSListAppend, "one", "B",
		LIST, "cset/foo$1", 0, 0.0, false)
	// third Change
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cset/foo$2", "two", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cset/foo$2$two", 0, INTEGER, "", 4, 0.0, false)
	mock.ExpectExec("insert into CHANGESETS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", 2, goap.CSListAppend, "one", "C",
		EMBED, "cset/foo$2", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test duplicate
	set := []goap.Change{
		goap.Change{goap.CSListAppend, "one", "A", 1},
		goap.Change{goap.CSListAppend, "one", "B", []interface{}{2, 3}},
		goap.Change{goap.CSListAppend, "one", "C",
			map[string]interface{}{"two": 4}}}
	err = tx.StoreChangeset("/foo", set)
	if err != goap.DuplicateItem {
		t.Fatal("StoreChangeset error:", err)
	}
	// Test working
	err = tx.StoreChangeset("/foo", set)
	if err != nil {
		t.Fatal("StoreChangeset error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreChangeset: unfulfilled expectations:", err)
	}
}

func TestLoadChangeset(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	// ===== no object
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"CHGINDEX", "ACTION", "OBJID", "FIELD", "TYPE", "STRING", "INTEGER", "FLOAT", "BOOL"})
	mock.ExpectQuery("select CHGINDEX, ACTION, OBJID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL from CHANGESETS").
		WithArgs("/foo").
		WillReturnRows(itemRows)
	// ===== working
	itemRows = sqlmock.NewRows([]string{"CHGINDEX", "ACTION", "OBJID", "FIELD", "TYPE", "STRING", "INTEGER", "FLOAT", "BOOL"}).
		AddRow(0, goap.CSListAppend, "one", "A", INTEGER, "", 1, 0.0, false).
		AddRow(1, goap.CSListAppend, "one", "B",
		LIST, "cset/foo$1", 0, 0.0, false).
			AddRow(2, goap.CSListAppend, "one", "C",
			EMBED, "cset/foo$2", 0, 0.0, false)
	mock.ExpectQuery("select CHGINDEX, ACTION, OBJID, FIELD, TYPE, STRING, INTEGER, FLOAT, BOOL from CHANGESETS").
		WithArgs("/foo").
		WillReturnRows(itemRows)
	// embedded list
	itemRows = sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("list", 0)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("cset/foo$1").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{
		"TYPE", "STRING", "INTEGER", "FLOAT", "BOOL"}).
			AddRow(INTEGER, "", 2, 0.0, false).
			AddRow(INTEGER, "", 3, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cset/foo$1$list").
		WillReturnRows(itemRows)
	// embedded object
	itemRows = sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("two", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("cset/foo$2").
		WillReturnRows(itemRows)
	itemRows = sqlmock.NewRows([]string{
		"TYPE", "STRING", "INTEGER", "FLOAT", "BOOL"}).
			AddRow(INTEGER, "", 4, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cset/foo$2$two").
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test no object
	set, err := tx.LoadChangeset("/foo")
	if err != goap.NoChangeset {
		t.Fatal("LoadChangeset error:", err)
	}
	// Test working
	set, err = tx.LoadChangeset("/foo")
	if err != nil {
		t.Fatal("LoadChangeset error:", err)
	}
	expected := []goap.Change{
		goap.Change{goap.CSListAppend, "one", "A", int64(1)},
		goap.Change{goap.CSListAppend, "one", "B",
			[]interface{}{int64(2), int64(3)}},
		goap.Change{goap.CSListAppend, "one", "C",
			map[string]interface{}{"two": int64(4)}}}
	if !reflect.DeepEqual(set, expected) {
		t.Fatal("LoadChangeset failure:", set)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadChangeset: unfulfilled expectations:", err)
	}
}
