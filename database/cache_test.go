package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	//"gitlab.com/activitygolang/go-activitystreams/object"
	goap "gitlab.com/activitygolang/go-activitystreams"
)

func TestStoreCache(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// existing object check
	itemRows := sqlmock.NewRows([]string{"ID"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo/bar", "cache").
		WillReturnRows(itemRows)
	// store OBJECT
	mock.ExpectExec("insert into OBJECT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "Note", "/foo", "cache", 0)
	// store field "@id"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar", "@id", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar$@id", 0, STRING, "/foo/bar", 0, 0.0, false)
	// store field "@type"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar", "@type", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar$@type", 0, STRING, "Note", 0, 0.0, false)
	// store field "attributedTo"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar", "attributedTo", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar$attributedTo", 0, STRING, "/foo", 0, 0.0,
		false)
	// store field "blah"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar", "blah", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("cache/foo/bar$blah", 0, STRING, "jabber", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	var tx goap.Transaction
	tx, err = db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.StoreCache(map[string]interface{}{
		"@id": "/foo/bar", "@type": "Note", "attributedTo": "/foo",
		"blah": "jabber"})
	if err != nil {
		t.Fatal("StoreCache error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("StoreCache: unfulfilled expectations:", err)
	}
}

func TestLoadCache(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo/bar", "cache").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("@id", 1).
		AddRow("@type", 1).
		AddRow("attributedTo", 1).
		AddRow("subject", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("cache/foo/bar").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "FailID", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cache/foo/bar$@id").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "FailType", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cache/foo/bar$@type").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "FailOwner", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cache/foo/bar$attributedTo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "LtUaE", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("cache/foo/bar$subject").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.LoadCache("/foo/bar")
	if err != nil {
		t.Fatal("LoadCache error:", err)
	}
	expected := map[string]interface{}{"@id": "/foo/bar", "@type": "Note",
		"attributedTo": "/foo", "subject": "LtUaE"}
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("LoadCache failure:", obj)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("LoadCache: unfulfilled expectations:", err)
	}
}
