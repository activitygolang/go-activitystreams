package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gitlab.com/activitygolang/go-activitystreams/object"
	//goap "gitlab.com/activitygolang/go-activitystreams"
)

func TestObjcolAdd(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectExec("insert into COLLECTION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "bar", STRING, "quux", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.ObjcolAdd(object.LocalID("/foo"), "bar", "quux")
	if err != nil {
		t.Fatal("ObjcolAdd error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolAdd: unfulfilled expectations:", err)
	}
}

func TestObjcolRM(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from COLLECTION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "bar", STRING, "quux", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.ObjcolRM(object.LocalID("/foo"), "bar", "quux")
	if err != nil {
		t.Fatal("ObjcolRM error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolRM: unfulfilled expectations:", err)
	}
}

func TestObjcolGetAllField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(INTEGER, "", 1, 0.0, false).
		AddRow(INTEGER, "", 2, 0.0, false).
		AddRow(INTEGER, "", 3, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from COLLECTION").
		WithArgs("/foo", "bar").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	values, err := tx.ObjcolGetAllField(object.LocalID("/foo"), "bar")
	if err != nil {
		t.Fatal("ObjcolGetAll error:", err)
	}
	expected := []interface{}{int64(1), int64(2), int64(3)}
	if !reflect.DeepEqual(values, expected) {
		t.Fatal("ObjcolGetAll failure:", values)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolGetAll: unfulfilled expectations:", err)
	}
}

func TestObjcolGetFields(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"FIELD"}).
		AddRow("bar").
		AddRow("quux")
	mock.ExpectQuery("select distinct FIELD from COLLECTION").
		WithArgs("/foo").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	
	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	fields, err := tx.ObjcolGetFields("/foo")
	if err != nil {
		t.Fatal("ObjcolGetFields error:", err)
	}
	expected := []string{"bar", "quux"}
	if !reflect.DeepEqual(fields, expected) {
		t.Fatal("ObjcolGetFields failure:", fields)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolGetFields: unfulfilled expectations:", err)
	}
}

func TestObjcolRMAll(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from COLLECTION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo")
	mock.ExpectCommit()
	
	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.ObjcolRMAll("/foo")
	if err != nil {
		t.Fatal("ObjcolRMAll error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolRMAll: unfulfilled expectations:", err)
	}
}

func TestObjcolGetAll(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	objRows := sqlmock.NewRows([]string{"FIELD"}).
		AddRow("stuff").
		AddRow("other")
	mock.ExpectQuery("select distinct FIELD from COLLECTION").
		WithArgs("/foo").
		WillReturnRows(objRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "a", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from COLLECTION").
		WithArgs("/foo", "stuff").
		WillReturnRows(objRows)
	itemRows = sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "b", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from COLLECTION").
		WithArgs("/foo", "other").
		WillReturnRows(objRows)
	mock.ExpectCommit()
	
	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	data, err := tx.ObjcolGetAll("/foo")
	if err != nil {
		t.Fatal("ObjcolGetAll error:", err)
	}
	expected := map[string][]interface{}{
		"stuff": []interface{}{"a"},
		"other": []interface{}{"b"},
	}
	if !reflect.DeepEqual(data, expected) {
		t.Fatal("ObjcolGetAll failure:", data)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolGetAll: unfulfilled expectations:", err)
	}
}

func TestObjcolSet(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"TYPE", "OWNER"}).
		AddRow("Note", "/foo")
	mock.ExpectQuery("select TYPE, OWNER from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from COLLECTION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo")
	mock.ExpectExec("insert into COLLECTION").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "stuff", STRING, "a", 0, 0.0, false)
	mock.ExpectCommit()
	
	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.ObjcolSet("/foo", map[string][]interface{}{
		"stuff": []interface{}{"a"}})
	if err != nil {
		t.Fatal("ObjcolSet error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("ObjcolSet: unfulfilled expectations:", err)
	}
}
