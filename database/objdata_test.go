package database

import (
	"reflect"
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func Test_storeObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// existing object check
	itemRows := sqlmock.NewRows([]string{"ID"})
	mock.ExpectQuery("select ID from OBJECT").
		WithArgs("/foo", "obj").
		WillReturnRows(itemRows)
	// store object
	mock.ExpectExec("insert into OBJECT").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "Note", "/baz", "obj", 0)
	// insert field @id
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "@id", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$@id", 0, STRING, "/foo", 0, 0.0, false)
	// insert field @type
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "@type", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$@type", 0, STRING, "Note", 0, 0.0, false)
	// insert field attributedTo
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo", "attributedTo", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("obj/foo$attributedTo", 0, STRING, "/baz", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.(*DBTx).storeObject(
		map[string]interface{}{"@id":"/foo", "@type":"Note",
			"attributedTo":"/baz"},
		"obj")
	if err != nil {
		t.Fatal("storeObject error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("EndTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("storeObject: unfulfilled expectations:", err)
	}
}

func Test_storeObjectData(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	// store field "a"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "a", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo$a", 0, INTEGER, "", 1, 0.0, false)
	// store field "b"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo", "b", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo$b", 0, STRING, "quux", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.(*DBTx).storeObjectData("/foo",
		map[string]interface{}{"a": 1, "b": "quux"})
	if err != nil {
		t.Fatal("storeObjectData error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("storeObjectData: unfulfilled expectations:", err)
	}
}

func Test_storeField(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "object", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, INTEGER, "", 4, 0.0, false)
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar", "things", 0)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$things", 0, STRING, "foo", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$things", 1, INTEGER, "", 3, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$things", 2, NULL, "", 0, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test single item
	err = tx.(*DBTx).storeField("/foo/bar", "object", 4)
	if err != nil {
		t.Fatal("storeField error:", err)
	}
	// Test list
	err = tx.(*DBTx).storeField("/foo/bar", "things",
		[]interface{}{"foo", 3, nil})
	if err != nil {
		t.Fatal("storeField error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("storeField: unfulfilled expectations:", err)
	}
}

func Test_storeFieldItem(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, NULL, "", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, STRING, "foo", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, LOCALID, "foo", 0, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, INTEGER, "", 5, 0.0, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, FLOAT, "", 0, 4.2, false)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, BOOL, "", 0, 0.0, true)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("/foo/bar$object", 0, EMBED, "embed/foo/bar$object$0",
		0, 0.0, false)
	// store field "a"
	mock.ExpectExec("insert into FIELDS").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("embed/foo/bar$object$0", "a", 1)
	mock.ExpectExec("insert into FIELDDATA").
		WillReturnResult(sqlmock.NewResult(0, 0)).
		WithArgs("embed/foo/bar$object$0$a", 0, INTEGER, "", 1, 0.0, false)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	// Test null
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, nil)
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test string
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, "foo")
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test LocalID
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, object.LocalID("foo"))
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test int
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, 5)
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test float
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, 4.2)
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test bool
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0, true)
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	// Test embed
	err = tx.(*DBTx).storeFieldItem("/foo/bar$object", 0,
		map[string]interface{}{"a": 1})
	if err != nil {
		t.Fatal("storeFieldItem error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("storeFieldItem: unfulfilled expectations:", err)
	}
}

func Test_loadObjectData(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	objRows := sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("a", 1).
		AddRow("b", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("/foo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(INTEGER, "", 1, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("/foo$a").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(INTEGER, "", 2, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("/foo$b").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	obj, err := tx.(*DBTx).loadObjectData("/foo")
	if err != nil {
		t.Fatal("loadObjectData error:", err)
	}
	expected := map[string]interface{}{"a": int64(1), "b": int64(2)}
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("loadObjectData failure:", obj)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("loadObjectData: unfulfilled expectations:", err)
	}
}

func Test_loadFieldItems(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	objRows := sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(INTEGER, "", 23, 0.0, false).
		AddRow(EMBED, "objembed/foo", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("foo").
		WillReturnRows(objRows)
	// embedded object load
	objRows = sqlmock.NewRows([]string{"NAME", "SINGLE"}).
		AddRow("afield", 1)
	mock.ExpectQuery("select NAME, SINGLE from FIELDS").
		WithArgs("objembed/foo").
		WillReturnRows(objRows)
	objRows = sqlmock.NewRows([]string{"TYPE", "STRING", "INTEGER", "FLOAT",
		"BOOL"}).
		AddRow(STRING, "avalue", 0, 0.0, false)
	mock.ExpectQuery("select TYPE, STRING, INTEGER, FLOAT, BOOL from FIELDDATA").
		WithArgs("objembed/foo$afield").
		WillReturnRows(objRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	value, err := tx.(*DBTx).loadFieldItems("foo")
	if err != nil {
		t.Fatal("loadFieldItems error:", err)
	}
	expected := []interface{}{int64(23),
		map[string]interface{}{"afield": "avalue"}}
	if !reflect.DeepEqual(value, expected) {
		t.Fatal("loadFieldItems failure:", value)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("loadFieldItems: unfulfilled expectations:", err)
	}
}

func Test_deleteObjectData(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from FIELDS").
		WithArgs("/foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows := sqlmock.NewRows([]string{"STRING"}).
		AddRow("fizzbin")
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("/foo", EMBED).
		WillReturnRows(itemRows)
	// === start deleting subobject === //
	mock.ExpectExec("delete from FIELDS").
		WithArgs("fizzbin").
		WillReturnResult(sqlmock.NewResult(0, 0))
	itemRows = sqlmock.NewRows([]string{"STRING"})
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("fizzbin", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("fizzbin").
		WillReturnResult(sqlmock.NewResult(0, 0))
	// === end deleting subobject === //
	mock.ExpectExec("delete from FIELDDATA").
		WithArgs("/foo").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.(*DBTx).deleteObjectData("/foo")
	if err != nil {
		t.Fatal("deleteObjectData error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("deleteObjectData: unfulfilled expectations:", err)
	}
}

func Test_shiftEntries(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"ITEMINDEX"}).
		AddRow(4).
		AddRow(5).
		AddRow(6)
	mock.ExpectQuery("select ITEMINDEX from FIELDDATA").
		WithArgs("foobar", 4).
		WillReturnRows(itemRows)
	mock.ExpectExec("update FIELDDATA").
		WithArgs(3, "foobar", 4).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update FIELDDATA").
		WithArgs(4, "foobar", 5).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec("update FIELDDATA").
		WithArgs(5, "foobar", 6).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.(*DBTx).shiftEntries("foobar", 4, -1)
	if err != nil {
		t.Fatal("shiftEntries error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("shiftEntries: unfulfilled expectations:", err)
	}
}

func Test_deleteObject(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	mock.ExpectExec("delete from OBJECT").
		WithArgs("/foo", "bar").
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	err = tx.(*DBTx).deleteObject("/foo", "bar")
	if err != nil {
		t.Fatal("deleteObject error:", err)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("deleteObject: unfulfilled expectations:", err)
	}
}

func Test_selectEmbed(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Set up expectations
	mock.ExpectBegin()
	itemRows := sqlmock.NewRows([]string{"STRING"}).
		AddRow("fizzbin")
	mock.ExpectQuery("select STRING from FIELDDATA").
		WithArgs("/foo", EMBED).
		WillReturnRows(itemRows)
	mock.ExpectCommit()

	// Run tests
	tx, err := db.StartTransaction()
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}
	embedIDs, err := tx.(*DBTx).selectEmbed("/foo")
	if err != nil {
		t.Fatal("selectEmbed error:", err)
	}
	if !reflect.DeepEqual(embedIDs, []string{"fizzbin"}) {
		t.Fatal("selectEmbed failure:", embedIDs)
	}
	err = tx.EndTransaction(nil)
	if err != nil {
		t.Fatal("StartTransaction error:", err)
	}

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("selectEmbed: unfulfilled expectations:", err)
	}
}
