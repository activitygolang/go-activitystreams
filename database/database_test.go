package database

import (
	"testing"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestDatabaseCOC(t *testing.T) {
	var err error
	var db *Database

	db = new(Database)
	mockedDB, mock, err := sqlmock.New()
	db.db = mockedDB

	// Test the creation of each table
	mock.ExpectBegin()
	mock.ExpectExec(`create table FIELDS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table FIELDDATA`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table OBJECT`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table COLLECTION`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table CHANGESETS`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table PROPAGATION`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectExec(`create table ACCOUNT`).
		WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectCommit()

	db.Initialize()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Initialize: unfulfilled expectations:", err)
	}

	// Test closing
	mock.ExpectClose()

	db.Close()

	if err = mock.ExpectationsWereMet(); err != nil {
		t.Fatal("Database Close: unfulfilled expectations:", err)
	}
}
