package database

import (
	//"fmt"
	//"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/activitygolang/go-activitystreams/object"
)

func (t *DBTx) StoreObject(obj map[string]interface{}) (err error) {
	// no debug here, handled by storeObject
	return t.storeObject(obj, "obj")
}

func (t *DBTx) LoadObject(objectID object.LocalID) (obj map[string]interface{}, err error) {
	t.debug(DEBUG, "LoadObject", objectID)
	objType, owner, err := t.BasicObjectData(objectID)
	if err != nil {
		return nil, err
	}
	// Now have ID, Type, Owner, Universe. Can load the rest of the object.
	obj, err = t.loadObjectData("obj" + string(objectID))
	if err != nil {
		return nil, err
	}
	obj["@id"] = objectID
	obj["@type"] = objType
	obj["attributedTo"] = owner
	return obj, nil
}

func (t *DBTx) RemoveObject(objectID object.LocalID) (err error) {
	t.debug(DEBUG, "RemoveObject", objectID)
	// TODO: check protected
	t.deleteObject(string(objectID), "obj")
	if err != nil {
		return err
	}
	err = t.deleteObjectData("obj" + string(objectID))
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) SetProtected(objectID object.LocalID, protected bool) (err error) {
	t.debug(DEBUG, "SetProtected", objectID, protected)
	// check that the object exists
	// the actual field doesn't matter...
	data, err := t.tx.Query(
		"select PROTECTED from OBJECT where ID = $1 and UNIVERSE = $2",
		string(objectID), "obj")
	if err != nil {
		return err
	}
	defer data.Close()
	if !data.Next() {
		// No entry for this object.
		return object.NoObject
	}
	data.Close()
	// update
	_, err = t.tx.Exec(
		"update OBJECT set PROTECTED = $1 where ID = $2 and UNIVERSE = $3",
		protected, objectID, "obj")
	if err != nil {
		return err
	}
	return nil
}

func (t *DBTx) IsProtected(objectID object.LocalID) (protected bool, err error) {
	t.debug(DEBUG, "IsProtected", objectID)
	data, err := t.tx.Query(
		"select PROTECTED from OBJECT where ID = $1 and UNIVERSE = $2",
		string(objectID), "obj")
	if err != nil {
		return false, err
	}
	defer data.Close()
	if !data.Next() {
		// No entry for this object.
		return false, object.NoObject
	}
	err = data.Scan(&protected)
	if err != nil {
		return false, err
	}
	return protected, nil
}

func (t *DBTx) BasicObjectData(objectID object.LocalID) (objType string, owner object.LocalID, err error) {
	// no debug here, handled by loadBasicObjectData
	objType, ownerTemp, err := t.loadBasicObjectData(string(objectID), "obj")
	return objType, object.LocalID(ownerTemp), err
}

func (t *DBTx) Exists (objectID object.LocalID) (exists bool, err error) {
	// no debug here, handled by existsCore
	return t.existsCore(objectID, "obj")
}

func (t *DBTx) existsCore(objectID object.LocalID, universe string) (exists bool, err error) {
	// load matching fields from OBJECT
	data, err := t.tx.Query( // Don't care about values, just existence check
		"select ID from OBJECT where ID = $1 and UNIVERSE = $2",
		string(objectID), universe)
	if err != nil {
		return false, err
	}
	defer data.Close()
	if data.Next() {
		// If any match: return true
		return true, nil
	} else {
		// No matches. No object
		return false, nil
	}
}
