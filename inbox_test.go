package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	"testing"
)

func TestInboxAccept(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	// Test Follow Accept ========
	// Setup the environment
	core.AddActor("jrandom", "Person")
	core.AddActor("foo", "Person")
	followID := object.LocalID("/jrandom/foooofollow")
	follow := map[string]interface{}{"@id": followID, "@type": "Follow",
		"object":       "https://foo.test/foo",
		"attributedTo": object.LocalID("/jrandom")}
	storage.objects[followID] = follow
	// Build Accept
	accept := map[string]interface{}{"@type": "Accept",
		"@id":          object.LocalID("/foo/acc"),
		"object":       string("https://foo.test" + followID),
		"attributedTo": object.LocalID("/foo")}
	// Run Follow test
	err := core.PostInboxLocal("/jrandom/inbox", accept)
	if err != nil {
		t.Fatal("InboxAccept error:", err)
	}
	// check following
	following := storage.objects["/jrandom/following"]
	items := following["items"]
	if !reflect.DeepEqual(items, []interface{}{"https://foo.test/foo"}) {
		t.Fatal("InboxAccept failure:", items, storage.objects)
	}
}

func TestInboxLike(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	core.Hostname = "foo.test"
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/jrandom/target")
	target := map[string]interface{}{"@id": targetID, "@type": "Note",
		"attributedTo": object.LocalID("/jrandom")}
	storage.objects[targetID] = target
	// Build Like
	likeID := object.LocalID("/foo/likelike")
	like := map[string]interface{}{"@id": likeID, "@type": "Like",
		"object":       string("https://foo.test" + targetID),
		"attributedTo": object.LocalID("/foo")}
	// Run test
	err := core.PostInboxLocal("/jrandom/inbox", like)
	if err != nil {
		t.Fatal("InboxLike error:", err)
	}
	// Check likes
	expected := map[object.LocalID]map[string][]interface{}{
		targetID: map[string][]interface{}{
			"likes": []interface{}{object.LocalID(likeID)}}}
	if !reflect.DeepEqual(storage.collections, expected) {
		t.Fatal("InboxLike failure:", storage.collections)
	}
}
