package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	//"fmt"
)

func (c *Core) preliminaryOutboxTweaks(actorID object.LocalID, activity map[string]interface{}) (activityID object.LocalID) {
	nowTime := MakeNowTimestamp()
	// set attributedTo fields to owning Actor
	activity["attributedTo"] = actorID
	// set published fields of objects to current timestamp
	activity["published"] = nowTime
	// set the activity's ID
	activityID = c.GenerateObjectID(actorID)
	activity["@id"] = activityID
	return activityID
}

func (c *Core) storeAndAppendActivity(tx Transaction, outboxID object.LocalID, activity map[string]interface{}, changes []Change) (err error) {
	// Assumes that the activity ID was just assigned properly
	activityID := activity["@id"].(object.LocalID)
	changes = append(changes,
		Change{Action: CSAddObject, ObjectID: activityID, Value: activity})
	err = tx.StoreObject(activity)
	if err != nil {
		return err
	}
	changes = append(changes, Change{Action: CSListAppend, ObjectID: outboxID,
		FieldName: "items", Value: activityID})
	err = tx.ListAppend(outboxID, "items", activityID)
	if err != nil {
		return err
	}
	// store changeset
	err = tx.StoreChangeset(string(activityID), changes)
	if err != nil {
		return err
	}
	return nil
}

func (c *Core) OutboxAccept(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	// Also handles TentativeAccept
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Get target object so we can know what to do
	// target object is what we are Accepting
	temp, ok := activity["object"]
	if !ok {
		return object.LocalID(""), nil, nil, object.BrokenObject
	}
	var targetObj map[string]interface{}
	switch temp.(type) {
	case string:
		targetID := object.ExtractNativeFromGlobal(temp.(string), c.Hostname,
			c.Port)
		if targetID == object.LocalID("") {
			// Remote object
			// targetObj = loadCache
		} else {
			targetObj, err = tx.LoadObject(targetID)
		}
	case object.LocalID:
		targetObj, err = tx.LoadObject(temp.(object.LocalID))
	default:
		// No valid object field. This Accept is broken.
		return object.LocalID(""), nil, nil, object.BrokenObject
	}
	// Now that we have the object we can figure out how to Accept it.
	if err == nil {
		targetType, _ := object.GetStr(targetObj, "@type")
		switch targetType {
		case "Follow":
			// Add target actor to this Actor's followers list
			targetActor, err := object.GetStr(targetObj, "attributedTo")
			if err != nil {
				// This object is borken, can't get its actor
				return "", nil, nil, object.BrokenObject
			} else {
				temp, err := tx.LoadObjectField(actorID, "followers")
				if err != nil {
					return "", nil, nil, err
				}
				var followersID object.LocalID
				switch temp.(type) {
				case object.LocalID:
					followersID = temp.(object.LocalID)
				default:
					return "", nil, nil, BadActorConstruction
				}
				chg := Change{Action: CSListRemove, ObjectID: followersID,
					FieldName: "items", Value: targetActor}
				changes = []Change{chg}
				err = tx.ListAppend(followersID, "items", targetActor)
				if err != nil {
					return "", nil, nil, err
				}
			}
		default:
			// ??? Invite, Offer ???
		}
	} else if err != object.NoObject {
		// Real error, fail
		return "", nil, nil, err
	}
	// If err == NoObject then we have nothing to target, store and move on.
	return activityID, nil, changes, nil
}

func (c *Core) OutboxAdd(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Remove
	// Get 'target', check server ownership, check type
	temp, err := object.GetStr(activity, "target")
	if err != nil {
		return "", nil, nil, err
	}
	targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if targetID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	targetObj, err := tx.LoadObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	targetType, err := object.GetStr(targetObj, "@type")
	if err != nil {
		return "", nil, nil, err
	}
	if targetType != "Collection" && targetType != "OrderedCollection" {
		return "", nil, nil, InvalidTargetType
	}
	// Check that actor has write permissions on target
	ownerID, err := object.GetLocalID(targetObj, "attributedTo")
	if err != nil {
		return "", nil, nil, err
	}
	if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
		// TODO: better ownership system
		return "", nil, nil, UnownedTarget
	}
	// Get 'object'
	assignStr, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	// Check that the collection doesn't already have this
	items, _, err := object.GetRefObjList(targetObj, "items")
	for _, item := range items {
		if reflect.DeepEqual(item, assignStr) {
			// Found a duplicate, no need to add.
			return "", nil, nil, DuplicateItem
		}
	}
	// Append string to collection
	chg := Change{Action: CSListRemove, ObjectID: targetID, FieldName: "items",
		Value: assignStr}
	changes = []Change{chg}
	err = tx.ListAppend(targetID, "items", assignStr)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxBlock(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Block
	// Get the Block target
	blockTarget, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	// Get actor block list
	temp, err := tx.LoadObjectField(actorID, "blocked")
	if err != nil {
		return "", nil, nil, err
	}
	var blockListID object.LocalID
	switch temp.(type) {
	case object.LocalID:
		blockListID = temp.(object.LocalID)
	default:
		return "", nil, nil, BadActorConstruction
	}
	// Add to block list
	chg := Change{Action: CSListRemove, ObjectID: blockListID, FieldName: "items",
		Value: blockTarget}
	changes = []Change{chg}
	err = tx.ListAppend(blockListID, "items", blockTarget)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxCreate(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	object, err := object.GetObj(activity, "object")
	if err != nil {
		// In theory the object field can be something other than an object.
		// This possibility is currently being ignored for simplicity
		// When that feature is implemented the error/type check will change
		return "", nil, nil, err
	}
	// Syncronize fields
	SyncFields(activity, object, "to")
	SyncFields(activity, object, "bto")
	SyncFields(activity, object, "cc")
	SyncFields(activity, object, "bcc")
	SyncFields(activity, object, "audience")
	object["attributedTo"] = activity["attributedTo"]
	object["published"] = activity["published"]
	// TODO: determine inbox targets (for propagation)
	// set object's ID, and the activity's object field to the ID
	objID := c.GenerateObjectID(actorID)
	activity["object"] = objID
	object["@id"] = objID
	// store activity and object
	chg := Change{Action: CSRemoveObject, ObjectID: objID}
	changes = []Change{chg}
	err = tx.StoreObject(object)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxDelete(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	temp, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if targetID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	// Check that target ID is owned by the same actor
	ownerID, err := c.NativeObjectOwner(tx, targetID)
	if err != nil {
		return "", nil, nil, err
	}
	if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
		// Error: actor does not own object
		return "", nil, nil, InvalidDeleteTarget
	}
	// Check that the target exists and is not protected
	targetObject, err := tx.LoadObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	prot, err := tx.IsProtected(targetID)
	if prot == true || err != nil {
		return "", nil, nil, InvalidDeleteTarget
	}
	// Carve a Tombstone
	tombstone := CarveTombstone(targetObject)
	// Delete the target object
	chg := Change{Action: CSUpdateObject, ObjectID: targetID,
		Value: targetObject}
	changes = []Change{chg}
	err = tx.RemoveObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	// Store the Tombstone
	err = tx.StoreObject(tombstone)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxFollow(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// TODO: Add 'object' to propagation list. Maybe?
	targetID, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, []interface{}{targetID}, nil, nil
}

func (c *Core) OutboxLike(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	// Like is expected to have an 'actor' field. We don't touch it.
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	targetID, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	if targetID == "" {
		return "", nil, nil, ActivityNoTarget
	}
	// Add target to liked collection
	temp, err := tx.LoadObjectField(actorID, "liked")
	if err != nil {
		return "", nil, nil, err
	}
	var likedID object.LocalID
	switch temp.(type) {
	case object.LocalID:
		likedID = temp.(object.LocalID)
	default:
		return "", nil, nil, BadActorConstruction
	}
	chg := Change{Action: CSListRemove, ObjectID: likedID, FieldName: "items",
		Value: targetID}
	changes = []Change{chg}
	err = tx.ListAppend(likedID, "items", targetID)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxMove(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Move
	// Get moved object ID
	moveObject, ok := activity["object"]
	if !ok {
		return "", nil, nil, object.NoField
	}
	// Check 'origin' for valid move source
	temp, err := object.GetStr(activity, "origin")
	if err != nil {
		return "", nil, nil, err
	}
	originID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if originID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	originObj, err := tx.LoadObject(originID)
	if err != nil {
		return "", nil, nil, err
	}
	originType, _ := object.GetStr(originObj, "@type")
	if !IsCollection(originType) {
		return "", nil, nil, InvalidTargetType
	}
	// Check that actor has write permissions on origin
	ownerID, _ := object.GetLocalID(originObj, "attributedTo")
	if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
		// TODO: better ownership system
		return "", nil, nil, UnownedTarget
	}
	// Check that object is in origin
	originItems, _, err := object.GetRefObjList(originObj, "items")
	if err != nil {
		return "", nil, nil, err
	}
	found := false
	for _, item := range originItems {
		if item == moveObject {
			found = true
			break
		}
	}
	if !found { // Wasn't in source... can't move what doesn't exist
		return "", nil, nil, ObjectNotInOrigin
	}
	// Check 'target' for valid move destination
	temp, err = object.GetStr(activity, "target")
	if err != nil {
		return "", nil, nil, err
	}
	targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if targetID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	targetObj, err := tx.LoadObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	targetType, _ := object.GetStr(targetObj, "@type")
	if !IsCollection(targetType) {
		return "", nil, nil, InvalidTargetType
	}
	// Check that actor has write permissions on target
	ownerID, _ = object.GetLocalID(targetObj, "attributedTo")
	if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
		// TODO: better ownership system
		return "", nil, nil, UnownedTarget
	}
	// Remove from origin
	chgRm := Change{Action: CSListAppend, ObjectID: originID, Value: moveObject}
	if err = tx.ListRemove(originID, "items", moveObject); err != nil {
		return "", nil, nil, err
	}
	// Add to target
	chgAdd := Change{Action: CSListRemove, ObjectID: targetID,
		Value: moveObject}
	if err = tx.ListAppend(targetID, "items", moveObject); err != nil {
		return "", nil, nil, err
	}
	changes = []Change{chgRm, chgAdd}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxQuestion(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Question
	// check for oneOf xor anyOf
	_, one := activity["oneOf"]
	_, any := activity["anyOf"]
	if one == any {
		return "", nil, nil, BadQuestion
	}
	return activityID, nil, nil, nil
}

func (c *Core) OutboxRemove(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Remove
	// Get 'target', check server ownership, check type
	temp, err := object.GetStr(activity, "target")
	if err != nil {
		return "", nil, nil, err
	}
	targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if targetID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	targetObj, err := tx.LoadObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	targetType, err := object.GetStr(targetObj, "@type")
	if err != nil {
		return "", nil, nil, err
	}
	if targetType != "Collection" && targetType != "OrderedCollection" {
		return "", nil, nil, InvalidTargetType
	}
	// Check that actor has write permissions on target
	ownerID, err := object.GetLocalID(targetObj, "attributedTo")
	if err != nil {
		return "", nil, nil, err
	}
	if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
		// TODO: better ownership system
		return "", nil, nil, UnownedTarget
	}
	// Get 'object'
	removalObject, ok := activity["object"]
	if !ok {
		return "", nil, nil, object.NoField
	}
	// Copy everything that is not an instance of the object to remove
	newItems := make([]interface{}, 0)
	items, _, err := object.GetRefObjList(targetObj, "items")
	for _, item := range items {
		if !reflect.DeepEqual(item, removalObject) {
			newItems = append(newItems, item)
		}
	}
	targetObj["items"] = newItems
	// Update object with new list
	chg := Change{Action: CSListAppend, ObjectID: targetID,
		Value: removalObject}
	changes = []Change{chg}
	err = tx.ListRemove(targetID, "items", removalObject)
	if err != nil {
		return "", nil, nil, err
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxUndo(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Undo
	// Get target
	temp, err := object.GetStr(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
	if targetID == object.LocalID("") {
		return "", nil, nil, object.NoObject
	}
	targetObj, err := tx.LoadObject(targetID)
	if err != nil {
		return "", nil, nil, err
	}
	// Switch on target type
	targetType, _ := object.GetStr(targetObj, "@type")
	// Check that type is activity
	if !IsActivity(targetType) {
		return "", nil, nil, InvalidTargetType
	}
	// Load activity changeset
	changeset, err := tx.LoadChangeset(string(targetID))
	if err != nil {
		return "", nil, nil, err
	}
	changes = make([]Change, len(changeset))
	for index, undoChange := range changeset {
		inverse, err := c.buildInverseChange(tx, undoChange)
		if err != nil {
			return "", nil, nil, err
		}
		changes[index] = inverse
		err = c.runChange(tx, undoChange)
		if err != nil {
			return "", nil, nil, err
		}
	}
	return activityID, nil, changes, nil
}

func (c *Core) OutboxUpdate(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	// Update
	// extract embedded object
	_, obj, err := object.GetRefObj(activity, "object")
	if err != nil {
		return "", nil, nil, err
	}
	if obj != nil {
		temp, err := object.GetStr(obj, "@id")
		if err != nil {
			return "", nil, nil, err
		}
		targetID := object.ExtractNativeFromGlobal(temp, c.Hostname, c.Port)
		if targetID == object.LocalID("") {
			return "", nil, nil, object.NoObject
		}
		// Get target data
		targetObj, err := tx.LoadObject(targetID)
		if err != nil {
			return "", nil, nil, err
		}
		prot, err := tx.IsProtected(targetID)
		if err != nil {
			return "", nil, nil, err
		}
		targetType, _ := object.GetStr(targetObj, "@type")
		ownerID, err := object.GetLocalID(targetObj, "attributedTo")
		if err != nil {
			return "", nil, nil, err
		}
		// Figure out if we can edit this object
		var updatedObj map[string]interface{}
		if IsActor(targetType) {
			if targetID != actorID {
				// We can only change *our* actor
				return "", nil, nil, UnownedTarget
			}
			// Updating the actor. This needs a special handler so that
			// special fields don't get stepped on.
			updatedObj = UpdateActor(targetObj, obj)
		} else {
			if !object.CompareIDsNative(ownerID, actorID, c.Hostname, c.Port) {
				return "", nil, nil, UnownedTarget
			} else if prot {
				return "", nil, nil, ProtectedObject
			}
			// update object
			updatedObj = UpdateNormalObject(targetObj, obj)
		}
		updatedObj["updated"] = activity["published"]
		// store update
		chg := Change{Action: CSUpdateObject, ObjectID: targetID,
			Value: targetObj}
		changes = []Change{chg}
		err = tx.RemoveObject(targetID)
		if err != nil {
			return "", nil, nil, err
		}
		err = tx.StoreObject(updatedObj)
		if err != nil {
			return "", nil, nil, err
		}
	} // If there isn't an object we treat it as a simple activity
	return activityID, nil, changes, nil
}

func (c *Core) OutboxNonSpecialActivity(tx Transaction, actorID object.LocalID, outboxID object.LocalID, activity map[string]interface{}) (activityID object.LocalID, prop []interface{}, changes []Change, err error) {
	activityID = c.preliminaryOutboxTweaks(actorID, activity)
	return activityID, nil, nil, nil
}
