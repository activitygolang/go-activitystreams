package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	"testing"
)

// Contains tests for the outbox handlers.
// This will necessarily also test other parts of the core system.

func TestOutboxAccept(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/baz/asdf")
	follow := map[string]interface{}{"@id": targetID, "@type": "Follow",
		"attributedTo": "/baz", "object": "/jrandom"}
	storage.objects[targetID] = follow
	// Construct Follow Accept
	accept := map[string]interface{}{"@type": "Accept", "object": targetID}
	activityID, err := core.PostOutbox("/jrandom/outbox", accept)
	if err != nil {
		t.Fatal("PostOutbox Accept error")
	}
	// Check objects
	followers := storage.objects["/jrandom/followers"]
	fItems := followers["items"].([]interface{})
	if !reflect.DeepEqual(fItems, []interface{}{"/baz"}) {
		t.Fatal("OutboxAccept followers failure:", fItems)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxAccept no changeset")
	}
	expected := []Change{
		Change{CSListRemove, "/jrandom/followers", "items", "/baz"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expected) {
		t.Fatal("OutboxAccept failure: bad changeset:", changeset)
	}
}

func TestOutboxAdd(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/jrandom/foo")
	target := map[string]interface{}{"@id": targetID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom")}
	storage.objects[targetID] = target
	// Construct Add
	add := map[string]interface{}{"@type": "Add",
		"target": string("https://foo.test" + targetID),
		"object": "jabber"}
	// Run test
	activityID, err := core.PostOutbox("/jrandom/outbox", add)
	if err != nil {
		t.Fatal("OutboxAdd error:", err, storage.objects)
	}
	// Check objects
	collection := storage.objects[targetID]
	expected := map[string]interface{}{"@id": targetID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items":        []interface{}{"jabber"}}
	if !reflect.DeepEqual(collection, expected) {
		t.Fatal("OutboxAdd failure:", collection)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxAdd no changeset")
	}
	expectedCS := []Change{
		Change{CSListRemove, "/jrandom/foo", "items", "jabber"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxAdd failure: bad changeset:", changeset)
	}
}

func TestOutboxBlock(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	// Build dummy actor
	core.AddActor("jrandom", "Person")
	// Contruct Block
	block := map[string]interface{}{"@type": "Block", "object": "evilfoo"}
	// Run Test
	activityID, err := core.PostOutbox("/jrandom/outbox", block)
	if err != nil {
		t.Fatal("OutboxBlock error:", err, storage.objects)
	}
	// Check objects
	blockList := storage.objects["/jrandom/blocked"]
	if !reflect.DeepEqual(blockList["items"], []interface{}{"evilfoo"}) {
		t.Fatal("OutboxBlock failure:", blockList)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxBlock no changeset")
	}
	expectedCS := []Change{
		Change{CSListRemove, "/jrandom/blocked", "items", "evilfoo"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxBlock failure: bad changeset:", changeset)
	}
}

func TestOutboxCreate(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	// Build dummy actor
	core.AddActor("jrandom", "Person")
	// Construct the test object
	creObject := map[string]interface{}{"@type": "Note", "subject": "foo"}
	// Construct the test Create
	activity := map[string]interface{}{"@type": "Create", "object": creObject}
	// Run test
	activityID, err := core.PostOutbox("/jrandom/outbox", activity)
	if err != nil {
		t.Fatal("OutboxCreate error:", err, storage.objects)
	}
	// Check objects
	if len(storage.objects) != 9 {
		t.Fatal("OutboxCreate bad database size:", storage.objects)
	}
	// Get the object ID so we can check it
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	act := storage.objects[localActivityID]
	objectID := act["object"].(object.LocalID)
	// Check changesets
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxCreate no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSRemoveObject, ObjectID: objectID},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxCreate failure: bad changeset:", changeset)
	}
}

func TestOutboxDelete(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/jrandom/asdf")
	obj := map[string]interface{}{"@id": targetID, "@type": "Note",
		"subject": "foo", "attributedTo": object.LocalID("/jrandom")}
	storage.objects[targetID] = obj
	// Construct Delete
	del := map[string]interface{}{"@type": "Delete",
		"object": string("https://foo.test" + targetID)}
	// Run Test
	activityID, err := core.PostOutbox("/jrandom/outbox", del)
	if err != nil {
		t.Fatal("OutboxDelete error:", err, storage.objects)
	}
	// Check objects
	deleted, ok := storage.objects[targetID]
	if !ok {
		t.Fatal("OutboxDelete no tombstone:", deleted, ok)
	} else if len(deleted) != 4 {
		t.Fatal("OutboxDelete bad tombstone length:", deleted)
	} else if deleted["@id"].(object.LocalID) != targetID {
		t.Fatal("OutboxDelete bad @id:", deleted)
	} else if deleted["@type"].(string) != "Tombstone" {
		t.Fatal("OutboxDelete bad @type:", deleted)
	} else if deleted["formerType"].(string) != "Note" {
		t.Fatal("OutboxDelete bad formerType:", deleted)
	} else if len(deleted["deleted"].(string)) < 30 {
		t.Fatal("OutboxDelete bad deleted:", deleted)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxDelete no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSUpdateObject, ObjectID: targetID,
			Value: map[string]interface{}{"@id": targetID, "@type": "Note",
				"subject": "foo", "attributedTo": object.LocalID("/jrandom")}},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxDelete failure: bad changeset:", changeset)
	}
}

func TestOutboxLike(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/foo/targetobj")
	targetObject := map[string]interface{}{"@id": targetID, "@type": "Note",
		"summary": "Fnord!", "attributedTo": object.LocalID("/foo")}
	storage.objects[targetID] = targetObject
	// Construct Like
	like := map[string]interface{}{"@type": "Like",
		"object": string("https://foo.test" + targetID),
		"actor":  "/jrandom"}
	// Run Test
	activityID, err := core.PostOutbox("/jrandom/outbox", like)
	if err != nil {
		t.Fatal("OutboxLike error:", err, storage.objects)
	}
	// Check objects
	liked := storage.objects["/jrandom/liked"]
	expected := map[string]interface{}{"@id": object.LocalID("/jrandom/liked"),
		"@type": "Collection", "attributedTo": object.LocalID("/jrandom"),
		"items": []interface{}{"https://foo.test/foo/targetobj"}}
	if reflect.DeepEqual(liked, expected) != true {
		t.Fatal("OutboxLike failure:", liked)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxLike no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSListRemove, ObjectID: "/jrandom/liked",
			FieldName: "items", Value: string("https://foo.test" + targetID)},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxLike failure: bad changeset:", changeset)
	}
}

func TestOutboxMove(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	originID := object.LocalID("/jrandom/foo")
	origin := map[string]interface{}{"@type": "Collection",
		"@id": originID, "attributedTo": object.LocalID("/jrandom"),
		"items": []interface{}{"one", "two", "three"}}
	storage.objects[originID] = origin
	targetID := object.LocalID("/jrandom/bar")
	target := map[string]interface{}{"@type": "Collection",
		"@id": targetID, "attributedTo": object.LocalID("/jrandom"),
		"items": []interface{}{"one", "two"}}
	storage.objects[targetID] = target
	// Construct Move
	move := map[string]interface{}{"@type": "Move",
		"origin": string("https://foo.test" + originID),
		"target": string("https://foo.test" + targetID),
		"object": "three"}
	// Run test
	activityID, err := core.PostOutbox("/jrandom/outbox", move)
	if err != nil {
		t.Fatal("OutboxMove error:", err, storage.objects)
	}
	// Check objects
	newOrigin := storage.objects[originID]
	expected := map[string]interface{}{"@id": originID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items":        []interface{}{"one", "two"}}
	if !reflect.DeepEqual(newOrigin, expected) {
		t.Fatal("OutboxMove failure:", newOrigin)
	}
	newTarget := storage.objects[targetID]
	expected = map[string]interface{}{"@id": targetID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items":        []interface{}{"one", "two", "three"}}
	if !reflect.DeepEqual(newTarget, expected) {
		t.Fatal("OutboxMove failure:", newTarget)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxMove no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSListAppend, ObjectID: originID, Value: "three"},
		Change{Action: CSListRemove, ObjectID: targetID, Value: "three"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxMove failure: bad changeset:", changeset)
	}
}

func TestOutboxQuestion(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	// Construct valid Question
	question := map[string]interface{}{"@type": "Question",
		"oneOf": []interface{}{"foo", "bar"}}
	// Run test
	activityID, err := core.PostOutbox("/jrandom/outbox", question)
	if err != nil {
		t.Fatal("OutboxQuestion error:", err, storage.objects)
	}
	// Check changesets
	changeset, ok := storage.changesets[string(activityID)]
	if ok {
		t.Fatal("OutboxQuestion shouldn't leave a changeset", changeset)
	}
	// Construct invalid Question
	question = map[string]interface{}{"@type": "Question",
		"oneOf": []interface{}{"foo", "bar"},
		"anyOf": []interface{}{"foo", "bar"}}
	// Run test
	_, err = core.PostOutbox("/jrandom/outbox", question)
	if err != BadQuestion {
		t.Fatal("OutboxQuestion error:", err, storage.objects)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok = storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxQuestion no changeset")
	}
	expected := []Change{
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expected) {
		t.Fatal("OutboxQuestion failure: bad changeset:", changeset)
	}
}

func TestOutboxRemove(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/jrandom/foo")
	target := map[string]interface{}{"@id": targetID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items":        []interface{}{"bar", "baz"}}
	storage.objects[targetID] = target
	// Construct Remove
	remove := map[string]interface{}{"@type": "Remove",
		"target": string("https://foo.test" + targetID),
		"object": "bar"}
	// Run test
	activityID, err := core.PostOutbox("/jrandom/outbox", remove)
	if err != nil {
		t.Fatal("OutboxRemove error:", err, storage.objects)
	}
	// Check objects
	collection := storage.objects[targetID]
	expected := map[string]interface{}{"@id": targetID, "@type": "Collection",
		"attributedTo": object.LocalID("/jrandom"),
		"items":        []interface{}{"baz"}}
	if !reflect.DeepEqual(collection, expected) {
		t.Fatal("OutboxRemove failure:", collection)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxRemove no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSListAppend, ObjectID: targetID, Value: "bar"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxRemove failure: bad changeset:", changeset)
	}
}

func TestOutboxTentativeAccept(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/baz/asdf")
	follow := map[string]interface{}{"@id": targetID, "@type": "Follow",
		"attributedTo": "/baz", "object": "/jrandom"}
	storage.objects[targetID] = follow
	// Construct Follow Accept
	accept := map[string]interface{}{"@type": "TentativeAccept",
		"object": targetID}
	activityID, err := core.PostOutbox("/jrandom/outbox", accept)
	if err != nil {
		t.Fatal("PostOutbox TentativeAccept error")
	}
	followers := storage.objects["/jrandom/followers"]
	fItems := followers["items"].([]interface{})
	if !reflect.DeepEqual(fItems, []interface{}{"/baz"}) {
		t.Fatal("PostOutbox TentativeAccept followers failure:", fItems)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxTentativeAccept no changeset")
	}
	expected := []Change{
		Change{Action: CSListRemove, ObjectID: "/jrandom/followers",
			FieldName: "items", Value: "/baz"},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expected) {
		t.Fatal("OutboxTentativeAccept failure: bad changeset:", changeset)
	}
}

func TestOutboxUndo(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	modID := object.LocalID("/jrandom/foo")
	modifyObj := map[string]interface{}{
		"@id":          modID,
		"@type":        "Note",
		"summary":      "Bar!",
		"attributedTo": object.LocalID("/jrandom")}
	storage.objects[modID] = modifyObj
	targetID := object.LocalID("/jrandom/bar")
	undoTarget := map[string]interface{}{
		"@id":          targetID,
		"@type":        "Update",
		"object":       map[string]interface{}{"@id": "foo", "summary": "Bar!"},
		"attributedTo": object.LocalID("/jrandom")}
	storage.objects[targetID] = undoTarget
	change := Change{
		Action:   CSUpdateObject,
		ObjectID: modID,
		Value: map[string]interface{}{"@id": modID, "@type": "Note",
			"summary": "Foo!", "attributedTo": object.LocalID("/jrandom")}}
	storage.changesets[string(targetID)] = []Change{change}
	// Construct Undo
	undo := map[string]interface{}{"@type": "Undo",
		"object": string("https://foo.test" + targetID)}
	// Run Test
	activityID, err := core.PostOutbox("/jrandom/outbox", undo)
	if err != nil {
		t.Fatal("OutboxUndo error:", err)
	}
	// Check Objects
	obj := storage.objects[modID] // Has it been reverted?
	expected := map[string]interface{}{
		"@id":          modID,
		"@type":        "Note",
		"summary":      "Foo!",
		"attributedTo": object.LocalID("/jrandom")}
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("OutboxUndo failure:", obj)
	}
	// Check Changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxUpdate no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSUpdateObject, ObjectID: modID,
			Value: map[string]interface{}{"@id": modID, "@type": "Note",
				"summary": "Bar!", "attributedTo": object.LocalID("/jrandom")}},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxUpdate failure: bad changeset:", changeset)
	}
}

func TestOutboxUpdate(t *testing.T) {
	// Setup the Core and DummyStorage
	storage := NewDummyStorage()
	core := NewCore()
	core.Hostname = "foo.test"
	core.SetDatabase(storage)
	// Setup the environment
	core.AddActor("jrandom", "Person")
	targetID := object.LocalID("/jrandom/foo")
	targetObject := map[string]interface{}{"@id": targetID, "@type": "Note",
		"summary": "foo", "attributedTo": object.LocalID("/jrandom")}
	storage.objects[targetID] = targetObject
	// Construct Update
	update := map[string]interface{}{"@type": "Update",
		"object": map[string]interface{}{
			"@id": string("https://foo.test" + targetID), "summary": "bar"}}
	expected := map[string]interface{}{"@id": targetID, "@type": "Note",
		"summary": "bar", "updated": "",
		"attributedTo": object.LocalID("/jrandom")}
	// Run Test
	activityID, err := core.PostOutbox("/jrandom/outbox", update)
	if err != nil {
		t.Fatal("OutboxUpdate error:", err)
	}
	// Check objects
	// We can't predict the value of deleted
	obj := storage.objects[targetID]
	expected["updated"] = obj["updated"]
	if !reflect.DeepEqual(obj, expected) {
		t.Fatal("OutboxUpdate failure:", obj)
	}
	// Check changesets
	localActivityID, _, _, _ := object.LocalIDFromGlobalID(activityID)
	changeset, ok := storage.changesets[string(localActivityID)]
	if !ok {
		t.Fatal("OutboxUpdate no changeset")
	}
	expectedCS := []Change{
		Change{Action: CSUpdateObject, ObjectID: targetID,
			Value: map[string]interface{}{"@id": targetID, "@type": "Note",
				"summary": "foo", "attributedTo": object.LocalID("/jrandom")}},
		Change{Action: CSAddObject, ObjectID: localActivityID,
			Value: storage.objects[localActivityID]},
		Change{Action: CSListAppend, ObjectID: "/jrandom/outbox",
			FieldName: "items", Value: localActivityID},
	}
	if !reflect.DeepEqual(changeset, expectedCS) {
		t.Fatal("OutboxUpdate failure: bad changeset:", changeset)
	}
}
