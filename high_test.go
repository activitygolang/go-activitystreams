package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
	"reflect"
	"testing"
	"fmt"
)

func startDummyServer(hostname string) (core *Core, db Storage) {
	db = NewDummyStorage()
	core = NewCore()
	core.SetDatabase(db)
	core.Hostname = hostname
	return core, db
}

func TestBasicServer(t *testing.T) {
	svr, db := startDummyServer("foo.test")
	if svr == nil || db == nil {
		t.Fatal("server start failure")
	}
	// add actor
	err := svr.AddActor("/foo", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	// send note
	noteID, err := svr.PostOutbox("/foo/outbox",
		map[string]interface{}{"@type": "Note", "subject": "Foo!",
			"content": "blah bleh bluh"})
	if err != nil {
		t.Fatal("PostOutbox error:", err)
	}
	// get create from ID
	local, server, port, err := object.LocalIDFromGlobalID(noteID)
	if err != nil {
		t.Fatal("Localize ID error:", local, server, port, err)
	}
	create, err := svr.Get(local, "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{
		// Unpredictable fields
		"@id": create["@id"], "object": create["object"],
		"published": create["published"],
		// Predictable fields
		"@type": "Create", "attributedTo": "https://foo.test/foo",
	}
	if !reflect.DeepEqual(create, expected) {
		t.Fatal("Get note failure:", create)
	}
}

func TestClusterPropagation(t *testing.T) {
	// ======================= INIT ===================
	// start 3 servers
	serverA, dbA := startDummyServer("one.test")
	if serverA == nil || dbA == nil {
		t.Fatal("serverA start failure")
	}
	serverB, dbB := startDummyServer("two.test")
	if serverB == nil || dbB == nil {
		t.Fatal("serverB start failure")
	}
	//serverC, dbC := startDummyServer("three.test")
	//if serverC == nil || dbC == nil {
	//	t.Fatal("serverC start failure")
	//}
	// Create 3 actors per server
	// === A
	err := serverA.AddActor("Afoo", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	err = serverA.AddActor("Abar", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	err = serverA.AddActor("Aquux", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	// === B
	err = serverB.AddActor("Bfoo", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	err = serverB.AddActor("Bbar", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	err = serverB.AddActor("Bquux", "Person")
	if err != nil {
		t.Fatal("AddActor error:", err)
	}
	// ==================== RUN TESTS ===================
	// Propagation: Afoo send to Abar, Bfoo, Bbar
	// send note
	createID, err := serverA.PostOutbox("/Afoo/outbox",
		map[string]interface{}{"@type": "Note", "subject":"Inter-prop test",
			"to":[]interface{}{"https://one.test/Abar",
				"https://two.test/Bfoo", "https://two.test/Bbar"}})
	if err != nil {
		t.Fatal("PostOutbox error:", err, createID)
	}
	// ================== DO PROPAGATION==================
	// Do upper level propagation
	//  get props
	objects, props, err := serverA.PendingPropagations()
	if err != nil {
		t.Fatal("PendingPropagation error:", err)
	}
	for objID, targets := range props {
		GlobalizeObject(objects[objID], "one.test", 0)
		for _, target := range targets {
			fmt.Println("Propagation target:", target)
			// send target to server
			var targetInbox object.LocalID
			switch target.Url { // yes it is crude, it is also simple
			case "https://two.test/Bfoo":
				targetInbox = object.LocalID("/Bfoo/inbox")
			case "https://two.test/Bbar":
				targetInbox = object.LocalID("/Bbar/inbox")
			default:
				t.Fatal("Invalid propagation:", target)
			}
			err = serverB.PostInboxRemote(targetInbox, objects[objID])
			if err != nil {
				t.Fatal("PostInboxRemote error:", err)
			}
		}
	}
	// =================== CHECK RESULTS ================
	// get results, should have propagation to Abar, Bfoo, and Bbar
	// check Abar
	abarInbox, err := serverA.Get("/Abar/inbox", "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected := map[string]interface{}{"@type":"OrderedCollection",
		"@id":"https://one.test/Abar/inbox",
		"attributedTo":"https://one.test/Abar",
		"items":[]interface{}{createID}}
	if !reflect.DeepEqual(abarInbox, expected) {
		t.Fatal("Propagation failure, Abar inbox:", abarInbox)
	}
	// check Bfoo
	bfooInbox, err := serverB.Get("/Bfoo/inbox", "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected = map[string]interface{}{"@type":"OrderedCollection",
		"@id":"https://two.test/Bfoo/inbox",
		"attributedTo":"https://two.test/Bfoo",
		"items":[]interface{}{createID}}
	if !reflect.DeepEqual(bfooInbox, expected) {
		t.Fatal("Propagation failure, Bfoo inbox:", bfooInbox)
	}
	// check Bbar
	bbarInbox, err := serverB.Get("/Bbar/inbox", "")
	if err != nil {
		t.Fatal("Get error:", err)
	}
	expected = map[string]interface{}{"@type":"OrderedCollection",
		"@id":"https://two.test/Bbar/inbox",
		"attributedTo":"https://two.test/Bbar",
		"items":[]interface{}{createID}}
	if !reflect.DeepEqual(bbarInbox, expected) {
		t.Fatal("Propagation failure, Bbar inbox:", bbarInbox)
	}
}

func TestGhostReplies(t *testing.T) {
	// ==== Setup Server ====
	svr, db := startDummyServer("foo.test")
	if svr == nil || db == nil {
		t.Fatal("server start failure")
	}
	// ==== Setup Actors ====
	svr.AddActor("/foo", "Person")
	svr.AddActor("/bar", "Person")
	// ==== Setup object environment ====
	tx, _ := db.StartTransaction()
	// collection to be addressed
	collectionID := object.LocalID("/foo/coltest")
	collectionObject := map[string]interface{}{
		"@type":"Collection", "@id":collectionID,
		"attributedTo":object.LocalID("/foo"),
		"items":[]interface{}{"https://foo.test/bar"},
	}
	tx.StoreObject(collectionObject)
	// object to be replied to
	replyeeID := object.LocalID("/bar/replyee")
	replyeeObject := map[string]interface{}{
		"@type":"Note", "@id":replyeeID,
		"attributedTo":object.LocalID("/bar"),
	}
	tx.StoreObject(replyeeObject)
	// ==== Trigger object ==== 
	testObject := map[string]interface{}{
		"@type":"Create", "@id":"https://bar.test/bfoo/1234",
		"to":[]interface{}{"https://foo.test/foo",
			"https://foo.test" + string(collectionID)},
		"inReplyTo":"https://foo.test" + string(replyeeID),
	}
	// ==== Send object to server ====
	fmt.Println("===========================================")
	err := svr.PostInboxRemote("/foo/inbox", testObject)
	if err != nil {
		t.Fatal("PostInboxRemote error:", err)
	}
	// ==== Check results ====
	// check foo/inbox
	fooInbox, err := tx.LoadObject("/foo/inbox")
	if err != nil {
		t.Fatal("foo/inbox loading error:", err)
	}
	items, ok := fooInbox["items"]
	expected := []interface{}{"https://bar.test/bfoo/1234"}
	if !ok || !reflect.DeepEqual(items, expected) {
		t.Fatal("/foo/inbox contents failure:", ok, items)
	}
	// check bar/inbox
	barInbox, err := tx.LoadObject("/bar/inbox")
	if err != nil {
		t.Fatal("bar/inbox loading error:", err)
	}
	items, ok = barInbox["items"]
	expected = []interface{}{"https://bar.test/bfoo/1234"}
	if !ok || !reflect.DeepEqual(items, expected) {
		t.Fatal("/bar/inbox contents failure:", ok, items)
	}
	// check replies field
	replyItems, err := tx.ObjcolGetAllField(replyeeID, "replies")
	if err != nil {
		t.Fatal("replyee loading error", err)
	}
	expected = []interface{}{"https://bar.test/bfoo/1234"}
	if !reflect.DeepEqual(replyItems, expected) {
		t.Fatal("replyee replies failure:", replyItems)
	}
	tx.EndTransaction(nil)
}
