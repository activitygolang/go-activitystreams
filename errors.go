package activitystreams

import (
	"errors"
)

// Errors
var InvalidTransaction = errors.New("Invalid transaction")
var ClosedStorage = errors.New("Database closed")
var InvalidOutbox = errors.New("Invalid Outbox ID")
var ProtectedObject = errors.New("Object is protected")
var InvalidDeleteTarget = errors.New("Deletion target is invalid")
var BadActorConstruction = errors.New("Actor is borked yo!")
var ActivityNoTarget = errors.New("Activity does not have a valid target")
var UnownedTarget = errors.New("Object of Update not owned by Actor")
var InvalidTargetType = errors.New("Target object is not the correct type")
var DuplicateItem = errors.New("Item already exists in this location")
var BadChangeset = errors.New("Invalid changeset action")
var NoChangeset = errors.New("No changeset under that ID")
var ObjectNotInOrigin = errors.New("Object not present in Origin collection")
var BadQuestion = errors.New("Questions must have either oneOf xor anyOf")
var BlockedActor = errors.New("Source Actor blocked by Target actor")
var BadPropagationTarget = errors.New("Propagation target does not exist")
