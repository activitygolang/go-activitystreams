package activitystreams

import (
	"gitlab.com/activitygolang/go-activitystreams/object"
)

// Account data
type Account struct {
	User string
	Salt [16]byte
	Time uint32
	Memory uint32
	KeyLen uint32
	HashPW []byte
	ActorID object.LocalID
	ActorURL string
	CreationDate string // RFC3339
}

// Storage interface
type Transaction interface {
	RevertTransaction() error
	EndTransaction(errIn *error) error
	// Primary object store -- object level
	StoreObject(obj map[string]interface{}) error
	LoadObject(objectID object.LocalID) (map[string]interface{}, error)
	RemoveObject(objectID object.LocalID) error
	SetProtected(objectID object.LocalID, protected bool) error
	IsProtected(objectID object.LocalID) (bool, error)
	BasicObjectData(objectID object.LocalID) (string, object.LocalID, error)
	Exists(objectID object.LocalID) (bool, error)
	// Primary object store -- field level
	LoadObjectField(objectID object.LocalID, fieldName string) (interface{}, error)
	ListAppend(objectID object.LocalID, fieldName string, data interface{}) error
	ListRemove(objectID object.LocalID, fieldName string, data interface{}) error
	AddField(objectID object.LocalID, fieldName string, data interface{}) error
	UpdateField(objectID object.LocalID, fieldName string, data interface{}) error
	RemoveField(objectID object.LocalID, fieldName string) error
	ValueInObject(objectID object.LocalID, fieldName string, data interface{}) (bool, error)
	// Object ancillary collection handling
	ObjcolAdd(objectID object.LocalID, fieldName string, value interface{}) error
	ObjcolRM(objectID object.LocalID, fieldName string, value interface{}) error
	ObjcolGetAllField(objectID object.LocalID, fieldName string) ([]interface{}, error)
	ObjcolGetFields(objectID object.LocalID) ([]string, error)
	ObjcolSet(objectID object.LocalID, data map[string][]interface{}) error
	ObjcolRMAll(objectID object.LocalID) error
	ObjcolGetAll(objectID object.LocalID) (map[string][]interface{}, error)
	// Changeset handling
	StoreChangeset(changeID string, changeset []Change) error
	LoadChangeset(changeID string) ([]Change, error)
	// Cached object store
	StoreCache(obj map[string]interface{}) error
	LoadCache(objID string) (map[string]interface{}, error)
	// Propagation holding
	StoreProp(obj map[string]interface{}, targets []PropRetryTarget) error
	GetPropIDs() ([]object.LocalID, error)
	GetPropObject(objectID object.LocalID) (map[string]interface{}, error)
	GetPropTargets(objectID object.LocalID) ([]PropRetryTarget, error)
	UpdatePropTarget(objectID object.LocalID, target PropRetryTarget) error
	RemovePropTarget(objectID object.LocalID, targetURL string) error
	// Account data
	StoreAccount(acct Account) error
	LoadAccount(user string) (Account, error)
	UpdateAccount(user string, acct Account) error
	DeleteAccount(user string) error
}

type Storage interface {
	// Storage utility / management
	Close() error
	StartTransaction() (Transaction, error)
}

// Upper system interface
// This interface is what the server provides to the AP core, so that it can
//  send data to other servers without needing to know about TLS or other
//  complications.
type UpperLevel interface {
	SendObject(object map[string]interface{}, toServer string) error
}
